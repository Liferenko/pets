if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
    " CScope (similar to Ctags)
    Plug 'mfulz/cscope.nvim'

    Plug 'williamboman/mason.nvim', { 'do': ':MasonUpdate' }

    Plug 'mhinz/vim-grepper'

    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

    " DB management in Nvim
    Plug 'tpope/vim-dadbod'
    Plug 'kristijanhusak/vim-dadbod-ui'
    
    " Vim AI
    Plug 'madox2/vim-ai', { 'do': './install.sh' }

    " Elixir plugin
    Plug 'elixir-editors/vim-elixir'
    "
    " Plugin outside ~/.vim/plugged with post-update hook
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'

    " Initialize plugin system
call plug#end()


" Look for tags file in a dir or abowe the tree
set mouse= "disable a mouse
set tags=./tags,tags;$HOME
set fileencodings=utf-8,cp1251,koi8-r,cp866
syntax on
" Размер табулации по умолчанию

" Возвращает нормальный цвет vim в tmux
"set background=dark
set t_Co=256


set shiftwidth=4
set softtabstop=2
set tabstop=2
set nu
set bs=2 " без этого бекспейс будет жутко криво работать
"set foldcolumn=2 " Левая колонка фолдинга
"set foldenable
set incsearch " Поиск по набору текста
set scrolloff=38

" A proper view of Netrw (vim Explorer tree)
let g:netrw_liststyle = 3
let g:netrw_browse_split = 0
let g:netrw_winsize = 50

set expandtab " Преобразование Таба в пробелы
set autoindent " Включить автоотступы
set showmode " показывает в каком режиме работаешь
set ignorecase "игнорировать прописные/строчные при поиске
set hlsearch "при поиске помечать все найденные строки

" Ctags
set tags+=$HOME/home/$USER/all_tags_saved_here/

" Esc to kj
"imap kj <Esc>

" Двойное нажатие // запускает поиск по выделенному тексту
vnoremap // y/<C-R>"<CR>

" Autocomplete brackets
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>

inoremap <! <!--  --><left><left><left><left>
inoremap < <><left>
inoremap <% <%=  %><left><left><left>
inoremap [ []<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O
inoremap { {}<left>


" Выключаем надоедливый "звонок"
set novisualbell
set dir=~/.vim " Все swap файлы будут помещаться в эту папку; ну их нафиг
set visualbell " Включает виртуальный звонок (моргает, а не бибикает при
" ошибках)

" " Fix arrow keys that display A B C D on remote shell:
" " Use Vim settings, rather then Vi settings (much better!).
" " This must be first, because it changes other options as a side effect.
set nocompatible

let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8 } }
let $FZF_DEFAULT_OPTS='--reverse'
let g:fzf_branch_actions = {
      \ 'rebase': {
      \   'prompt': 'Rebase> ',
      \   'execute': 'echo system("{git} rebase {branch}")',
      \   'multiple': v:false,
      \   'keymap': 'ctrl-r',
      \   'required': ['branch'],
      \   'confirm': v:false,
      \ },
      \ 'track': {
      \   'prompt': 'Track> ',
      \   'execute': 'echo system("{git} checkout --track {branch}")',
      \   'multiple': v:false,
      \   'keymap': 'ctrl-t',
      \   'required': ['branch'],
      \   'confirm': v:false,
      \ },
      \}

" My own remap for Explorer
nnoremap <leader>ve :Vexplore<CR>
nnoremap <leader>te :Texplore<CR>
nmap gs  <plug>(GrepperOperator)
nnoremap <leader>f :Buffers<CR>
nnoremap <Leader>co :copen<CR>
nnoremap <leader>prw :CocSearch <C-R>=expand("<cword>")<CR><CR>
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
nmap <leader>rbf o#TODO REMOVE BEFORE FLIGHT!!!!!!<esc>

