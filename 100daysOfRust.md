2023-11-12

When I will be on an interview again - I'd like to ask what tools company's team use in communication.
Teams & Outlook - red flag
Slack - hmm, well-enough
Discord - means the team is friendly and nerdy enough to use the best known tools


---
2023-11-09

# Error:
:observer.start() is not starting in IEx 

### Solution:
You can do extra_applications:













---

```
if Mix.env == :dev, do: [:observer, :wx, :logger, :runtime_tools], else: [:logger, :runtime_tools]
```
2023-09-29
1) An alternative to `with` usage in Elixir - https://hexdocs.pm/sage/readme.html
`It’s like Ecto.Multi but across business logic and third-party APIs.`

instead of 
```
  with {:ok, user} <- create_user(attrs),
           {:ok, plans} <- fetch_subscription_plans(attrs),
           {:ok, charge} <- charge_card(user, subscription),
           {:ok, subscription} <- create_subscription(user, plan, attrs),
           {:ok, _delivery} <- schedule_delivery(user, subscription, attrs),
           {:ok, _receipt} <- send_email_receipt(user, subscription, attrs),
           {:ok, user} <- update_user(user, %{subscription: subscription}) do
```

we may use this:
```
 new()
    |> run(:user, &create_user/2)
    |> run(:plans, &fetch_subscription_plans/2, &subscription_plans_circuit_breaker/3)
    |> run(:subscription, &create_subscription/2, &delete_subscription/3)
    |> run_async(:delivery, &schedule_delivery/2, &delete_delivery_from_schedule/3)
    |> run_async(:receipt, &send_email_receipt/2, &send_excuse_for_email_receipt/3)
    |> run(:update_user, &set_plan_for_a_user/2)
    |> finally(&acknowledge_job/2)
    |> transaction(SageExample.Repo, attrs)
```
---

2023-09-28
1) looks like I can avoid bash scripts with Terraform Providers or K8s KinD usage (https://kind.sigs.k8s.io/docs/user/working-offline/)

---

2023-09-13
1) kubectl alternative? Hmm, k9s looks interesting

---

2023-09-11
1) kinda ready-to-use ECK configs - https://github.com/framsouza/eck-ready-for-production/blob/main/external-dns.yaml



--- 
2023-07-02
1) `kubectl get pods -A` to check which of your pods are running if you don't know your namespace



---

2023-07-02
1) Bad SQL for removing expired items -
`SELECT ... WHERE DateDiff(mm, OrderDate,GetDate()) >= 30;`
Fixed: `SELECT ... WHERE OrderDate < DateAdd(mm, -30, GetDate());`


---

2023-07-01
1) "The more pressure you take, the more pressure you will get" - once being said in ThePrimeagen video here - https://www.youtube.com/watch?v=3J6ZpoPWauI
So saying "No" will play good game for me right now AND will play better in long run

---

2023-06-24
1) Way better to use language model "Yes and..." instead of "Yes, but" in any type of conversation

---

2023-06-23
1) Algo course notes:
    - ask better clarifying questions
    - writing the code - is the last thing to do and its optional
    - ask questions earlier

    - shape data structure(s) to the problem - not the problem to the data structure
    - the 1st solution you do - should be dummiest and easiest solution that accurate. Optimize it later
2) 

---

2023-06-22
1) did a bit of rm-to-rip typelearning
2) found a Warp (CLI alternative). So far so good. Looks fancy. Works with Tmux

---

2023-06-18
1) A course "Rust for TS developers" is done. The course is good, useful, quite "dry" and cover 2 main concepts: trait+imp+generics and Borrow checker.
I think it's quite good for starting up.
2) `rustup docs --book` - offline Rust book

---

2023-06-17
1) I can't say I got the 100% of generic idea. I got the result it wants to reach: to be 1 for every situations. And that's pretty nice. Kinda macro from Elixir: lets generate all possible variants for every situation.
2) I think I got the idea of `impl<T, U> ...` - it means that 2 types might be different (ex: f64 and i32) BUT in some cases it still will work when T=f64 and U=f64. We cover both cases: when they are same and they are different. With `impl<T>` we won't have this opportunity to cover both.

---

2023-06-16
1) `cargo install cargo-watch` + watch only `/src` and clear the console - `cargo watch -c -w src -x run`
2) done with yesterday's code where From and IntoIter had been kicked my brain out.
Now Dispay works, Iter works for Rectangle, `collide/2` works (but I'm not sure it works okay for Rect-within-Circle cases)
3) how to public rust app in brew - https://federicoterzi.com/blog/how-to-publish-your-rust-project-on-homebrew/


---

2023-06-15
1) Display and Default. Boom! Looks good, I hope I will use this knowledge :)
2) vim knowledge boom moment: I saw how ThePrimeagen did "add lost brackets to the end of the line for this N lines". It works this way: select the lines, press shift+:, it shows `:'<'>` and here I can use any of commands. For this example it was :s/%pattern_to_change%$/%new_pattern/g.
Dollar sign is the sign "The end of the line"

---

2023-06-13
1) added Rust-written music player. Vim keybindings, shitty look.. oh, thats what I was looking for :)
2) played around Rust tutor this time. Modules + crates + `use ...` - goes okay. Lovin' it

---

2023-06-10
1) vec![...] is temporary. We need to declare it in a var and use it AFTER - now the vec! will be in a memory
2) kinda worked out with structs, impls and triats. Structs were useing for objects (Circle, Rectangle), impl for their action with specific struct (Area for Circle, Area for Rectangle) and triat for the action we want to do with the all structs (Area)
3) idea of learning project - thinky-type the RIP project (Rust-written rm command)
4) `::std::io::...` use for absolute path. At the same time `std::io::...` is pretty standart

---
2023-06-10
1) if I need to get a String from args but stuck with Option<String> - maybe I need to use .expect("error msg") at the end.
Example
```
    let path: String =
        env::args()
        .last()
        .expect("should be at least one arg");
```
2) kinda hard part when I tried `trait` to add it by myself. But resolve it with some hinds from a course
3) `.parse::<T>()` can have `.parse::<T>().ok()` or `.parse::<T>().err()`
4) Rules:
    - there might be only one owner
    - there might be unlimited amount on immutable borrows (references, or my example: read-onlys);
    - there might be only one mutable ref (or my example - RWs) and no immutable refs

---
2023-06-9
1) watched a little :/ it was moving-out day

---
2023-06-8
1) worked with Option, .get(index) and *num, &num and *&num

---
2023-06-7
1) Played around .collect() and .iter()
2) Coc-rust works kinda good for Nvim with rust. Nice!
3) worked pretty ok with tutorial this night

---
2023-06-4
1) NOW I can say pattern matching is working in Rust pretty the same way it works in Elixir and Erlang
2) for using any sign as an alias name in ZSH - I need to add backslash. Example: \? 
3) question_mark now works quite okay. The goal of this app is reached. Nice! 

4) in Rust we may use `let y = &x` as a read-only reference and `let y = &mut x` as RW reference.
This RW-* is kinda new for me. 
When we need to use `&mut x` - we must declare `x` as mutable - `let mut x = ...`

in Rust docs it says:
```
    let mut x = vec![1, 2, 3];
    let y = &mut x; // y is a mutable reference to x
```

In this example, y is a mutable reference to the vector x. The reference y can be used to modify the vector's elements without transferring ownership.

---
2023-06-3
1) bought a course "Rust for TS devs". Start!

---
2023-06-2
1) Tried amazing thing: idea -> chat.openai -> CLI app in 25 minutes

---
2023-06-1
1) tried to compile rs-file without main function. For now I'm not sure I know how to do it :)
2) a scope in Rust works quite obvious. I like it: {} define the scope.
3) Rust's compiler can teach me. Nice!

---


