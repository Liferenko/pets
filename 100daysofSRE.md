2023-04-12
1) :AI in nvim works and works pretty awesome
2) :DBUI solves my tasks and works pretty awesome as well

---

2023-04-06
1) how to compile ex project with maximum check of errors and warnings
`mix compile --all-warnings --warnings-as-errors`

---

2023-04-04
1) pgcli connection to remote db
`pgcli postgresql://user:password@host:port/dbname`
---

2023-03-29
1) found OpenCommit here - https://github.com/di-sukharev/opencommit
it helps with commit messages

2023-03-21
1) `curl http://169.254.169.254/latest/meta-data/spot/instance-action` - we need to ask this url constantly
to catch a termination signal
Idea: Elixir or Erlang, make a tiny service to catch this signal 


2023-03-20
1) launched around 10-12 AWS spot instances. Got some insights about. 
Looks like it decrease my AWS costs in April

---------

2023-03-8
1) finally mosh works with AWS. This time it was quite easy to config. 

---------

2023-02-05
1) Weylus - a screen sharing with any device which has browser and same WiFi.
Might be a usecase for a coding inside a vehicle. BT keyboard + phone == laptop-free coding session inside a vehicle
Next step - to try to config it as a second monitor (https://github.com/H-M-H/Weylus)

---------

2023-02-02
1) Playwright - e2e testing better than selenium

---------

2023-01-23
1) git cz makes it easier to git-commit with templates

---------

2023-01-17
1) Need to kill process but donno the pid?
`pidof %app_name`

---------

2023-01-09
1) Too many processes sit on one port? Wanna kill them all? 
`sudo fuser -k %PORT%/tcp`

---------

2023-01-06
1) Now I can make a text-based-screenshot of Tmux pane. Prefix+Alt+P - screenshot

---------

2023-01-04
1) ? as an alias for `w3m google.com` is the power!
2) w3m looks like my choice as well. FInally feels better and way more useful
3) I tried to add dark theme to DBeaver - it killed this app. Thats a bad news. 
4) Good news that I installed gobang - CLI dbeaver app :) Feels nice so far :)

---------

2023-01-01
1) First note in 2023. It turns out Imma a decade developer now. First commit I did was in December, 23th, 2013. Khool

---------

2022-11-21
1) 12V-220V invertor ain't cost bazillion dollars. It costs around 10 usd. Damn! I didn't know that
2) Gmail has a functionality "mark all unread and remove them by one click". I thought I can make it only with partitions (50-100 emails per partition)

---------

2022-11-20
1) one step closer to my Blacklog application for Android. Now it can stop when time's up

----------

2022-11-19
1) Android Studio might work with Android device via WiFi. It's possible to debugging your app here and remotely
2) Vim bindings work quite well in Android Studio
3) My first forked Android app was launched on my Xiami Redmi POCO X3.
4) I defined TODOs for Blacklog

2022-11-08
Today I learned:
1) in Elixir it is possible to String.split nur only with one splitter but with a list of them. Exhibit A: 
String.split(asf, ["/", " ", "+"])

--------------

2022-11-03
Today I learned:
1) ElixirLS and ErlangLS still work bad on my laptop :/ Sucks

--------------

2022-11-02
Today I learned:
1) its better to keep a diary (aka journaling)
2) Nice idea to make (or remake) Elixir projects (nano projects) to Erlang

--------------

2022-10-30

Today I learned:
1) Looks like all I need to use for certification preparations is tutorialdojo. It covers all areas: time-limited quiz + explanations after time is up
2) AWS CloudTrail Log File Verification can detect when log-file was made up (it calls "has been tempered with")

---------------
30 days of AWS started here
---------------


2022-09-15

Today I learned:

1) Erlang trouble with ASDF - the solution was in GCC
source - https://elearning.wsldp.com/pcmagazine/configure-error-no-acceptable-c-compiler-found-in-path/

---------------


2022-09-13

Today I learned:

1) Where to get knowledge about OTP? Here - https://learnyousomeerlang.com/what-is-otp
2) Ansible has an AWS opportunity - https://docs.ansible.com/ansible/latest/collections/amazon/aws/ec2_instance_module.html#examples

---------------


2022-09-4

Today I learned:

1) Redshift is C-store
2) C-store is Columnar storage. Row vs column storage idea looks pretty understandable

---------------


2022-08-31

Today I learned:

1) Summer moved on. Result: my first summer with LeetCode solved tasks
2) SQL tasks looks too easy? Try medium and hard one

---------------


2022-08-30

Today I learned:

1) Erlang/Elixir have BIF and NIF.
BIF - build-in functions
NIF - native inplemented functions. NIF might be used when you need to use Clang in your code.

---------------


2022-08-16

Today I learned:

1) Browser-like search in Tmux - tmux’s default key sequence to enter copy mode and begin a search of the terminal history/scrollback is pretty awkward: Ctrl + b [ Ctrl + r if you’re using the default emacs-style copy mode bindings, or Ctrl + b [ ? if you’re using the vim-style bindings.

---------------


2022-08-13

Today I learned:

1) LeetCode: solved an easy task about tree root and children. It shows out how drastically far Clang goes from Elixir (or vise versa). 
Elixir - ~300ms and ~50mb of RAM for calculation 
Clang - 1ms and ~5mb of RAM for calculation 

2) YEY! My first LeetCode task solved. Believable? It is already

Progress:
- 

---------------


2022-08-9

Today I learned:

Progress:
- reached 1:15:47 of this course - https://www.youtube.com/watch?v=BRuvq59miIo&t=538s

---------------


2022-07-13

Today I learned:

1) I still can think that the tasks can not be that easy.
2) Im still posponing questions to colleagues and after it Im experiencing snowball effect with the tasks. Don't do it, Pablo-Pavlo-Paul. Ask your colleagues!


---------------


2022-06-20

Today I learned:

1) termbin.com - Pastebin for terminal


---------------


2022-05-25

Today I learned:

1) HTTPie - cli postman - https://httpie.io/cli


---------------


2022-04-24

Today I learned:

1) Vim Markdown preview - https://github.com/iamcco/markdown-preview.nvim


---------------


2022-04-23

Today I learned:

1) Exq - queues and schedulers - https://elixircasts.io/elixir-job-processing-with-exq


---------------


2022-04-04

Today I learned:

1) I've lost a streak. It was with me since December 15th 2021 till war starting. 


---------------


2022-03-15
no lectures

Today I learned:

1) https://github.com/ueberauth/ueberauth - Auth with socialnets for Elixir. Plug-based


---------------


2022-03-3
no lectures

Today I learned:

1) Azure instances creating - az vm create -n Virma-az11 -g group-a --image Win2019Datacenter --admin-password %9383% --admin-username %jd% --location centralindia
https://github.com/Azure/azure-cli


---------------


2022-02-17
no lectures

Today I learned:

1) gitconfig aliases. Finally! Now I can use git s, git chn, git ll


---------------


2022-02-05
no lectures

Today I learned:

1) GenServer in a sandbox

---------------


2022-02-04
no lectures

Today I learned:

1) It turns out with "iex --name bla@bla --cookie same_cookie_as_remote_node --remsh this_remote_node@and_ip_address" I can be connected to a node. Mindblowing
2) erlangpl really works. with just ./erlangpl -n name_of_the_node -c same_cookie_as_remote_node

---------------


2022-01-11
no lectures

Today I learned:
1) An issue with root's lost password - 
Solution:
 Reboot your computer
 Hold Shift during boot to start GRUB menu
 Highlight your image and press E to edit
 Find the line starting with "linux" and append rw init=/bin/bash at the end of that line
 Press Ctrl+X to boot.
 Type in passwd username
 Set your password.
 Type in reboot. If that doesn't work, hit Ctrl+Alt+Del

2)  to add yourself in sudoers you need root password and add yourself in visudo


---------------


2022-01-09
no lectures

Today I learned:
1) SAA Exam preparations - https://explore.skillbuilder.aws/learn/course/125/exam-readiness-aws-certified-solutions-architect-associate-digital
2) AWS freelance platform - https://iq.aws.amazon.com/?utm=docs&service=Serverless%20Applications%20Lens

---------------


2022-01-08
no lectures

Today I learned:
1) ready-to-use use-cases for CDK - https://github.com/awslabs/aws-solutions-constructs/tree/main/source/use_cases
updated: more Py-use-cases - https://github.com/aws-samples/aws-cdk-examples/tree/master/python

2) this might be next step for TBC payment processing - https://github.com/aws-samples/aws-cdk-examples/tree/master/python/lambda-layer/



---------------


2022-01-06
15th lecture
Lecture AWS S3

Today I learned:
1) Add MFA to learning and personal accounts
2) Docker works with WordPress and MySQL



---------------


2022-01-05
14th lecture
Lecture AWS services first look + AWS IAM

Today I learned:
1) Edge locations
2) IAM.  MFA means Multi- and not Two-three-Nth factor auth
3) Nice thoughts from http://www.paulgraham.com/raq.html
- Two startups want to hire me. Which should I choose?
Pretend you're an investor—which you are, of your time—and ask yourself which of the two you'd buy stock in.



---------------


2022-01-04
no lectures

Today I learned:
1) Docker-compose for Wordpress... is just working from a box :D That's satisying as hell :)


---------------


2022-01-01
no lectures

Today I learned:
1) 


---------------


2021-12-30
no lectures

Today I learned:
1) AWS CDK >>> cdk synth && cdk deploy && cdk destroy
2) AWS CDK will ask you when it seems to raise your costs for AWS. You are the person who choose
Example I've worked with - https://docs.aws.amazon.com/cdk/v2/guide/ecs_example.html


---------------


2021-12-28
no lectures

Today I learned:
1) AWS documentations and AWS learning videos have not always correct :) I've got problem with TypeScript in learning video which shows old version of code.


---------------


2021-12-27
no lectures

Today I learned:
1) There is the Path inside the AWS. Full-size big-ass Path to your goal with AWS. It's free!


---------------


2021-12-26
no lectures

Today I learned:
1) Alibaba Cloud - AWS from asian world - https://www.alibabacloud.com/pricing?spm=a3c0i.7911826.6791778070.dnavpricing0.794b3870r38JNg
Free trial included
2) Alibaba Cloud learning path - https://www.alibabacloud.com/help/en/doc-detail/86739.html
3) WHole AWS Docs - https://docs.aws.amazon.com
4) Explained and calculated AWS implementations. Ready-to-start with or ready-to-help-sell-AWS-to-the-client implementations - https://aws.amazon.com/solutions/implementations/?solutions-all.sort-by=item.additionalFields.sortDate&solutions-all.sort-order=desc&awsf.Content-Type=*all&awsf.AWS-Product%20Category=*all&awsf.AWS-Industry=*all

---------------


2021-12-25
no lectures

Today I learned:
1) PDF reader with VIM bindings - Zathura
2) AWS learning plans - https://explore.skillbuilder.aws/learn
3) Well-acritectured design principles - https://docs.aws.amazon.com/wellarchitected/latest/analytics-lens/well-architected-design-principles.html

Ideas:


---------------


2021-12-24
AWS class

Today I learned:
1) I've reached first AWS certification

---------------


2021-12-23
no lectures

Today I learned:
1) how to make terminal screencasts - https://asciinema.org/
2) AWS calculator - https://calculator.aws
3) AWS lambda with blank-python - https://github.com/awsdocs/aws-lambda-developer-guide/tree/master/sample-apps/blank-python

---------------


2021-12-20
12th lecture in a row
gitlab-ci

Today I learned:
1) if add a dot to job name - it is the same as comment it. Runner will skip this job

---------------


2021-12-18
12th lecture in a row
2nd lecture soft skills

Today I learned:
1) https://12factor.net/

---------------


2021-12-17
11th lecture in a row
1st lecture CI/CD

Today I learned:
1) https://12factor.net/

---------------


2021-12-13
9th lecture in a row
3st lecture about Gitlab

Today I learned:
1) show Nginx docker-container for localhost 8080 (can be used for local projects) - sudo docker run -t -d -p 8080:80 --name nginx-baby nginx

---------------


2021-12-10
9th lecture in a row
3st lecture about Gitlab

Today I learned:
1) GPG keys. Added it to RIA and PariMatch projects


---------------


2021-12-09
Day-off


Today I learned:
5) For the first time in my life I've got Kubernetes dashboard. Got it by my own (to be honest by https://www.katacoda.com, but whatever... :D )
That's why I had no willing to fall asleep :) Worth it!


---------------


2021-12-08
8th lecture in a row
2st lecture about Gitlab

Today I learned:
1) Google boolean search - https://support.google.com/websearch/answer/2466433?hl=en
2) OhMyZsh for zsh
3) How to write readme correctly - https://editorconfig.org/
4) Easy codeshare with coded in Base64 data - https://github.com/topaz/paste \ https://topaz.github.io/paste/
5) Katacoda (kinda best learning-by-doing classes) - https://www.katacoda.com


---------------


2021-12-06
7th lecture in a row
1st lecture about Gitlab

Today I learned:
1) Bash functions. Donno how to add arguments. UPD: already know :D
2) know how put "pipes" as if-statement


---------------


2021-12-05
Day-off
None

Today I learned:
1) Zines - better way to understand hard concepts
    https://wizardzines.com/zines/bite-size-bash/
2) Vagrant init / Vagrant up / Vagrant ssh


---------------


2021-12-04
6th lecture in a row
1st lecture about Soft skills



Today I learned:
1) I found these commands 
    - tcpdump -t
    - ss -tulpn \ netstat -tulpn (might be used with watch - will be updated each 2 sec)
    - iftop

---------------


2021-12-03
5th lecture in a row
3rd lecture of Linux troubleshooting

Today I learned:
1) Google SRE books - https://sre.google/books/

2) Top commands for troubleshooting 
- uptime
- dmesg | tail
- vmstat 1
- mpstat -P ALL 1
- pidstat 1
- iostat -xz 1
- free -m
- sar -n DEV 1
- sar -n TCP,ETCP 1
- top
3) https://miro.medium.com/max/1000/1*nwNScSVlCA8lGl75YdA5hQ.png
4) Here is the source of netflixtech posts - https://brendangregg.com/linuxperf.html


---------------
