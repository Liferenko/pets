# Error
```
RuntimeError at POST /lab/protobuf/game_inventory/element/game\n\nException:\n\n    ** (RuntimeError) policy enforcement failed for request with Context=%LabFlask.Proto.Lab.Global.Context{site_id: nil, source_flask_name: \"game_admin\", source_flask_instance_uuid:
```

### Solution 
clear cookie. Might be LabAuth token issue 

---
# Error:
:observer.start() is not starting in IEx 

### Solution:
You can do extra_applications:

```
if Mix.env == :dev, do: [:observer, :debugger, :wx, :logger, :runtime_tools], else: [:logger, :runtime_tools]
```

after it in IEx use
```
>> :int.ni(Module.Full.Name)
>> :int.break(Module.Full.Name, %line_of_code, exp: 588%)
```

More here - https://blog.appsignal.com/2021/11/30/three-ways-to-debug-code-in-elixir.html




































---

# Error:
MatchError at GET /

Exception:

    ** (MatchError) no match of right hand side value: {:error, "not found"}
        (backoffice_base 1.50.2) lib/backoffice_base/components/user_menu.ex:64: BackofficeBase.Components.UserMenu."update (overridable 1)"/2
        (phoenix_live_view 0.19.5) lib/phoenix_live_view/utils.ex:487: Phoenix.LiveView.Utils.maybe_call_update!/3
        (phoenix_live_view 0.19.5) lib/phoenix_live_view/diff.ex:653: anonymous fn/5 in Phoenix.LiveView.Diff.render_pending_components/6
        (elixir 1.15.6) lib/enum.ex:2510: Enum."-reduce/3-lists^foldl/2-0-"/3
        (stdlib 5.1.1) maps.erl:416: :maps.fold_1/4
        (phoenix_live_view 0.19.5) lib/phoenix_live_view/diff.ex:629: Phoenix.LiveView.Diff.render_pending_components/6
        (phoenix_live_view 0.19.5) lib/phoenix_live_view/diff.ex:143: Phoenix.LiveView.Diff.render/3
        (phoenix_live_view 0.19.5) lib/phoenix_live_view/static.ex:252: Phoenix.LiveView.Static.to_rendered_content_tag/4
        (phoenix_live_view 0.19.5) lib/phoenix_live_view/static.ex:135: Phoenix.LiveView.Static.render/3
        (phoenix_live_view 0.19.5) lib/phoenix_live_view/controller.ex:39: Phoenix.LiveView.Controller.live_render/3
        (phoenix 1.7.7) lib/phoenix/router.ex:430: Phoenix.Router.__call__/5
        (pachislots_admin_flask 0.1.0) lib/pachislots_admin_flask_web/endpoint.ex:1: PachislotsAdminFlaskWeb.Endpoint.plug_builder_call/2
        (pachislots_admin_flask 0.1.0) deps/plug/lib/plug/debugger.ex:136: PachislotsAdminFlaskWeb.Endpoint."call (overridable 3)"/2
        (pachislots_admin_flask 0.1.0) lib/pachislots_admin_flask_web/endpoint.ex:1: PachislotsAdminFlaskWeb.Endpoint.call/2
        (phoenix 1.7.7) lib/phoenix/endpoint/sync_code_reload_plug.ex:22: Phoenix.Endpoint.SyncCodeReloadPlug.do_call/4
        (plug_cowboy 2.6.1) lib/plug/cowboy/handler.ex:11: Plug.Cowboy.Handler.init/2
        (cowboy 2.10.0) /Users/macbook/projects/YOLO/pachislots_admin_flask/deps/cowboy/src/cowboy_handler.erl:37: :cowboy_handler.execute/2
        (cowboy 2.10.0) /Users/macbook/projects/YOLO/pachislots_admin_flask/deps/cowboy/src/cowboy_stream_h.erl:306: :cowboy_stream_h.execute/3
        (cowboy 2.10.0) /Users/macbook/projects/YOLO/pachislots_admin_flask/deps/cowboy/src/cowboy_stream_h.erl:295: :cowboy_stream_h.request_process/3
        (stdlib 5.1.1) proc_lib.erl:241: :proc_lib.init_p_do_apply/3
    

Code:

`lib/backoffice_base/components/user_menu.ex`

    59             auth: %{claims: %{"sub" =&gt; admin_name, "subId" =&gt; admin_id}} = auth
    60           },
    61           socket
    62         ) do
    63       socket = assign(socket, assigns)
    64&gt;      {:ok, %{operator: %{name: operator_name}}} = BackofficeBase.Utils.get_user(auth)
    65       admin_roles = BackofficeBase.Utils.get_all_roles(auth)
    66       user_menu = get_user_menu(admin_roles, admin_id)
    67   
    68       {:ok,
    69        assign(socket,


















































---

# Error
```
[error] #PID<0.21270.1> running Phoenix.Endpoint.SyncCodeReloadPlug (connection #PID<0.21269.1>, stream id 1) terminated
Server: localhost:4018 (http)
Request: POST /users/log_in
** (exit) an exception was raised:
    ** (ArgumentError) construction of binary failed: segment 1 of type 'binary': expected a binary but got: nil
        (admin_flask 0.1.1) lib/admin_flask_web/user_auth.ex:369: AdminFlaskWeb.UserAuth.get_external_url/3
        (admin_flask 0.1.1) lib/admin_flask_web/user_auth.ex:85: AdminFlaskWeb.UserAuth.log_in_user/3
        (admin_flask 0.1.1) lib/admin_flask_web/controllers/user_session_controller.ex:1: AdminFlaskWeb.UserSessionController.action/2
        (admin_flask 0.1.1) lib/admin_flask_web/controllers/user_session_controller.ex:1: AdminFlaskWeb.UserSessionController.phoenix_controller_pipeline/2
        (phoenix 1.7.7) lib/phoenix/router.ex:430: Phoenix.Router.__call__/5
        (admin_flask 0.1.1) lib/admin_flask_web/endpoint.ex:1: AdminFlaskWeb.Endpoint.plug_builder_call/2
        (admin_flask 0.1.1) lib/plug/debugger.ex:136: AdminFlaskWeb.Endpoint."call (overridable 3)"/2
        (admin_flask 0.1.1) lib/admin_flask_web/endpoint.ex:1: AdminFlaskWeb.Endpoint.call/2
        (phoenix 1.7.7) lib/phoenix/endpoint/sync_code_reload_plug.ex:22: Phoenix.Endpoint.SyncCodeReloadPlug.do_call/4
        (plug_cowboy 2.6.1) lib/plug/cowboy/handler.ex:11: Plug.Cowboy.Handler.init/2
        (cowboy 2.10.0) /Users/oxiv/test_repos/admin_flask/deps/cowboy/src/cowboy_handler.erl:37: :cowboy_handler.execute/2
        (cowboy 2.10.0) /Users/oxiv/test_repos/admin_flask/deps/cowboy/src/cowboy_stream_h.erl:306: :cowboy_stream_h.execute/3
        (cowboy 2.10.0) /Users/oxiv/test_repos/admin_flask/deps/cowboy/src/cowboy_stream_h.erl:295: :cowboy_stream_h.request_process/3
        (stdlib 4.2) proc_lib.erl:240: :proc_lib.init_p_do_apply/3
```

### Solution





















































---
SOLVED
# Error:
```
│ Warning | FailedUpdateRegistrationToken | 45s (x8021 over 3d21h) | runner-controller | Updating registration token failed
```

### Solution
just recreate runners namespace, it was removed for some reason






























---

SOLVED!
# Error:
```
ERROR! the application :pa**islots_admin_fl**k has a different value set for key :site_id during runtime compared to compile time. Since this application environment entry was marked as compile time,  
* Compile time value was set to: 1                                                                                                                                                                     
* Runtime value was set to: 17                                                                                                                                                                         

To fix this error, you might:                                                                                                                                                                       
* Make the runtime value match the compile time one                                                                                                                                                    
* Recompile your project. If the misconfigured application is a dependency, you may need to run "mix deps.compile pa**islots_admin_fl**k --force"                                               
* Alternatively, you can disable this check. If you are using releases, you can set :validate_compile_env to false in your release configuration. If you are using Mix to start your system, you can p 
```


### Solution

1. try to sync-up :site_id in both config/config.ex and config/runtime.ex

```
site_id: String.to_integer(System.get_env("***_SITE_ID", "1"))
```

2. try to acoid caching while deploy&build are going. Might be useful to update CACHE_VERSION_BUILD in .github/workflows

3. Looks like github-runner is not up and that's why it can't delivery the latest release
```
k get pods -n pachislots-admin-flask-namespace
No resources found in pachislots-admin-flask-namespace namespace.
```

4. WORKED
avoid to mention :site_id in config/config.ex AND config/runtime.ex. It's better to set it in config/dev.ex, config/test.ex and config/runtime.ex




































---

# Error:

 ```
 [error] error: %La*Fl**k..Proto.Lab.Substance.Config.Reaction.CreateSiteConfig.Response{either: {:error, %La*Fl**k..Proto.Lab.Substance.Config.Reaction.CreateSiteConfig.Response.Error{key: nil, value: :INVALID_VALUE, __unknown_fields__: []}}, __unknown_fields__: []}, for config: %PachislotsAdminFlask.Actions.CreateConfig{key: "Elixir.PachislotsAdminFlask.Flask", value: "http://localhost:4018"}
 [error] error: %La*Fl**k..Proto.Lab.Substance.Config.Reaction.CreateSiteConfig.Response{either: {:error, %La*Fl**k..Proto.Lab.Substance.Config.Reaction.CreateSiteConfig.Response.Error{key: :ALREADY_EXISTS, value: nil, __unknown_fields__: []}}, __unknown_fields__: []}, for config: %PachislotsAdminFlask.Actions.CreateConfig{key: "Elixir.PachislotsAdminFlask.Flask", value: "\"http://localhost:4018\""}
 [error] error: %La*Fl**k..Proto.Lab.Substance.Config.Reaction.CreateSiteConfig.Response{either: {:error, %La*Fl**k..Proto.Lab.Substance.Config.Reaction.CreateSiteConfig.Response.Error{key: :ALREADY_EXISTS, value: nil, __unknown_fields__: []}}, __unknown_fields__: []}, for config: %PachislotsAdminFlask.Actions.CreateConfig{key: "Elixir.PachislotsAdminFlask.Flask", value: "3"}
```

### Solution
add error handling 


















---

# Error:

```
{
    :error,
    {
        %UndefinedFunctionError{module: :pa**islots_admin_fl**k, function: :override_context_fields, arity: 1, reason: nil, message: nil},
        [{:pa**islots_admin_fl**k, :override_context_fields,
            [%La*Fl**k..Proto.Lab.Global.Context{site_id: 1, source_flask_name: nil, source_flask_instance_uuid: nil, subject_info: nil, nonce: 0, operator_id: 1, trace_info: nil,
                either: {:element, %La*Fl**k..Proto.Lab.Global.Context.ElementContext{target_flask_name: nil, target_flask_instance_uuid: nil, flask_timeout: nil, timeout: nil, __unknown_fields__: []}}, __unknown_fields__: []}], []},
                {PachislotsAdminFlask.Data.Configs, :load, 0, [file: ~c"lib/pa**islots_admin_fl**k/data/configs.ex", line: 56]},
                {PachislotsAdminFlask.Data.ConfigsTest, :"test load/0.  get data", 1, [file: ~c"test/pa**islots_admin_fl**k/data/config_test.exs", line: 7]}, {ExUnit.Runner, :exec_test, 2, [file: ~c"lib/ex_unit/runner.ex", line: 463]}, {:timer, :tc, 2, [file: ~c"timer.erl", line: 270]}, {ExUnit.Runner, :"-spawn_test_monitor/4-fun-1-", 5, [file: ~c"lib/ex_unit/runner.ex", line: 385]}]}}
```

Solution: doublecheck test/suppport/substance_mocks - in my case I've been using `function/3` but in mocks it was `function/2`












































































---

# Error:
Datadog, kubernetes, pods, namespaces. Can't wire them together

##### Suberrors: 
- `Logger.error("Tried to finish a trace without an active trace."`
- `Handler "pa**islots_fl**k-repo" has failed and has been detached. Class=:error Reason=:undef Stacktrace=[ {La*Fl**k..StubTracer, :current_trace_id, [], []}, {SpandexEcto.EctoLogger, :trace, 3, [file: 'lib/spandex_ecto/ecto_logger.ex'`




### Clues 
- https://docs.datadoghq.com/containers/kubernetes/apm/?tab=operator
- all Datadog events - https://app.datadoghq.eu/event/explorer?query=cluster_name%3Alab-platform-cluster&cols=&messageDisplay=expanded-lg&options=&refresh_mode=paused&sort=DESC&view=spans&from_ts=1698066720000&to_ts=1698066730000&live=false

- maybe the API key and APP key saved in secrets unencoded. I mean it literally just store there as-is. All other values are base64'd
Example:
```
  aws-access-key-id: QUtJQVIyTz*************OUzI=
  aws-secret-access-key: YlBmTU***********************************************Q==
  datadog-secret-api-key: 03703*************************5c
  datadog-secret-app-key: 155**********************************419
```

- might be crucial part to add this as a values.yaml
``
# TODO need to add it somewhere in kubernetes/
 targetSystem: linux
 
 datadog:
   apm:
     portEnabled: true
```


```
Use the Datadog Admission Controller to inject environment variables and mount the necessary volumes on new application pods, automatically configuring pod and Agent trace communication. Learn how to automatically configure your application to submit traces to Datadog Agent by reading the Injecting Libraries Using Admission Controller documentation.
```


## Hypothesys:
- remove `host` and `port` from a flask. DD_HOST and DD_PORT will be injected by Datadog admission controller

# Final solution





























































---
#### Error: 
k8s namespace is terminating and cant finish it properly. 

Tried:
- `k delete namespace <NAMESPACE> --force`. DId nothing because of connected agents

### Solution: 
here - https://www.ibm.com/docs/en/cloud-private/3.2.0?topic=console-namespace-is-stuck-in-terminating-state#manually-delete-a-terminating-namespace

TLDR; remove finalizer: kubernetes from namespace's yaml config. It will help to shut the namespace down properly.


---
#### Error: 
anytime you see compilation error - make `rm -r _build/`




---
#### Error: 
```
➜  pa**islots_fl**k git:(feature/add_datadog_tracers) asdf install erlang 25.3
asdf_25.3 is not a kerl-managed Erlang/OTP installation
The asdf_25.3 build has been deleted
Extracting source code
Building Erlang/OTP 25.3 (asdf_25.3), please wait...
Configure failed.
checking for javac... javac
checking for JDK version 1.6... no
configure: WARNING: Could not find any usable java compiler, will skip: jinterface
checking for log2... yes
checking CFLAGS for -O switch... configure: error:
        CFLAGS must contain a -O flag. If you need to edit the CFLAGS you probably
        also want to add the default CFLAGS. The default CFLAGS are "-O2 -g".
        If you want to build ERTS without any optimization, pass -O0 to CFLAGS.
ERROR: /Users/macbook/.asdf/plugins/erlang/kerl-home/builds/asdf_25.3/otp_src_25.3/erts/configure failed!
./configure: line 370: kill: (-45711) - No such process

Please see /Users/macbook/.asdf/plugins/erlang/kerl-home/builds/asdf_25.3/otp_build_25.3.log for full details.
```

Solution:
UPD: try this - https://dev.to/zacky1972/perfect-steps-of-installalling-erlang-and-elixir-to-apple-silicon-mac-2021-dec-edition-4iae
maybe
```
➜  pa**islots_fl**k git:(feature/add_datadog_tracers) export KERL_CONFIGURE_OPTIONS="--disable-debug \
                               --disable-hipe \
                               --disable-sctp \
                               --disable-silent-rules \
                               --enable-darwin-64bit \
                               --enable-dynamic-ssl-lib \
                               --enable-kernel-poll \
                               --enable-shared-zlib \
                               --enable-smp-support \
                               --enable-threads \
                               --enable-wx \
                               --with-wx-config=/usr/local/bin/wx-config \
                               --without-javac \
                               --without-jinterface \
                               --without-odbc"
➜  pa**islots_fl**k git:(feature/add_datadog_tracers) asdf install
elixir 1.14.3-otp-25 is already installed
asdf_25.3 is not a kerl-managed Erlang/OTP installation
The asdf_25.3 build has been deleted
Extracting source code
Building Erlang/OTP 25.3 (asdf_25.3), please wait...
Configure failed.
checking for a compiler that handles jumptables... clang
checking for kstat_open in -lkstat... (cached) no
checking for kvm_open in -lkvm... no
checking for log2... yes
checking CFLAGS for -O switch... configure: error:
        CFLAGS must contain a -O flag. If you need to edit the CFLAGS you probably
        also want to add the default CFLAGS. The default CFLAGS are "-O2 -g".
        If you want to build ERTS without any optimization, pass -O0 to CFLAGS.
ERROR: /Users/macbook/.asdf/plugins/erlang/kerl-home/builds/asdf_25.3/otp_src_25.3/erts/configure failed!
./configure: line 370: kill: (-75779) - No such process

Please see /Users/macbook/.asdf/plugins/erlang/kerl-home/builds/asdf_25.3/otp_build_25.3.log for full details.
➜  pa**islots_fl**k git:(feature/add_datadog_tracers) export CFLAGS="-O2 -g"
➜  pa**islots_fl**k git:(feature/add_datadog_tracers) asdf install erlang 25.3

```

3)
```
# Resolve macOS+Erlang25.3 issue
export CFLAGS="-O2 -g"
export CPPFLAGS="-I/opt/homebrew/opt/openjdk@11/include"
export KERL_CONFIGURE_OPTIONS="--disable-jit"
```
4) `brew install openjdk@11`













############## Something new
```
➜  lab_platform_flask_template git:(development) ✗ asdf install erlang 25.3
asdf_25.3 is not a kerl-managed Erlang/OTP installation
The asdf_25.3 build has been deleted
Extracting source code
Building Erlang/OTP 25.3 (asdf_25.3), please wait...
APPLICATIONS DISABLED (See: /Users/macbook/.asdf/plugins/erlang/kerl-home/builds/asdf_25.3/otp_build_25.3.log)
 * jinterface     : Java compiler disabled by user
 * jinterface     : User gave --without-jinterface option
 * odbc           : User gave --without-odbc option

APPLICATIONS INFORMATION (See: /Users/macbook/.asdf/plugins/erlang/kerl-home/builds/asdf_25.3/otp_build_25.3.log)
 * wx             : wxWidgets was not compiled with --enable-webview or wxWebView developer package is not installed, wxWebView will NOT be available
 *         wxWidgets must be installed on your system.
 *         Please check that wx-config is in path, the directory
 *         where wxWidgets libraries are installed (returned by
 *         'wx-config --libs' or 'wx-config --static --libs' command)
 *         is in LD_LIBRARY_PATH or equivalent variable and
 *         wxWidgets version is 3.0.2 or above.

Build failed.
  _RSA_padding_check_SSLv23, referenced from:
      _pkey_crypt_nif in pkey.o
ld: warning: search path '/lib' not found
clang: error: linker command failed with exit code 1 (use -v to see invocation)
make[4]: *** [../priv/lib/aarch64-apple-darwin23.0.0/crypto.so] Error 1
make[4]: *** Waiting for unfinished jobs....
make[3]: *** [opt] Error 2
make[2]: *** [opt] Error 2
make[1]: *** [opt] Error 2
make: *** [libs] Error 2

Please see /Users/macbook/.asdf/plugins/erlang/kerl-home/builds/asdf_25.3/otp_build_25.3.log for full details.
Removing all artifacts except the logfile
(Use KERL_AUTOCLEAN=0 to keep build on failure, if desired)
Cleaning up compilation products for asdf_25.3
Cleaned up compilation products for asdf_25.3 under /Users/macbook/.asdf/plugins/erlang/kerl-home/builds
```
---





# One more new step
`brew unlink openssl@3 && brew link openssl@3`































#### Error: 
Cannot index event publisher.Event{Content:beat.Event{Timestamp:time.Date(2023, time.September, 21, 3, 23, 55, 542071377, time.UTC), Meta:{"pipeline":"filebeat-8.7.0-kibana-audit-pipeline"}, Fields:{"agent":{"ephemeral_id":"cff6025c-2989-4fa*******************","id":"3538adee************************0368","name":"ip-10-*******************************ternal","type":"filebeat","version":"8.7.0"},"container":{"id":"69ab55eaa79cf4f3bf6f4e71f89***********************31e319d47f5080","image":{"name":"docker.elastic.co/kibana/kibana:8.7.0"},"runtime":"containerd"},"ecs":{"version":"1.12.0"},"error":{"message":"Error decoding JSON: invalid character '-' after array element","type":"json"},"event":{"dataset":"kibana.audit","module":"kibana","timezone":"+00:00"},"fileset":{"name":"audit"},"host":{"name":"*****-10.eu*********************ternal"},"input":{"type":"container"},"kubernetes":{"container":{"name":"kibana"},"labels":{"common_k8s_elastic_co/type":"kibana","kibana_k8s_elastic_co/name":"eck","kibana_k8s_elastic_co/version":"8.7.0","pod-template-hash":"c5*****d7"},"namespace":"************-template-development-namespace","namespace_labels":{"kubernetes_io/metadata_name":"************-template-development-namespace","name":"************-template-development-namespace"},"namespace_uid":"4c54a677-****************762c2a552dd","node":{"hostname":"*****-10.eu-central-1.compute.internal","labels":{"beta_kubernetes_io/arch":"amd64","beta_kubernetes_io/instance-type":"m5n.2xlarge","beta_kubernetes_io/os":"linux","eks_amazonaws_com/capacityType":"SPOT","eks_amazonaws_com/nodegroup":"mng-spot-xlarge-20230713184002327400000001","eks_amazonaws_com/nodegroup-image":"ami-0fa4ff16bf6d9adfc","failure-domain_beta_kubernetes_io/region":"eu-central-1","failure-domain_beta_kubernetes_io/zone":"eu-central-1b","k8s_io/cloud-provider-aws":"3e80c52d52ef396443c35ec8472b8c0a","kubernetes_io/arch":"amd64","kubernetes_io/hostname":"*****-10.eu-central-1.compute.internal","kubernetes_io/os":"linux","node_kubernetes_io/instance-type":"m5n.2xlarge","topology_kubernetes_io/region":"eu-central-1","topology_kubernetes_io/zone":"eu-central-1b"},"name":"*****-10.eu-central-1.compute.internal","uid":"9bcbc0b3-cc34-4f58-9a17-ff859f847dde"},"pod":{"ip":"10.0.11.74","name":"ec****************l7gq","uid":"6d0a0c54-c513-436b-a967-1674c20a6c33"},"replicaset":{"name":"eck-kb-c5964dbd7"}},"log":{"file":{"path":"/var/log/containers/eck-kb-c5964dbd7-kl7gq_************-template-development-namespace_kibana-69ab************************************1069a3ccac31e319d47f5080.log"},"offset":80872},"message":"[2023-09-21T03:23:55.541+00:00][ERROR][elasticsearch-service] Unable to retrieve version information from Elasticsearch nodes. Request timed out","service":{"type":"kibana"},"stream":"stdout"}, Private:file.State{Id:"native::34669603-66305", PrevId:"", Finished:false, Fileinfo:(*os.fileStat)(0xc002106340), Source:"/var/log/containers/eck-kb-c5964dbd7-kl7gq_************-template-development-namespace_kibana-69ab55eaa79c**********************************ccac31e319d47f5080.log", Offset:81057, Timestamp:time.Date(2023, time.September, 21, 3, 26, 44, 819469398, time.Local), TTL:-1, Type:"container", Meta:map[string]string(nil), FileStateOS:file.StateOS{Inode:0x2110423, Device:0x10301}, IdentifierName:"native"}, TimeSeries:false}, Flags:0x1, Cache:publisher.EventCache{m:mapstr.M(nil)}} (status=400): {"type":"mapper_parsing_exception","reason":"failed to parse field [@timestamp] of type [date] in document with id 'VpfGtYoBA_wAJ71ylDq4'. Preview of field's value: ''","caused_by":{"type":"illegal_argument_exception","reason":"cannot parse empty date"}}, dropping event!

Solution:
add json.ignore_decoding_error in filebeat.yml
source - https://github.com/elastic/beats/pull/6547


---
#### Error: Kibana pod has an "failed to read message" error, after Elastic pod restarting - pod's status is Padding, pod can't start again
Suberror: Elastic pod is in pending status and cant be restarted
Suberror: PV is terminating. Can't properly finish
might-be-solution: `kubectl scale deployment <deployment-name> --replicas=0` to stop deployment
subsolution: `k delete -f kubernetes/eck/development/elastic.yaml` helped to stop pending pods


new suberror after pod restarted: `  Warning  FailedScheduling  8m11s  default-scheduler  running PreBind plugin "VolumeBinding": Operation cannot be fulfilled on persistentvolumeclaims "elasticsearch-data-eck-es-default-0": the object has been modified; please apply your changes to the latest version and try again`

subsolution: seems like the issue laid in volumeClaimTemplates - it should be `elasticsearch-data` and PVC name should starts with the same template name - elasticsearch-data-eck-es-default-blah-blah

New issue: filebeat pods are not started yet
Subsolution: seems like FIlebeat's cluster-role should be in the same file as it was before clean-up
Subissue:   Warning  FailedDeployModel  6m35s  ingress  Failed deploy model due to AccessDenied: User: arn:aws:sts::1255*****095:assumed-role/lab-platform-cluster-aws-load-balancer-controller-sa-irsa/169*********0521571 is not authorized to perform: elasticloadbalancing:AddTags on resource: arn:aws:elasticloadbalancing:eu-central-1:12*********5:targetgroup/k8s-logging-eckkbhtt-83****2330/* because no identity-based policy allows the elasticloadbalancing:AddTags action
           status code: 403, request id: 82******-761e-4fcc-bb1f-01cc44214fc7

Solution:
looks like well-known issue which already has been resolved by DevOps team

It was resolved. DevOps issue 

here is a patch for this error
```
# # ---- AWS Load Balancer Controller Patch ----
# # ---- Workaround for https://github.com/aws-ia/terraform-aws-eks-blueprints-addons/issues/200 ----
# data "aws_iam_policy_document" "aws_lb" {
#   statement {
#     sid    = ""
#     effect = "Allow"
#     resources = [
#       "arn:aws:elasticloadbalancing:*:*:targetgroup/*/*",
#       "arn:aws:elasticloadbalancing:*:*:loadbalancer/net/*/*",
#       "arn:aws:elasticloadbalancing:*:*:loadbalancer/app/*/*"
#     ]

#     actions = [
#       "elasticloadbalancing:AddTags",
#     ]

#     condition {
#       test = "StringEquals"
#       values = [
#         "CreateTargetGroup",
#         "CreateLoadBalancer"
#       ]
#       variable = "elasticloadbalancing:CreateAction"
#     }

#     condition {
#       test     = "Null"
#       values   = ["false"]
#       variable = "aws:RequestTag/elbv2.k8s.aws/cluster"
#     }
#   }
# }

# resource "aws_iam_policy" "eks_aws_lb_patch_policy" {
#   name        = "${var.cluster_name}-aws-lb-patch"
#   description = "Patch Policy for AWS Load Balancer Controller"
#   policy      = data.aws_iam_policy_document.aws_lb.json
# }

# resource "aws_iam_policy_attachment" "eks_aws_lb_patch_attachment" {
#   depends_on = [module.eks_blueprints_kubernetes_addons]
#   name       = "${var.cluster_name}-aws-lb-patch"
#   policy_arn = aws_iam_policy.eks_aws_lb_patch_policy.arn
#   roles      = ["${var.cluster_name}-aws-load-balancer-controller-sa-irsa"]
# }
```

---

#### `** (DBConnection.EncodeError) Postgrex expected %DateTime{}, got ~N[2023-08-19 14:02:39]. Please make sure the value you are passing matches the definition in your table or in your query or convert the value accordingly.`

Solution:
