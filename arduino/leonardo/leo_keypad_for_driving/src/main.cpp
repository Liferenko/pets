#include <Arduino.h>
#include <Keypad.h>
#include <Keyboard.h>

/*
TODO: 
- add handbrake - https://github.com/dmadison/Sim-Racing-Arduino/blob/master/examples/Handbrake/HandbrakeJoystick/HandbrakeJoystick.ino


*/

// Keypad
// #define Password_Length 8

// char Data[Password_Length];
// char Master[Password_Length] = "1111111";
int data_count;
const byte Rows = 4;
const byte Cols = 4;

int delayTime = 100;

// char hexaKeys[Rows][Cols] = {
//   {'!', '@', '$', '%'},
//   {'&', '*', '+', '_'},
//   {')', '(', ':', 'V'},
//   {'*', '0', '#', 'D'}
// };

byte hexaKeys[Rows][Cols] = {
  {0xFB, 0xFA, 0xF9, 0xF8},
  {0xF7, 0xF6, 0xF5, 0xF4},
  {0xF3, 0xCF, 0xCE, 0xF0},
  {0xEF, 0xCC, 0xED, 0xEC}
};

byte r1 = 5;
byte r2 = 4;
byte r3 = 3;
byte r4 = 2;

byte c1 = 9;
byte c2 = 8;
byte c3 = 7;
byte c4 = 6;


byte rowPins[Rows] = {r1, r2, r3, r4};
byte colPins[Cols] = {c1, c2, c3, c4};

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, Rows, Cols);

void setup() {
  Serial.begin(9600);

  Keyboard.begin();
}

void loop() {
  char customKey = customKeypad.getKey();

  if(customKey) {
    Keyboard.write(customKey);
    Serial.print("You pressed: ");
    Serial.println(customKey);
  }

  // if(Serial.available()) {
  //   char data = Serial.read();
  //   Keyboard.write(data);
  // }
}
