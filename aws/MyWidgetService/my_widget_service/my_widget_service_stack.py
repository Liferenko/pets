from aws_cdk import (
    # Duration,
    Stack,
    # aws_sqs as sqs,
)
from constructs import Construct
from .widget_service import WidgetService
#TODO REMOVE BEFORE FLIGHT!!!!!!

class MyWidgetServiceStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        WidgetService(self, "Widgets")
