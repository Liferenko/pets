from aws_cdk import (
    # Duration,
    Stack,
    aws_s3,
    aws_lambda,
    aws_apigateway
)
from constructs import Construct

class WidgetService(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        bucket = aws_s3.Bucket.from_bucket_name(self, "WidgetStore", "todo-bucket-name")
        handler = aws_lambda.Function(
                self,
                "WidgetHandler",
                runtime=aws_lambda.Runtime.NODEJS_12_X,
                code=aws_lambda.Code.from_asset("resources"),
                handler="widgets.main",
                environment={'BUCKET': "todo-bucket-name"}
        )

        bucket.grant_read_write(handler)

        api = aws_apigateway.RestApi(self, "hello-api",
                rest_api_name="Widget Service",
                description="This service serves widgets."
        )

        get_widgets_integration = aws_apigateway.LambdaIntegration(handler,
                request_templates={"application/json": '{"statusCode": 200}'}
        )

        api.root.add_method("GET", get_widgets_integration)
