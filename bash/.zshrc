# If you come from bash you might have to change your $PATH.




#### DANGER ZONE

export GH_PAT="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" # Expires Nov 3 2023
####################

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
export OPENAI_API_KEY="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"
# export TMUX_PLUGIN_MANAGER_PATH="$HOME/.tmux/plugins/"
export PATH=/opt/homebrew/bin/brew:$PATH
# for dart
export PATH="$PATH":"$HOME/.pub-cache/bin"

# for lazygit
export PATH="/usr/local/bin:$PATH"

# for anaconda
export PATH="/opt/homebrew/anaconda3/bin/:$PATH"

# for Sigmath
# export OPUS_PATH=`brew --prefix opus`
# export C_INCLUDE_PATH=$C_INCLUDE_PATH:$OPUS_PATH/include
# export LIBRARY_PATH=$LIBRARY_PATH:$OPUS_PATH/lib



# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
ulimit -S -n 2048

VSCODE=code
export FZF_BASE=/opt/homebrew/bin/fzf
plugins=(
  zsh-autosuggestions
  git
  vscode
  fzf
  asdf
  kubectl
  aws
  docker
)

# K8s
source <(kubectl completion zsh)

# remap key for autosuggestions
bindkey '^ ' autosuggest-accept

# source "$HOME/.tmux.conf"
source $ZSH/oh-my-zsh.sh

# FZF


# User configuration

export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias ctags=/opt/homebrew/bin/ctags
alias scan_ctags="ctags -R --exclude=_build --exclude=node_modules --exclude=assets --exclude='*.js' --exclude='*.html' ."
alias e=nvim
alias ee="scan_ctags ; nvim"
alias \?="$HOME/pets/rust/question_mark/question_mark"
#alias til="~/pets/bash/notetaker_per_project.sh TIL"
alias til="nvim ~/til"
alias daily="~/pets/bash/notetaker_per_project.sh Yolo"
alias tlog="~/pets/bash/notetaker_per_project.sh errors-and-troubleshooting"
# alias til="nvim ~/pets/100daysOfRust.md"
# old remove after 2023-12-15 alias tlog="nvim ~/pets/TROUBLESHOOTINGLOG.md"
alias k=kubectl

# Knowledge base (kb)
alias kbl="kb list"
alias kbe="kb edit"
alias kba="kb add"
alias kbv="kb view"
alias kbd="kb delete --id"
alias kbg="kb grep"
alias kbt="kb list --tags"

alias livebook="/Users/macbook/.asdf/installs/elixir/1.15.6-otp-26/.mix/escripts/livebook"

# Resolve macOS+Erlang25.3 issue
export CFLAGS="-O2 -g"
export CPPFLAGS="-I/opt/homebrew/opt/openjdk@11/include"
export PATH="/opt/homebrew/opt/openjdk/bin:$PATH"

### For Sigmath project
export PKG_CONFIG_PATH="/opt/homebrew/opt/openssl@3/lib/pkgconfig"
export LDFLAGS="-L/opt/homebrew/opt/openssl@3/lib"
export CPPFLAGS="-I/opt/homebrew/opt/openssl@3/include"
export CFLAGS="-I/opt/homebrew/opt/openssl@3/include"
export C_INCLUDE_PATH=:/include:/opt/homebrew/Cellar/opus/1.3.1/include
export LIBRARY_PATH=:/lib:/usr/local/opt/openssl/lib/:/opt/homebrew/Cellar/opus/1.3.1/lib
export OPUS_PATH=/opt/homebrew/opt/opus
export C_INCLUDE_PATH=:/include:/opt/homebrew/opt/opus/include
export LIBRARY_PATH=:/lib:/usr/local/opt/openssl/lib/:/opt/homebrew/opt/opus/lib
export PATH="/opt/homebrew/opt/postgresql@16/bin:$PATH"

PATH="/Users/macbook/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/Users/macbook/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/Users/macbook/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/Users/macbook/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/Users/macbook/perl5"; export PERL_MM_OPT;
