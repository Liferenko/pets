#!/bin/sh

noteFilename="$HOME/Documents/GDrive/notes/notes-$1.md"

if [ ! -f $noteFilename ]; then
    echo "# Notes for $1 project" > $noteFilename
else
    echo "# Notes for Today I learned" > "$HOME/Documents/GDrive/notes/today_i_learned.md"
fi

nvim -c "norm gg4o" \
    -c "norm ggo## $(date +%Y-%m-%d) $(date +%H:%M)" \
    -c "norm o### Duration: " \
    $noteFilename
