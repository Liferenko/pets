#!/bin/bash

# install Vim + git
sudo apt update && apt-get install -y \
neovim \
build-essential \
vim \
snap \
tree \
python3 \
translate-shell \
w3m \
nodejs \
npm \
mysql-server \
asdf \
tmux \
net-tools \
inotify-tools \
ctags \
feh \
openconnect \
network-manager-openconnect \
network-manager-openconnect-gnome \
postgresql \
postgresql-contrib \
compton \
compton-conf

#### For Vim\NVim plugins PlugInstall
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# Install Minikube for K8s 
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
sudo snap install kubectl


# Install Workbench for 
# In case Workbench will talk some shit again - try that solutin (https://askubuntu.com/questions/1246091/problems-with-mysql-workbench-for-ubuntu-20-04)

# Install Insomnia (API testing tools)
#sudo snap install insomnia
# UPDATE: postman CLI (aka newman)
# usage: newman run postman_exported_collection.json -r cli-response

# Touchpad tap-to-click

# when I'll be needed to open localhost on a phone 
# 1 - ifconfig and use ip4
# 2 - >>> sudo ufw disable

# to check Wifi networks and to connect to any of them - nm-applet

# sudo - Canadian style 
alias please="sudo"

# Cursor delay speed

# Space as i3 mod key
#mv .Xmodmap /home/$USER/.Xmodmap

# new one: Install TUI db manager - Gobang 
cd 
wget https://github.com/TaKO8Ki/gobang/releases/download/v0.1.0-alpha.5/gobang-0.1.0-alpha.5-x86_64-unknown-linux-musl.tar.gz
tar xvf gobang-0.1.0-alpha.5-x86_64-unknown-linux-musl.tar.gz
# deprecated! PostgreSQL/MySQL CLI with autocompletion ([source](https://github.com/dbcli/pgcli))
# pip3 install -U pgcli mycli


# I should carefully add last part of my previous .profile to new .profile
#MANUALLY mv .profile /home/$USER/.profile


# Install Erlang/OTP
#git clone https://github.com/erlang/otp.git
#cd otp
#./otp_build autoconf
#./configure
#make
#make install
#cd

# Install Erlang/OTP/Elixir with asdf (source - https://gist.github.com/HangingClowns/3b4ee68e4343371bc6340b49f119aee0)
sudo apt-get install -y build-essential git wget libssl-dev libreadline-dev libncurses5-dev zlib1g-dev m4 curl wx-common libwxgtk3.0-dev autoconf

cd
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.6.3

# For Ubuntu or other linux distros
echo '. $HOME/.asdf/asdf.sh' >> ~/.bashrc
echo '. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc

asdf plugin-add erlang
asdf plugin-add elixir

asdf install erlang 23.2
asdf install elixir 1.14

asdf global erlang 23.2
asdf global elixir 1.14




### i3-gaps installation
# sauce - https://www.youtube.com/watch?v=7RNgpvBMua0

### Configurate MySQL


### Start RabbitMQ

### Install telegram

cd
### Install Docker CE
snap install docker

#@depricated
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
apt-cache madison docker-ce


# pull my configure Vim file
# git clone %URL%


# Offline documentation Zeal (for Ubuntu) or Dash (for MacOS)
# TODO make if-state (if os == Linux => sudo apt-get install zeal). else brew install dash
#sudo apt-get install zeal
#sudo add-apt-repository ppa:zeal-developers/ppa
#sudo apt-get update
#sudo apt-get install zeal




# All-for-VIM 
## plugins
cd
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim




# Bluetooth soft

# Clone Tmux plugin manager 
# source for restart saved sessions - https://github.com/tmux-plugins/tmux-resurrect
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# To not save all .orig-files 
git config --global mergetool.keepBackup false

