#!/bin/bash

# using ffmpeg to speed up video
#
# Usage:
#  speedup_video.sh <input_video_path> <speedup_factor> <output_video_name>

#convert in raw.h264
ffmpeg -i $1 -map 0:v -c:v copy -bsf:v h264_mp4toannexb raw.h264

# convert with audio
#ffmpeg -i $1 -filter:v "setpts=0.5*PTS" -filter:a "atempo=1.4" $3

# speed up
ffmpeg -fflags +genpts -r $2 -i raw.h264 -c:v copy $3
