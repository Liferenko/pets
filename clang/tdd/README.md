# Curiosity feeding - Is it possible to write TDD in Clang?


[Source link](http://www.jera.com/techinfo/jtns/jtn002.html#Introduction)

*Insights:*
- "People think that writing a unit testing framework has to be complex. In fact, you can write one in just a few lines of code, as this tech note shows."
- "There's no excuse for not unit testing."(c)
- `gcc %filename.c% -o %output_name_of_file% && ./output_name_of_file`

---
`make compile_and_run` to run tests
---

# TODO:
- nice to try: [bowling_score with TDD](https://www.slideshare.net/amritayan/test-driven-development-in-c)
