#include <stdio.h>
#include "minunit.h"

int tests_run = 0;
int days_in_week = 7;
int number_of_weeks = 4;

float wrong_number = 4.0;

static char * test_days() {
    mu_assert("error, days in a week equal 7", days_in_week == 7);
    return 0;
}

static char * test_days_cant_be_float() {
    mu_assert("error, days can't be floar number", days_in_week != wrong_number);
    return 0;
}

static char * test_number_of_days_in_week() {
    mu_assert("error, days_in_week can\'t be greater then 7", days_in_week <= 7);
    return 0;
}

static char * test_number_of_weeks() {
    mu_assert("error, number of weeks in  month equals 4", number_of_weeks == 4);
    return 0;
}

static char * test_number_of_weeks_greater_then_7() {
    mu_assert("error, number of weeks can\'t be greater then 4", !(number_of_weeks > 4));
    return 0;
}

static char * all_tests() {
    mu_run_test(test_number_of_days_in_week);
    mu_run_test(test_number_of_weeks);
    mu_run_test(test_number_of_weeks_greater_then_7);
    mu_run_test(test_days);
    mu_run_test(test_days_cant_be_float);

    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);

    return result != 0;
}
