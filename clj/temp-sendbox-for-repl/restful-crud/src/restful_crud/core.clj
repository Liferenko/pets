(ns restful-crud.core
  (:require [toucan.db :as db]
            [toucan.models :as models]
            [ring.adapter.jetty :refer [run-jetty]]
            [compojure.api.sweet :refer [api routes]]
            [restful-crud.patient :refer [patient-routes]])
  (:gen-class))

(def db-spec
  {:dbtype "postgres"
   :dbname "tiny-cliniq"
   :user "tinydev"
   :password "dummypassweneedtochangeandsaveinconfigs"})
 
(def app (api (apply routes patient-routes)))

(defn -main
  [& args]
  (db/set-default-db-connection! db-spec)
  (models/set-root-namespace! 'restful-crud.models)
  (run-jetty app {:port 8080}))
