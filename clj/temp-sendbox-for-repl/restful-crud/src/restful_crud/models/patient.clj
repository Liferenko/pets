(ns restful-crud.models.patient
  (:require [toucan.models :refer [defmodel]]))

(defmodel Patient :patient)
