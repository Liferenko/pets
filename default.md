# locate this file in  .gitlab/merge_request_templates/default.md

**Feature self-check list:**
- [ ] There is a moduledoc with a meaningful description of what module does unless the purpose of module is very obvious like in Resolvers/Schemas/Workflows/Finders/Filters. The moduledoc must explain the area of responsility of a module and reflect on how complex logic of the module works should it have one. When you modify existing module, you are required to add missing moduledoc or modify the existing description should you change it. 
- [ ] There are @specs for all affected public functions

**Notes to reviewers:**

**Patch notes:**

**How to test:**
