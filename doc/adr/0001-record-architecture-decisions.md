# 1. Record architecture decisions

Date: 2024-08-23

## Status

Accepted

## Context

I need to record the architectural decisions made on these projects. Seems like it is a good evolution over existing README updates

## Decision

I'll use Architecture Decision Records, as [described by Michael Nygard](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions).

## Consequences

See Michael Nygard's article, linked above. For a lightweight ADR toolset, see Nat Pryce's [adr-tools](https://github.com/npryce/adr-tools).
