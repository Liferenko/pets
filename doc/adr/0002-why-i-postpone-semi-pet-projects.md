# 2. Why I postpone semi-pet projects

Date: 2024-08-23

## Status

Suggested

## Context

The issue motivating this decision, and any context that influences or constrains the decision.
A lot of good ideas have been already added in pets/ but they are paused.

## Decision

The change that we're proposing or have agreed to implement.
For healthTech projects like Medicard I ended up with potential butt-hurt with papers all around EU or US. They've got something similar to GDPR but for patient's info.
For projects like BodyLogs I postponed it because of lack of technical clearness how to collect those data from human body.
Mobile app like Blackbox has to blocks to be paused. So I'm not sure why it paused 

## Consequences

What becomes easier or more difficult to do and any risks introduced by the change that will need to be mitigated.
Those ideas stay here as in a graveyard of idea :/ 
