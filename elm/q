if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

" ALE - in-process linter (for Elm and JS)
Plug 'w0rp/ale'

" Elm plugin
Plug 'elmcast/elm-vim'

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" Any valid git URL is allowed
Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" Multiple Plug commands can be written in a single line using | separators
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" Using a non-master branch
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }

" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
Plug 'fatih/vim-go', { 'tag': '*' }

" Plugin options
Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }

" Plugin outside ~/.vim/plugged with post-update hook
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" Unmanaged plugin (manually installed and updated)
Plug '~/my-prototype-plugin'

" Initialize plugin system
call plug#end()

set fileencodings=utf-8,cp1251,koi8-r,cp866
syntax on
" Размер табулации по умолчанию
" " colorscheme default

set shiftwidth=4
set softtabstop=4
set tabstop=8
set bs=2 " без этого бекспейс будет жутко криво работать
set nonu nu
"set foldcolumn=2 " Левая колонка фолдинга
"set foldenable
set incsearch " Поиск по набору текста

set expandtab " Преобразование Таба в пробелы
set autoindent " Включить автоотступы
set showmode " показывает в каком режиме работаешь
set ignorecase "игнорировать прописные/строчные при поиске
set hlsearch "при поиске помечать все найденные строки

" Выключаем надоедливый "звонок"
set novisualbell
set dir=~/.vim " Все swap файлы будут помещаться в эту папку; ну их нафик
set visualbell " Включает виртуальный звонок (моргает, а не бибикает при
" ошибках)

" " Fix arrow keys that display A B C D on remote shell:
" " Use Vim settings, rather then Vi settings (much better!).
" " This must be first, because it changes other options as a side effect.
set nocompatible
