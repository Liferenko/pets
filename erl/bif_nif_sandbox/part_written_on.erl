-module(complex6).
-export([function_foo/1, function_bar/1]).
-on_load(init/0).

init() ->
    ok = erlang:load_nif(".complex6_nif", 0).

function_foo(_X) ->
    exit(nif_library_not_loaded).

function_bar(_Y) ->
    exit(nif_library_not_loaded).
