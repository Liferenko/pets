%%%-------------------------------------------------------------------
%% @doc fantasyteam public API
%% @end
%%%-------------------------------------------------------------------

-module(fantasyteam_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    fantasyteam_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
