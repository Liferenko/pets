%%%-------------------------------------------------------------------
%% @doc genchat public API
%% @end
%%%-------------------------------------------------------------------

-module(genchat_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    genchat_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
