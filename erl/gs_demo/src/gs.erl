-module(gs).

-behaviour(gen_server).

-export([start_link/0]).
-export([init/1, handle_continue/2]).


start_link() ->
    Body = "Hey, teacher",
    gen_server:start_link(gs, Body, []).

init(Args) ->
    io:format("Initializing...\n", []),
    {ok, Args, {continue, fun() -> doLongTask() end}}.
 
handle_continue(Func, State) ->
    io:format("Continuing... State is: ~p\n", [State]),
    Func(),
    {noreply, State}.

doLongTask() ->
     timer:sleep(3000),
     io:format("Finished long task.\n").
