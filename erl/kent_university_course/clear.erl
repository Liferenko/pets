-module(clear).
-export([me/0]).

% Clean the shell
me() ->
    io:format("~s", [os:cmd("clear")]).

