-module(file_indexing).
-export([get_file_content/2, path/2]).

get_file_content(Filename, Modes) ->
    {ok, File} = file:open(Filename, Modes),
    {ok, File}.

path(Path, Filename) ->
    file:path_consult(Path, Filename).
    
