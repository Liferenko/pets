-module(geometry).
-export([perimeter/1]).


perimeter(0) ->
    {0, ok};
perimeter({circle, _Point, Radius}) ->
    (2 * math:pi()) * Radius;
perimeter({square, _Point, Side}) ->
    4 * Side.
