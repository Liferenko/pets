-module(how_many_equal_SUITE).
-export([all/0]).
-export([how_many_equals/1]).

all() -> [how_many_equals].

how_many_equals(_) ->
    3 = simple_math:how_many_equals(34,34,34),
    2 = simple_math:how_many_equals(34,25,34),
    0 = simple_math:how_many_equals(34,25,36),
    {comment, "Es is normal!"}.
