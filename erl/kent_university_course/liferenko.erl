% There is a couple of modules I about to use constantly
-module(liferenko).
-export([area/2, hypotenuse/2, perimeter/2]).
-export([pretty_answer/1, take/2]).

% TODO:
%   - to add measurement utits to each func
%   - to add simple tests for that module (refresh in my mind a book PropEr)

-spec take(integer(), [T]) -> [T].

hypotenuse(Xside, Yside) ->
    math:sqrt(simple_math:quadratic(Xside) + simple_math:quadratic(Yside)).

perimeter( Xside, Yside ) ->
    hypotenuse( Xside, Yside ) + Xside + Yside.

area( Xside, Yside ) ->
    H = hypotenuse(Xside, Yside),
    simple_math:area(Xside, Yside, H).


%%%%%%%%%%% Handlers

pretty_answer( Function ) ->
    clear:me(),
    timer:sleep(500),
    % TODO: to check is it possible to avoid first part in io:format down there
    io:format("~ts ~p.~n", [<<"Твой ответ - "/utf8>>, Function]).




% TODO: solve when Number is greater than 0 in first clause
take(0, _Word) ->
    [];
take(_Number, []) ->
    [];
take(Number, [H|T]) ->
    [H|take(Number - 1, T)].
