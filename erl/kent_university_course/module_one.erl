-module(module_one).
-export([maxThree/3]).

maxThree(First, Second, Third) ->
    % compare? recursion?
    Left = max(First, Second),
    Right = max(Second, Third),
    max(Left, Right).
