-module(paper_folding).
-export([fold_paper/1]).

-spec fold_paper(Number :: non_neg_integer()) -> pos_integer().

fold_paper(0) ->
    1;
fold_paper(1) ->
    2;
fold_paper(Number) ->
    Number + fold_paper(Number - 1).
