-module(simple_math).
-export([area/3, cubic/1, double/1, quadratic/1, multiply/2]).
-export([max_three/3, how_many_equals/3]).
-export([factorial/1, fibonacci/1]).

-spec factorial(Number :: non_neg_integer()) -> pos_integer().

area(A, B, C) ->
    % var C can't be greater than double of A or B
    S = (A+B+C)/2,
    math:sqrt(S*(S-A)*(S-B)*(S-C)).

cubic( Number ) -> 
    quadratic(Number) * Number.

double( X ) ->
    multiply(2, X).

quadratic( Number ) ->
    Number * Number.

multiply( X, Y ) ->
    X * Y.

%%%%% Comparesing
max_three(First, Second, Third) ->
    {Left, Right} = {max(First, Second), max(Second, Third)},
    max(Left, Right).

how_many_equals(Value, Value, Value) -> 3;
how_many_equals(Value, Value, _) -> 2;
how_many_equals(_, Value, Value) -> 2;
how_many_equals(Value, _,  Value) -> 2;
how_many_equals(_, _, _) -> 0.


%%%%% Recursion
factorial(0) -> 
    1;
factorial(Number) ->
    factorial(Number - 1) * Number.

fibonacci(0, Y, _) -> Y;
fibonacci(X, Y, Z) ->
    fibonacci(X - 1, Z, (Z + Y)).

fibonacci(Number) ->
    fibonacci(Number, 0, 1).

