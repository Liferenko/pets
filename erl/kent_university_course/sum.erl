-module(sum).
-export([sum/1]).

% direct recursion
%sum([])     -> 0;
%sum([H|T])  -> H + sum(T).


% tail rec
sum(T) -> sum(T, 0).

sum([], Sum)  -> Sum;
sum([H|T], S) -> sum(T, S+H).
