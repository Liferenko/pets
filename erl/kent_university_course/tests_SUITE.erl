-module(tests_SUITE).
-export([all/0]).
-export([cubic/1, 
         how_many_equals/1, 
         factorial/1, 
         fibonacci/1, 
         foldPaper/1, 
         perimeter/1,
         sum/1
        ]).

all() -> [cubic, how_many_equals, factorial, fibonacci, fold_paper, perimeter, sum].

cubic(_) ->
    8 = simple_math:cubic(2),
    125 = simple_math:cubic(5),
    1331 = simple_math:cubic(11),
    87528.38399999999 = simple_math:cubic(44.4),
    {comment, "Cubic is okay"}.

how_many_equals(_) ->
    3 = simple_math:how_many_equals(34, 34, 34),
    2 = simple_math:how_many_equals(34, 25, 34),
    0 = simple_math:how_many_equals(34, 25, 36),
    {comment, "Es is normal!"}.

factorial(_) ->
    1 = simple_math:factorial(0),
    24 = simple_math:factorial(4),
    % TODO: neg_integers 1 = simple_math:factorial(-88),
    {comment, "Recursion is ok"}.

fibonacci(_) ->
    0 = simple_math:fibonacci(0),
    1 = simple_math:fibonacci(1),
    21 = simple_math:fibonacci(8),
    14930352 = simple_math:fibonacci(36),
    {comment, "Fib with 3 args and tail recursion"}.



%%%% Paper folding
foldPaper(_) ->
    1 = paper_folding:foldPaper(0),
    2 = paper_folding:foldPaper(1),
    4 = paper_folding:foldPaper(2),
    7 = paper_folding:foldPaper(3),
    11 = paper_folding:foldPaper(4),
    172 = paper_folding:foldPaper(18),
    
    %TODO: floats and negative_ints
    %172 = paper_folding:foldPaper(18.2),
    %172 = paper_folding:foldPaper(-1),
    {comment, "Folding works green"}.


%%%% Shapes
perimeter(_) ->
    {0, ok} = geometry:perimeter(0),
    6.283185307179586 = geometry:perimeter({circle, {1,1}, 1}),
    16 = geometry:perimeter({square, {1,1}, 4}),
    {comment, "Perimeter is green"}.

%%%% Sum
sum(_) ->
    12 = sum:sum([3,4,5]),
    14 = sum:sum([2,3,4,5]),
    {comment, "Sum works green"}.



%%%% Indexing words in file
%get_file_content(_) ->
%    {ok, _} = file_indexing:get_file_content("address.txt", [read]),
%    {comment, "A content received. Green"}.
