-module(tests_take_SUITE).
-export([all/0]).
-export([take/1]).

all() -> [take].

take(_) ->
    [] = liferenko:take(0, "hello"),
    "hell" = liferenko:take(4, "hello"),
    "hello" = liferenko:take(5, "hello"),
    "hello" = liferenko:take(9, "hello"),
    {comment, "Take goes Green"}.
