-module(index).
-export([get_file_contents/1]).

%%% The aim of this exercise is to index a text file, by line number. 
%%% source - https://ugc.futurelearn.com/uploads/files/0b/56/0b566824-e773-49ce-b97d-356473a3e4b4/Functional_Erlang_Wk3_Assignment.pdf

get_file_contents(Filename) ->
    {ok, File} = file:open(Filename, [read]),
    Rev = get_all_lines(File, []).

get_all_lines(File, Partial) ->
    case io:get_line(File, "") of
        eof -> file:close(File),
               Partial;
        Line -> {Strip, _} = lists:split(length(Line)-1, Line),
                get_all_lines(File, [Strip|Partial])
    end.
