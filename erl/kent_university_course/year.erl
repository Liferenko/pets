-module year.
-export([is_leap/1]).

is_leap(Year) ->
    is_leap(Year rem 4, Year rem 100, Year rem 400).

is_leap(_, _, 0) -> true;
is_leap(_, 0, _) -> false;
is_leap(0, _, _) -> true;
is_leap(_, _, _) -> false.
