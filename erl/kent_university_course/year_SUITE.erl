-module year_SUITE.
-export([all/0]).
-export([is_leap/1]).

all() -> [is_leap].

is_leap(_) ->
   false = year:is_leap(1),
   false = year:is_leap(1995),
   false = year:is_leap(1997),
   % multiplies of 4 are leap...
   true = year:is_leap(1996),
   true = year:is_leap(2004),
   true = year:is_leap(2020),
   % ...except mult of 100...
   false = year:is_leap(1900),
   false = year:is_leap(1800),
   false = year:is_leap(2100),
   % ...except mult of 400
   true = year:is_leap(1600),
   true = year:is_leap(2000),
   true = year:is_leap(2400),
   {comment, ""}.
