# 6 weeks of Erlang - Log



### Day 31: June 3, 2020
##### (projects: Erlang course)


**Today's Progress**:  

**Thoughts:** :  
        1) We can use IO.inspect as a debugger thing (aka print() in Py or change.log() in fucking JS)
        
**TODO** :
        [ ] To finish a last third of Erlang Guidelines [bookmark](https://github.com/inaka/erlang_guidelines#write-function-specs)


**Stack:** : 
        2) Ecto
        3) Elixir

**Link to work:**
        1) [Ecto sandbox](https://gitlab.com/Liferenko/ecto-sandbox)









### Day 30: June 2, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Learing stopped (want to dive in Elixir)

**Thoughts:** :  
        1) I prefer to dive in Ecto+Elixir with real-world service. It means I have no chance to continue that Erlang class
        
**TODO** :
        [ ] To finish a last third of Erlang Guidelines [bookmark](https://github.com/inaka/erlang_guidelines#write-function-specs)


**Stack:** : 
        2) Ecto
        3) Elixir

**Link to work:**
        1) [Ecto sandbox](https://gitlab.com/Liferenko/ecto-sandbox)









### Day 29: June 1, 2020
##### (projects: Erlang course)


**Today's Progress**:  

**Thoughts:** :  
        1) I was dancing with Elixir+PostgreSQL environment
        
**TODO** :
        [ ] To finish a last third of Erlang Guidelines [bookmark](https://github.com/inaka/erlang_guidelines#write-function-specs)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 28: May 31, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) File indexing. Tests. Still shit, but whatever...

**Thoughts:** :  
        
**TODO** :
        [ ] To finish a last third of Erlang Guidelines [bookmark](https://github.com/inaka/erlang_guidelines#write-function-specs)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 27: May 30, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Refactoring a code according to Erlang Guidelines

**Thoughts:** :  
        1) If I want to use _Variable - why don't use just _ instead, hah?:)
        
**TODO** :
        [ ] To finish a last third of Erlang Guidelines [bookmark](https://github.com/inaka/erlang_guidelines#write-function-specs)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 26: May 29, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Elixir + Phoenix + new schema in PostgreSQL. Unstable yet, but... But something :)

**Thoughts:** :  
        1) I am still so surprised about commands like "mix phx.gen.html" and so on :) Looks like uncontrollable magic and it scares a bit :)
        
**TODO** :
        [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 25: May 28, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Dances with Phoenix app

**Thoughts:** :  
        1) I definitely have no energy for Erlang course. :/ Shit :/
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang
        2) Elixir + Phoenix

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 24: May 27, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Sorry, Erlang. But there is NASA launch :)

**Thoughts:** :  
        1) Sorry, Erlang. But there is NASA launch :)
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 23: May 26, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Refactoring take/2

**Thoughts:** :  
        2) Instead of using a variable Result I may use an idea like that one - [H|take(Number - 1, T))]
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 22: May 25, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) "take" function
        2) Debugging

**Thoughts:** :  
        1) I see I started to use Zeal more often (and that fact is really nice new habit)
        2) It turns out Erlang debugger might be finally my buddy :D thanks that dude (https://vimeo.com/32724400). I saw a blind side of my working function. And it would has been imposible to recognise without debugger 
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 21: May 24, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Rest day
        2) Worked with dirs and paths in Erlang. Unfinished

**Thoughts:** :  
        1) Need to exhale a bit
        2) Looks like ct_run has different (an unobvious) paths to the files
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 20: May 23, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Tests for direct/tail recursions
        2) Simple playin-around with week2 recursion

**Thoughts:** :  
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 19: May 22, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Init a property-based pet project
        2) replace tests from CommonTest to PropEr

**Thoughts:** :  
        1) I refreshed the old knowledges about Property-based testing and am trying to use it with that course
        2) Rebar3. Again troubles with exporting him in $PATH. I hope now it works as expected (will see after reboot)
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 18: May 21, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Refactored fib/3 function. There is no need to test 3-argumented func, because it is already used in single-argumented func.
        2) Perimeter for circle and square

**Thoughts:** :  
            1) About exporting one-argumented function when there is another one with 3 arguments inside: we can export func/1 if we use func/3 only inside and as a support func for func/1. Sounds simple and logic. Now I know.
            2) Im not sure if Im doing okay with pattern matching and recursion. I feel like Im just doing cases instead of recursion.
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 17: May 20, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) green fib/3 tests 
        2) Refactoring the 2 fibonacci functions into one
        3) TODO: head mismatch. To solve 

**Thoughts:** :  
        1) Im not sure I got an idea of fibonacci3/3 (especially the part with third argument).
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)
    [x] Type check and declaring


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 16: May 19, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Tail recursion (Week2)
        2) Type check (with spec and with "when N>0")

**Thoughts:** :  
        1) Looks like I can use exception as an type checker. It is more like "if statement", but at least it works :)
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)
    [x] Type check and declaring


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 15: May 18, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Week3. Init tests and stupid logic
        2) Week2. Fibonacci + tests
        3) Paper folding calculation

**Thoughts:** :  
        1) I'm about to start losing a motivation
        2) I feel unfamiliar with type declaring in Erlang. My tests are still failing when there are floats and negative integers.
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)
    [ ] Type check and declaring


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 14: May 17, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) updated factorial method with TDD (week 2)

**Thoughts:** :  
        1) I need to solve a type spec issue. There is still an open wound with negative integers in factorial.
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang
        2) Common Test
        3) Zeal

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 13: May 16, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) TDD for recursion block (week 2)

**Thoughts:** :  
        1) I forgot TDD workflow. Again. I wrote a method and only after that I realized I forgot tests))
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 12: May 15, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) TDD in Erlang - counting a BMI. Green tests
        2) TDD in Elixir - counting a BMI. Green tests

**Thoughts:** :  
        1) It turns out var names are folowwing not same logic: Ex - all vars should start with lower char; Erl - with capital char
        2) One more baby step in TDD and FP
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell
        2) Common Test Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 11: May 14, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Really baby step in TDD with Common Tests

**Thoughts:** :  
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell
        2) Common Test Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 10: May 13, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) finished week 1 and quize
        2) did simple unit test without framework
        3) refactired unit tests with Common Test 

**Thoughts:** :  
        1) TDD in Erlang. Finally! Yet another baby step to TDD maniac year goal :) [Sauce](http://erlang.org/doc/apps/common_test/run_test_chapter.html) 
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 9: May 12, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) practicing in pattern-matching. Function maxThree
        2) maxThree refactoring
        3) simplo-stupid unit testing :)

**Thoughts:** :  
        1) "Equal sign is like pattern matching sign."(c)
        2) Need to find how in tests start 3+ lines of assertion (what sign should I use: comma and semicolon)
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 8: May 11, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) some warmups with `erlang:binary_to_.../2`;
        2) was read much about lists:___().

**Thoughts:** :  
        1) NB! "...atoms are not garbage collected. So, from a security reason, we need to be careful not to convert user input into atoms. This is a typical concern in web programming where the user input can never be trusted"(c) Antonio Cangiano, from Erlang course  
        2) It was something not so obvious for me: I thought when you compiled parent module and his children are uncompiled yet - the children will be compiled as well. But no.
        You need to compile them before. That is logical and now I know for sure :)
        3) Atoms are cool as "tags" or "flags". "... no matter what algorithm is used for pattern-matching, matching strings/binaries can not be faster than matching atoms"
        4) About atoms again: A common Erlang idiom: use the 1st field to indicate what sort of data in the tuple
        5) Tuples == fixed size; Lists == variable size
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell
        2) Zeal doc app

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 7: May 10, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Read an Erlang guideline (as an alternative to Python's PEP8)
        2) 

**Thoughts:** :  
        1) I like the way Erlang is working with case syntax. It looks like breaking DRY rule, but after a second look you're starting to see why that way is quite good. [There is an example](https://github.com/inaka/erlang_guidelines/blob/master/src/smaller_functions.erl). I hope I will not forgot that :)
        2) Whoa, I didn't know I already have soooo many bad cases regarding to ErlGuite :)
        3) 
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 6: May 9, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) refactoring of liferenko.erl (rewrite a logic of a printing usage)
        2) unsuccessful tries of pattern matching using. I tried to separate caltucalte's "ok"-status from answer.

        3) init a client's app Elixir\Phoenix inside docker container
        4) config DB (unfinished. I stopped on 10:00:15.954 [error] GenServer #PID<0.238.0> terminating)


**Thoughts:** :  
        1) Good Saturday. Needless to say, it was perfect learning day :)
        2) I think I finally found why and where to use a book "Property Driven Development in Erlang/Elixir". It's time to restart reading :)


**Stack:** : 
        1) Erlang/OTP 22 shell
        2) Docker
        3) PostgreSQL

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)








### Day 5: May 8, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - add hypotenuse calcilation with clean+show

**Thoughts:** :  
        1) Looks like I've got wrong answer. Need to double check :))


**Stack:** : 
        - Erlang/OTP 22 shell

**Link to work:**









### Day 4: May 7, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - io:format()
        - utf/8 and Cyrillic letters
        - printing and debug tracing 

**Thoughts:** :  
        1) I did refresh in my mind an old and archived knowledges about printing in Erlang
        2) Finally moved forward some debugging skill in FP. I remember 2 years ago I unfortunately skipped that topic. My bad. 
        3) I did one more baby step in Zeal DocApp. One more step shows how powerful that thing is. 
        4) TODO: Paul, remember about your TDD hopes and fears :)


**Stack:** : 
        - Erlang/OTP 22 shell
        - Zeal application

**Link to work:**









### Day 3: May 6, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - wrote some simple erlang modules (multiply, double)
        - wrote a clear command
        - read a block about memory size of different data types (founded in Zeal->Erlang->Guides->Advanced->10.1 Memory)

**Thoughts:** :  
        - finally found out how to clear an Erlang shell. It was nasty method. Guess what? I did it more nasty :)
        Finally there is some code written by myself :D
        - in short future I need to slowread a part "Application" from Zeal->Erlang->Guides->Application
        - One erlang process's size is 338 words. It's a lot of memory. I think I will need it in real highload project.
        - it would be nice to read and to replicate code form a guide "Concurrent Programming" in Zeal->Erlang->Concurrent Programming. Why? Because there is ready-to-test code

**Stack:** : 
        - Erlang/OTP 22 shell

**Link to work:**









### Day 2: May 5, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - two videos done
        - some plays with toys inside erl console. Just warming up :)
        - I asked a question I didn't answered 2 years ago. The question is about clear the erl console. I mean it's not a big deal to get an answer. I did it more for deep connection with that course.

**Thoughts:** :  
        - I was excited about super-simple thing: help(). I knew about inline help in evety lib and langs, but in Erlang console I feel it helpful as hell :) This simple little command e(N) made me feel like a child who got something new in his mind)
        - In console 'erl' we may use Ctrl+R as we're using in shell

**Stack:** : 

**Link to work:**









### Day 1: May 4, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - Three videos from Week 1 are done.  
        - I did some configurations for multi-monitors in i3wm. I hope I'm back to i3 and will use it during that course.

**Thoughts:** :  
1) Hello there! I'm Paul. I started to date with BEAM in Jan 2018. It was something special for my OOP mindset. It was like they gave you steering wheel from spaceship while you had an experience to drive BWM all the time :) As you can see I can't enjoy driving BMW anymore))
2) It turns out there is a good and meaty conversations in comments block. I guess it would be useful to read it each time

**Stack:** : 
        - Vim 8 

**Link to work:**









### Preparation Day -1: May 3, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - one day before course's starting. I guess I'm ready. Teammates are ready as well

**Thoughts:** :  Nice! Erlang/OTP 22 is installed, Alles gut.

**Stack:** : 
        - Vim 8 

**Link to work:**









### Preparation Day -8: April 26, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - there is a team from r/erlang, which ready to do steps in that course together

**Thoughts:** :  there are 5 tips and tools for social learning. I think I did it before I found that official tips)) Nice! 

**Stack:** : 
        - Vim 8 

**Link to work:**
        - [5 tips and tools for social learning](https://www.futurelearn.com/info/blog/5-tips-tools-social-learning)


===================================================================

