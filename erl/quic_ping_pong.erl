application:esure_all_started(quicer),
Port = 4567
LOptions = [
           {cert, "cert.pem"},
           {key, "key.pem"},
           {alpn, ["sample"]}
          ],
{ok, L} = quicer:listen(Port, LOptions),
{ok, Conn} = quicer:accept(L, [], 5000),
{ok, Conn} = quicer:handshake(Conn),
{ok, Stm} = quicer:accept_stream(Conn, []),
receive {quic, <<"ping">>, Stm, _, _, _} -> ok end,
{ok, 4} = quicer:send(Stm, <<"pOng">>),
quicer:close_listener(L).
