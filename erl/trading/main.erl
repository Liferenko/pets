%%% File: main.erl
%%% Description: simple trading API for proposing a buy deals and sell deals
%%% Author: Paul Liferenko <http://t.me/Liferenko>

%%% A buy deal placing
%%            Type         URL                Args    
%% Input      POST        /deals/buy          price (int)
%% Output     ---         ---                 status, delta

%%% A sell deal placing
%%            Type         URL                Args    
%% Input      POST        /deals/sell         price (int)
%% Output     ---         ---                 status, delta

%% get_all_deals
%%            Type         URL                Args    
%% Input      GET          /deals             price (int)
%% Output     ---         ---                 id, type, timestamp, price


-module(main).
-export([buy/1, sell/1, get_all_deals/0]).
    

buy(Price) ->
    %% Сделка закрывается, если имеется противоположная по типу сделка и абсолютная разница в цене между сделками
    %% не превышает 3.
    
    %% ВАжно! клиент ожидает ответ, пока не будет найдена подходящая сделка.
    if
        Price >= 3 ->
                io:format("Done! You've set a deal for buing this item.~n ur price: ~n deal delta");
    true ->
        io:format("Your bid has been counted. Wait until the end of the auction.").



sell(Price) ->
    %% Сделка закрывается, если имеется противоположная по типу сделка и абсолютная разница в цене между сделками
    %% не превышает 3.
    if
        Price >= 3 ->
        %% ВАжно! клиент ожидает ответ, пока не будет найдена подходящая сделка.
        io:format("Done! You've a deal for selling this item.~n ur price: ~n deal delta");
    true ->
        io:format("Done.").
get_all_deals() ->
    io:format("Here is %d deals for this time. Details ~n deal delta").
