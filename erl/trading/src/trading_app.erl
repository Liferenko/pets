-module(trading_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

start(_Type, _Args) ->

        %% So I need to read more about routing - https://ninenines.eu/docs/en/cowboy/2.6/guide/routing/
	Dispatch = cowboy_router:compile([
            {'_', [{"/", hello_handler, []}]}
        ]),
        {ok, _} = cowboy:start_clear(my_http_listener, 
                [{port, 8080}], #{env => #{dispatch => Dispatch}}
        ),
        trading_sup:start_link().

stop(_State) ->
	ok.
