defmodule AA do
  # Test task during an interview. It was fun to resolve it
  """
  --- Day 5: Alchemical Reduction ---
  You've managed to sneak in to the prototype suit manufacturing lab. The Elves are making decent progress, but are still struggling with the suit's size reduction capabilities.

  While the very latest in 1518 alchemical technology might have solved their problem eventually, you can do better. You scan the chemical composition of the suit's material and discover that it is formed by extremely long polymers (one of which is available as your puzzle input).

  The polymer is formed by smaller units which, when triggered, react with each other such that two adjacent units of the same type and opposite polarity are destroyed.

  Units' types are represented by letters; units' polarity is represented by capitalization.
  For instance, r and R are units with the same type but opposite polarity, whereas r and s are entirely different types and do not react.

  For example:

  In aA, a and A react, leaving nothing behind.
  In abBA, bB destroys itself, leaving aA. As above, this then destroys itself, leaving nothing.
  In abAB, no two adjacent units are of the same type, and so nothing happens.
  In aabAAB, even though aa and AA are of the same type, their polarities match, and so nothing happens.
  Now, consider a larger example, dabAcCaCBAcCcaDA:
  dabCBAcaDA:

  dabAcCaCBAcCcaDA  The first 'cC' is removed.
  dabAaCBAcCcaDA    This creates 'Aa', which is removed.
  dabCBAcCcaDA      Either 'cC' or 'Cc' are removed (the result is the same).
  dabCBAcaDA        No further actions can be taken.
  dabAaCBAcaDA
  After all possible reactions, the resulting polymer contains 10 units.

  How many units remain after fully reacting the polymer you scanned?
  (Note: in this puzzle and others, the input is large;
  if you copy/paste your input, make sure you get the whole thing.)

  """

  def long_formula_list do
    "111dabAcCaCBAcCcaDA"
    |> String.to_charlist()
  end

  @spec can_it_react?(String.t) :: true | false
  def can_it_react?(unit_pair) do
    [h, t] = String.to_charlist(unit_pair)

    can_it_react?(h, t)
  end

  def can_it_react?(h, t) do
    cond do
      h - t == 32 ->
        true
      h - t == -32 ->
        true
      true -> 
        false
    end
  end


  def check_formula(_list, acc \\ [])

  # def check_formula([], acc) do
  #   IO.inspect(acc, "final formula")
  # end
  #
  def check_formula([head | []], acc) do
    IO.inspect([acc, head]|> List.to_string(), label: "final formula")
    # reco

  end

  # [a,b | tail]
  def check_formula([head | [head2 | t2] = tail], acc) do
    case can_it_react?(head, head2) do
      true -> 
        check_formula(t2, acc)

      false -> 
        acc = List.insert_at(acc, -1, head)
        check_formula(tail, acc)
    end
  end

end
