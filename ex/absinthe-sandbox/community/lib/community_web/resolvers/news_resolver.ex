defmodule CommunityWeb.NewsResolver do
  alias Community.News

  @doc """
  iex> CommunityWeb.NewsResolver.all_links("23", "asdf", "4")
  :ok
  """
  def all_links(_root, _args, _info) do
    {:ok, News.list_links()} # https://www.howtographql.com/graphql-elixir/2-queries/
  end
end
