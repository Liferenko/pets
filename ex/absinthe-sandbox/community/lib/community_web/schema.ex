defmodule CommunityWeb.Schema do
  use Absinthe.Schema

  alias CommunityWeb.NewsResolver

  object :link do
    field :id, not_null(:id)
    field :url, not_null(:string)
    field :description, not_null(:string)
  end

  query do
    @desc "Get all links with no details"
    field :all_links, not_null(list_of(not_null(:link))) do
      resolve(&NewsResolver.all_links/3) #??? dont get it yet
    end
  end
end
