defmodule Community.Repo.Migrations.UpdateTypoInDesc do
  use Ecto.Migration

  def change do
    alter table("links") do
      remove :desctiption
      add :description, :string
    end

  end
end
