defmodule ExampleTest do
  use ExUnit.Case
  doctest Prefix

  test "always pass" do
    assert true
  end

  test "add mrs to prefix" do
    assert mrs.("Smith") == "Mrs Smith"
  end
  
  """
  test "add second word in anon func" do
    assert Prefix.prefix.("Elixir").("Rocks") == "Elixir Rocks"
  end
  """
end
