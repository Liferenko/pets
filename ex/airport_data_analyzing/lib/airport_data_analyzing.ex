defmodule AirportDataAnalyzing do
  @moduledoc """
  Documentation for `AirportDataAnalyzing`.
  """
  alias NimbleCSV.RFC4180, as: CSV

  @doc """

  ## Examples

      iex> AirportDataAnalyzing.airports_csv()
      "/home/eleven/pets/ex/airport_data_analyzing/_build/test/lib/airport_data_analyzing/priv/airports.csv"

  """
  def airports_csv() do
    Application.app_dir(:airport_data_analyzing, "/priv/airports.csv")
  end

  @doc """

  ## Examples

  iex> {time, whatever} = :timer.tc(&AirportDataAnalyzing.open_airports/0)
  iex> whatever
  []

  """
  def open_airports() do
    airports_csv()
    |> File.stream!()
    |> Flow.from_enumerable()
    |> Flow.map(
      fn row ->
        [row] = CSV.parse_string(row, skip_headers: false) 
        %{
          id:       Enum.at(row, 0),
          type:     Enum.at(row, 2),
          name:     Enum.at(row, 3),
          country:  Enum.at(row, 8),
        }
      end)
    |> Flow.reject(&(&1.type == "closed"))
    |> Flow.partition(key: {:key, :country})
    |> Flow.group_by(& &1.country)
    |> Flow.map(fn {country, data} -> {country, Enum.count(data)} end)
    |> Flow.take_sort(10, fn {_, a}, {_, b} -> a > b end)
    |> Enum.to_list()
    |> List.flatten()
  end
end
