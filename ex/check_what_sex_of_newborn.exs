defmodule Doctor do
    def for( gender, doc_answer ) do
        fn
            (^gender) -> "#{doc_answer} #{gender}"
            (_)       -> "Hm, we need doublecheck"
        end
    end
end

dr_phil = Doctor.for( "girl", "Congrats, you have a " )
dr_susan = Doctor.for( "boy", "Big day! You have a " )

IO.puts dr_phil.("girl")
IO.puts dr_susan.("boy")
IO.puts dr_phil.("xe")
