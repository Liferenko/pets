defmodule CLIchat do
  @moduledoc """
  Documentation for `CLIchat`.
  """

  @spec start() :: GenServer.on_start()
  def start(), do: GenServer.start(CLIchat.Application, [self(), name: :chat])
  
  def start_link(), do: GenServer.start_link(CLIchat.Application, [self(), name: :chat])

  def peers(), do: GenServer.call(:chat, :list_peers)
  def msg(text), do: GenServer.call(:chat, {:broadcast, text})
  def dm(to, text), do: GenServer.call(:chat, {:send_dm, to, text})
end
