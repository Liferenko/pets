defmodule Sender do
  @moduledoc """
  Documentation for `Sender`.
  """

  #task = Task.async(fn -> Sender.send_email("hello@k.com") end)
  
  @spec notify_all([String.t]) :: any
  def notify_all(emails) do
    emails
    |> Task.async_stream(
      &send_email/1,
      ordered: false
    )
    |> Enum.to_list()
  end

  def send_email("he3@hllo.com" = email) do
    raise "Oops, couldn't send email to #{email}"
  end

  def send_email(email) do
    Process.sleep(3_000)
    IO.puts("Email to #{email} sent")
    {:ok, "email_sent"}
  end
end
