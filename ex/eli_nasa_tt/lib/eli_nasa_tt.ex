defmodule EliNasaTt do
  @moduledoc """
  Documentation for EliNasaTt.
  """

  @doc """
  Hello world.

  ## Examples

      iex> EliNasaTt.hello()
      :world

  """
  def hello do
    :world
  end
end
