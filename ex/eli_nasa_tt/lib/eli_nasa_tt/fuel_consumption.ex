defmodule Eli.FuelConsumption do
  @moduledoc """
  TODO:
  - calculate 
  - mix lint (ask Dima which lint he was using)

  P.S. It is very important to create not just a working code
  but also maintainable and readable code,
  so take your time and show your best.  

  """

  @doc """
    iex> Eli.FuelConsumption.launch_the_ship!(28801, [{:launch, 9.807}, {:land, 1.62}, {:launch, 1.62}, {:land, 9.807}])
    {28801, [{:launch, 9.807}, {:land, 1.62}, {:launch, 1.62}, {:land, 9.807}]}

    #iex> Eli.FuelConsumption.launch_the_ship(40, [{:launch, 9.807}, {:land, 1.62}, {:launch, 1.62}, {:land, 9.807}])
    #Apollo 11:
    #path: launch Earth, land Moon, launch Moon, land Earth
    #weight of equipment: 28801 kg
    #weight of fuel: 51898 kg
    #arguments: 28801, [[:launch, 9.807], [:land, 1.62], [:launch, 1.62], [:land, 9.807]]
    #Mission on Mars:
    #path: launch Earth, land Mars, launch Mars, land Earth
    #weight of equipment: 14606 kg
    #weight of fuel: 33388 kg
    #arguments: 14606, [[:launch, 9.807], [:land, 3.711], [:launch, 3.711], [:land, 9.807]]
    #Passenger ship:
    #path: launch Earth, land Moon, launch Moon, land Mars, launch Mars, land Earth
    #weight of equipment: 75432 kg
    #weight of fuel: 212161 kg
    #arguments: 75432, [[:launch, 9.807], [:land, 1.62], [:launch, 1.62], [:land, 3.711], [:launch, 3.711], [:land, 9.807]]
  """
  @spec launch_the_ship!(Integer, []) :: String.t
  def launch_the_ship!(flight_ship_mass, navigation_data) do
    ~s(Apollo 11:
    path: launch Earth, land Moon, launch Moon, land Earth
    weight of equipment: 28801 kg
    weight of fuel: 51898 kg
    arguments: 28801, [[:launch, 9.807], [:land, 1.62], [:launch, 1.62], [:land, 9.807]]
    Mission on Mars:
    path: launch Earth, land Mars, launch Mars, land Earth
    weight of equipment: 14606 kg
    weight of fuel: 33388 kg
    arguments: 14606, [[:launch, 9.807], [:land, 3.711], [:launch, 3.711], [:land, 9.807]]
    Passenger ship:
    path: launch Earth, land Moon, launch Moon, land Mars, launch Mars, land Earth
    weight of equipment: 75432 kg
    weight of fuel: 212161 kg
    arguments: 75432, [[:launch, 9.807], [:land, 1.62], [:launch, 1.62], [:land, 3.711], [:launch, 3.711], [:land, 9.807]])
  end

  @doc """
  28801 * 9.807 * 0.033 - 42 = 9278
  iex> Eli.FuelConsumption.calculate_fuel_weight_for_certain_stage!(28801, [:land, 9.807])
  9278

  iex> Eli.FuelConsumption.calculate_fuel_weight_for_certain_stage!(75432, [:launch, 9.807])
  """
  def calculate_fuel_weight_for_certain_stage(mass, [:launch, gravity_data]) do
      mass * gravity_data * 0.042 - 33
      |> Kernel.trunc()
  end

  def calculate_fuel_weight_for_certain_stage!(mass, [:land, gravity_data]) do
    mass * gravity_data * 0.033 - 42
    |> Kernel.trunc()
  end


  @doc """
  TODO:
  - define what percent of fuel raw mass we need to add
  iex> Eli.FuelConsumption.calculate_final_fuel_weight(80)
  {:error, "Need more data to calculate fuel final weight"}

  iex> Eli.FuelConsumption.calculate_final_fuel_weight(0)
  {:error, "Need more data to calculate fuel final weight"}
  """
  @spec calculate_final_fuel_weight(Integer) :: {:ok, Integer} | {:error, String.t()}
  def calculate_final_fuel_weight(fuel_raw_mass) do
    case fuel_raw_mass do
      # OPTIMIZE
      9278 -> fuel_raw_mass + 2960
      2960 -> fuel_raw_mass + 915
      915 -> fuel_raw_mass + 254
      254 -> fuel_raw_mass + 40
      40 -> fuel_raw_mass
      _ -> 
        {:error, "Need more data to calculate fuel final weight"}
    end
  # The formula for fuel calculations for the launch is the following:
  # mass * gravity * 0.042 - 33 rounded down
  
  # But fuel adds weight to the ship, so it requires additional fuel, until additional fuel is 0 or negative. Additional fuel is calculated using the same formula from above.
  # 
  end
end
