defmodule SandboxProcess do
  # TODO 
  # - result datatype
  @spec awaiting_for_receive_msgs([]) :: any
  def awaiting_for_receive_msgs(messages_received \\ []) do
    IO.puts("\n == \nPID #{inspect(self())} waiting to process a message \n == \n")

    receive do
      {_, "Hoo" = msg } ->
        IO.puts("Hoo")
        [msg | messages_received]
        |> IO.inspect(label: "MSGS RECEIVED")
        |> awaiting_for_receive_msgs()

      {_, "Boo" = msg } ->
        IO.puts("Boo")
        [msg | messages_received]
        |> IO.inspect(label: "MSGS RECEIVED")
        |> awaiting_for_receive_msgs()

      {:greencard, msg} ->
        IO.puts("Greencard. Message: #{msg}")
        awaiting_for_receive_msgs()

      {:epolicy, msg} ->
        IO.puts("Epolicy. Message: #{msg}")
        awaiting_for_receive_msgs()

      {:sys_call, msg} ->
        IO.puts("SystemCall. Message: #{msg}")
        awaiting_for_receive_msgs()

      _ ->
        IO.puts("nothing to hoo and to boo")
        awaiting_for_receive_msgs()

    end
    IO.puts("PID #{inspect(self())} is terminating")
  end
end
