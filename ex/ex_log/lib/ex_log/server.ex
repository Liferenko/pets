defmodule ExLog.Server do
  @moduledoc """
  This is the log server.

  1. At first sight, what possible issues and/or improvements do you see
  2. Supposing the server will face high concurrency, what other improvements
     we can make to this logger implementation and why?
  """

  @behaviour GenServer

  ## API

  @doc false
  def start_link(filename) do
    GenServer.start_link(__MODULE__, filename, name: __MODULE__)
  end

  @doc false
  def child_spec(filename) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [filename]}
    }
  end

  @doc false
  def log(data) do
    GenServer.call(__MODULE__, {:log, data})
  end

  ## GenServer Callbacks

  @impl true
  def init(filename) do
    # remove prev one
    _ = File.rm(filename)
    # start new
    io_pid = File.open!(filename, [:append]) # ? give filename
    {:ok, {filename, io_pid}}

    # TODO where to close the file
  end

  @impl true
  def handle_call({:log, data}, _from, filename) do
    io_dev = File.open!(filename, [:append]) #! 
    IO.puts(io_dev, data)
    :ok = File.close(io_dev) # !

    {:reply, :ok, filename}
  end
end
