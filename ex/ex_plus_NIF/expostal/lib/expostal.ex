defmodule Expostal do
  @moduledoc """
  Documentation for Expostal.

  Tutorial source - https://cs.mcgill.ca/~mxia3/2017/06/18/tutorial-extending-elixir-with-c-using-NIF/
  """

  @doc """
  Hello world.

  ## Examples

      iex> Expostal.hello()
      :world

  """
  def hello do
    :world
  end
end

defmodule Expostal.Parser do
  @moduledoc """
  Address parsing module for Openvenue's Libpostal, which does parses addresses.
  """

  @on_load { :init, 0 }

  app = Mix.Project.config[:app]

  # loading the NIF
  def init do
    path = :filename.join(:code.priv_dir(unquote(app)), 'parser')
    :ok = :erlang.load_nif(path, 0)
  end

  @doc """
  Parse given address into a map of address components
  ## Examples
      iex> Expostal.Parser.parse_address("845 Sherbrooke St W, Montreal, QC H3A 0G4")
      %{city: "montreal", house_number: "845",
        road: "sherbrooke st w", state: "qc",
        postalcode: "h3a 0g4"}

      iex> Expostal.Parser.parse_address("80a Paliashvili St, Tbilisi, QC H3A 0G4")
      %{city: "tbilisi", house_number: "80a",
        road: "paliashvili st", state: "qc",
        postalcode: "0102"}
  """
  @spec parse_address(address :: String.t) :: String.t
  def parse_address(address)
  def parse_address(_) do
    # if the NIF can't be loaded, this function is called instead.
    exit(:nif_library_not_loaded)
  end

end

