defmodule Expostal.MixProject do
  use Mix.Project

  def project do
    [
      app: :expostal,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:phoenix, "~> 1.4.10"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:gettext, "~> 0.11"},
      {:stash, "~> 1.0.0"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end

defmodule Mix.Tasks.Compile.Libpostal do
  def run(_) do
    if match?({:win32, _}, :os.type) do
      IO.warn("Windows is not supported")
      exit(1)
    else
      File.mkdir_p("priv")
      {result, _error_code} = System.cmd("make", ["priv/parser.so"], stderr_to_stdout: true)
      IO.binwrite(result)

      {result, _error_code} = System.cmd("make", ["priv/expand.so"], stderr_to_stdout: true)
      IO.binwrite(result)
    end
    :ok
  end
end
