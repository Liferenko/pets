#check_if_it_fizz = fn (1st, 2nd, 3rd) -> 
#   do
#       { 0, 0, _ } -> IO.puts( "FizzBuzz" ),
#       { 0, _, _ } -> IO.puts( "Fizz" ),
#       { _, 0, _ } -> IO.puts( "#{3rd}" )
#   end

handle_open = fn
    { :ok, file } -> "Read data #{ IO.read(file, :line) }"
    { _,   error }-> "Error: #{ :file.format_error(error) }"
end

handle_open.( File.open("pasha.txt") )
