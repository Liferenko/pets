# FraudDetectElixirAxon

**TODO: Add description**

[Source post](https://dockyard.com/blog/2022/04/07/catching-fraud-with-elixir-and-axon)

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `fraud_detect_elixir_axon` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:fraud_detect_elixir_axon, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/fraud_detect_elixir_axon](https://hexdocs.pm/fraud_detect_elixir_axon).

