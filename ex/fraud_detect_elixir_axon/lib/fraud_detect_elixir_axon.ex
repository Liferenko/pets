defmodule FraudDetectElixirAxon do
  @moduledoc """
  Documentation for FraudDetectElixirAxon.
  """

  use Explorer

  @doc """
  Hello world.

  ## Examples

      iex> FraudDetectElixirAxon.hello()
      :world

  """
  def hello do
    :world
  end

  def get_data(filepath) do
    df = Explorer.DataFrame.read_csv!(filepath, dtypes: [{"Time", :float}])
  end
end
