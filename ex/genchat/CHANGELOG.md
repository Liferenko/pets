2022-10-28
- add :get_members with correct test asserting
- replace String.to_atom/1 with {:via, UsernameRegistry, ...}
- removed 51st message from chat history

2022-10-27
- replaced default state with %State{}. All tests are green
- add teests for :send_message + :get_history
- add :join_room and :room_members. TODO: solve @derive issue and PIDs into the member list

2022-10-26
- started new project from empty directory but with previous experience :)
- added doc tests
- added messages + rooms
- added test for API endpoints
- [stable] green tests + doctests
- API endpoints for send_message and get_chat_history

2022-10-25
- looks like every Member process is shutting down when :get_members method started. Donno why yet
- updated :get_members method with more precise response
- [unstable] added Registry to create chat room. Got (EXIT) no process: the process is not alive or there's no process ')
- [stable] added ProcessRegistry when application starts

- TOOD send message to users

2022-10-24
- green test for :join
- added :DOWN handler
- added own PID for different members
- blueprint for DynamicSupervisor of members (unstable, need to be reworked)

2022-10-23
- updated :leave endpoint

2022-10-21
- updated :history
- updated tests
- [unstable] :get_members has strange state "true" instead of %State{}. Can't find the solution and the source of this true
- updated :join method
- updated :join and :members tests

2022-10-20

2022-10-19
- updated message functions 
- added them to GenServer
- updated msg print 
- started the messages sending to rooms (almost finish the blueprint)
- add state to a chatroom (includes members list and messages_history)
- TODO store message in chat room

2022-10-18
- message blueprint without GenServer
- green tests for room members and join/leave

2022-10-17
- Added room members storing in a process state

2022-10-14
- greentests for room members

2022-10-13
- added ETS table for users
- added :send_message_to_user + uniq_msg_id generator
- msg blueprint
TODO:
- Doublecheck that each Chat Room has own PID (it means each chat room is different process)

2022-10-12
- update room endpoint
- update :ets table for creating and selecting in a room
- update tests. All 15 tests are green (its a cheating, but it is the goal for now)

2022-10-11
- added GenServer
- :ets table for Rooms 
_ added handle_call for room_members

2022-10-10
- Added enpointrs to api
- API test

2022-10-07
- Added 14 scenario tests 

2022-10-06
- Added DB schemas for messages, users, rooms, rooms_members
- Added migrations
- Updated README and TODOs
