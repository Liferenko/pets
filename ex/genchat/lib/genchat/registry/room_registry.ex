defmodule Genchat.RoomRegistry do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: :room_registry)
  end

  def whereis_name(room_name) do
    GenServer.call(:room_registry, {:whereis_name, room_name})
  end

  def register_name(room_name, pid) do
    GenServer.call(:room_registry, {:register_name, room_name, pid})
  end

  def unregister_name(room_name) do
    GenServer.cast(:room_registry, {:unregister_name, room_name})
  end

  def send(room_name, message) do
    case whereis_name(room_name) do
      :undefined ->
        {:badarg, {room_name, message}}

      pid ->
        Kernel.send(pid, message)
        pid
    end
  end

  ##
  @impl true
  def init(_) do
    {:ok, %{}}
  end

  @impl true
  def handle_call({:whereis_name, room_name}, _from, state) do
    {:reply, Map.get(state, room_name, :undefined), state}
  end

  @impl true
  def handle_call({:register_name, room_name, pid}, _from, state) do
    case Map.get(state, room_name) do
      nil ->
        monitor = Process.monitor(pid)
        {:reply, :yes, Map.put(state, room_name, pid)}

      _ ->
        {:reply, :no, state}
    end
  end

  @impl true
  def handle_cast({:unregister_name, room_name}, state) do
    {:noreply, Map.delete(state, room_name)}
  end

  @impl true
  def handle_info({:DOWN, ref, :process, pid, reason}, state) do
    {:noreply, remove_pid(state, pid)}
  end

  ## Helpers
  def remove_pid(state, goal_pid) do
    Enum.filter(state, fn {key, pid} -> pid != goal_pid end)
    |> Enum.into(%{})
  end
end

