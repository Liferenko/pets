defmodule Genchat.UsernameRegistry do
  @moduledoc """
  TODO
  - merge it with Registry upthere
  """
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: :username_registry)
  end

  def whereis_name(user_name) do
    GenServer.call(:username_registry, {:whereis_name, user_name})
  end

  def register_name(user_name, pid) do
    GenServer.call(:username_registry, {:register_name, user_name, pid})
  end

  def unregister_name(user_name) do
    GenServer.cast(:username_registry, {:unregister_name, user_name})
  end

  def send(user_name, message) do
    case whereis_name(user_name) do
      :undefined ->
        {:badarg, {user_name, message}}

      pid ->
        Kernel.send(pid, message)
        pid
    end
  end

  ##
  @impl true
  def init(_) do
    {:ok, %{}}
  end

  @impl true
  def handle_call({:whereis_name, user_name}, _from, state) do
    {:reply, Map.get(state, user_name, :undefined), state}
  end

  @impl true
  def handle_call({:register_name, user_name, pid}, _from, state) do
    case Map.get(state, user_name) do
      nil ->
        monitor = Process.monitor(pid)
        {:reply, :yes, Map.put(state, user_name, pid)}

      _ ->
        {:reply, :no, state}
    end
  end

  @impl true
  def handle_cast({:unregister_name, user_name}, state) do
    {:noreply, Map.delete(state, user_name)}
  end

  @impl true
  def handle_info({:DOWN, ref, :process, pid, reason}, state) do
    {:noreply, remove_pid(state, pid)}
  end

  ## Helpers
  def remove_pid(state, goal_pid) do
    Enum.filter(state, fn {key, pid} -> pid != goal_pid end)
    |> Enum.into(%{})
  end
end
