defmodule Genchat.Member do
  @moduledoc """
  TODO 
  - rework Member process name from to_atom/1 to {:via, ...}
  - add PID to @defive coz I need member list with PIDs. {:via} might help
  """
  @derive {Jason.Encoder, only: [:name, :joined_at]}
  defstruct [
    name: nil,
    pid: nil,
    monitor: nil,
    joined_at: NaiveDateTime.utc_now()
  ]

  use GenServer
  alias Genchat.Server

  @spec start_link([]) :: {:ok, PID}
  def start_link([name: name] = opts) do
    GenServer.start_link(__MODULE__, %{status: "offline"}, name: via_tuple(name))
  end

  @spec join_room(String.t, %{}) :: :ok
  def join_room(room_name, member_name) do
    # create new user
    {:ok, member_pid} = start_link([name: member_name])
    monitor = Process.monitor(member_pid)

    # add to room
    Server.add_new_member_to_room(
      room_name,
      %__MODULE__{
        name: member_name,
        pid: member_pid,
        monitor: monitor
      })

    #Server.send_message(room_name, "#{inspect(member_name)} joined the room")

  end

  def init(member_state) do
    {:ok, member_state}
  end

  def via_tuple(member_name) do
    {:via, Genchat.UsernameRegistry, {:chat_room, member_name}}
  end
end

defmodule Genchat.Schemas.Members do
  @moduledoc """
  TODO:
  - maybe is_user_banned? as integer is not better idea than as a boolean
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Genchat.Schemas.{Room, User}

  schema "rooms_members" do
    belongs_to :room_id, Room
    belongs_to :user_id, User
    field :is_user_banned?, :integer

    timestamps()
  end

  @doc false
  def changeset(room_members, attrs) do
    room_members
    |> cast(attrs, [:room_id, :user_id, :is_user_banned?])
    |> validate_required([:room_id, :user_id, :is_user_banned?])
  end
end
