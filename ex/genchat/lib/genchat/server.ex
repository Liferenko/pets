defmodule Genchat.Server do
  @moduledoc """
  Chat and rooms
  """
  use GenServer
  alias Genchat.{State, Member}

  def start_link(name) do
    GenServer.start_link(__MODULE__, %State{history: [], members: []}, name: via_tuple(:room, name))
  end

  ## Client API (move it to api_controller)
  @doc """
  ## Examples
    iex> Genchat.Supervisor.create_room("bar")
    iex> Genchat.Server.send_message("bar", "Which cigarretes you prefer?")
    :ok
    iex> Genchat.Server.send_message("bar", "")
    iex> Genchat.Server.get_chat_history("bar")
    ["", "Which cigarretes you prefer?"]
  """
  def send_message(room_name, message) do
    GenServer.cast(via_tuple(:room, room_name), {:send_message, message})
  end

  @doc """

  ## Examples
    #iex> Genchat.Server.get_room_members("fellas_room")
    #["What does Marcellos Walles look like?", "Sup, big boy?"]
  """
  def get_room_members(room_name) do
    GenServer.call(via_tuple(:room, room_name), :get_room_members)
  end

  @doc """
  ## Examples
    iex> Genchat.Supervisor.create_room("fellas_room")
    iex> Genchat.Server.send_message("fellas_room", "Sup, big boy?")
    iex> Genchat.Server.send_message("fellas_room", "What does Marcellos Walles look like?")
    iex> Genchat.Server.get_chat_history("fellas_room")
    ["What does Marcellos Walles look like?", "Sup, big boy?"]
  """
  def get_chat_history(room_name) do
    GenServer.call(via_tuple(:room, room_name), :get_chat_history)
  end

  @doc """
  ## Examples
  """
    def add_new_member_to_room(room_name, member) do
      GenServer.cast(via_tuple(:room, room_name), {:add_new_member_to_room, member})
  end



  ## Server
  @impl true
  def init(room_state) do
    {:ok, room_state}
  end


  @impl true
  def handle_call(:get_chat_history, _from, room_state) do
    {:reply, room_state.history, room_state}
  end

  @impl true
  def handle_call(:get_room_members, _from, room_state) do
    {:reply, room_state.members, room_state}
  end

  @impl true
  def handle_cast({:send_message, new_message}, %State{history: history, members: members} = _room_state) do
    # check if number of messages less then 50 and update chat history
    updated_history = keep_msg_history_limited(new_message, history)

    {:noreply, %State{history: updated_history, members: members}}
  end

  @impl true
  def handle_cast({:add_new_member_to_room, new_member}, %State{history: history, members: members} = _room_state) do
    {:noreply, %State{history: history, members: members ++ [new_member]}}
  end

  ## Helpers
  def via_tuple(:room, room_name) do
    {:via, Genchat.RoomRegistry, {:chat_room, room_name}}
  end

  @doc """
  ## Examples
  iex> history = Enum.map(1..5, fn msg -> Integer.to_string(msg) end)
  iex> Genchat.Server.keep_msg_history_limited("Im so freshhh", history)
  ["Im so freshhh", "1", "2", "3", "4", "5"]

  iex> history = Enum.map(1..52, fn msg -> Integer.to_string(msg) end)
  iex> Genchat.Server.keep_msg_history_limited("Im so freshhh", history)
  ["Im so freshhh", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",\
    "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22",\
    "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34",\
    "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47",\
    "48", "49", "50", "51"]
  """
  @spec keep_msg_history_limited(String.t, []) :: []
  def keep_msg_history_limited(new_message, history) do
    # TODO change to 50+
    history_limit = 50
    if length(history) <= history_limit do
      [new_message | history]
    else
      trimmed_history = Enum.drop(history, -1)
      IO.inspect(trimmed_history)
      [new_message | trimmed_history]
    end
  end
end
