defmodule Genchat.Supervisor do
  @moduledoc """
  TODO:
  - replace with DynamicSupervisor
  """
  use Supervisor

  def start_link() do
    Supervisor.start_link(__MODULE__, [], name: :chat_sup)
  end

  def create_room(room_name) do
    Supervisor.start_child(:chat_sup, [room_name])
  end


  @impl true
  def init(_) do
    children = [
      worker(Genchat.Server, [])
    ]

    supervise(children, strategy: :simple_one_for_one)
  end
end

