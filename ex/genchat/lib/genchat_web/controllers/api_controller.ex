defmodule GenchatWeb.ApiController do
  use GenchatWeb, :controller
  @moduledoc false
  require Logger
  alias Genchat.{Helpers, Server, Member}

  ## Chat
  def get_chat_history(conn, params) do
    room_history = Server.get_chat_history(params["room_name"])

    json(conn, room_history)
  end

  ## Message
  # TODO:
  # - move it to Message module
  def send_message(conn, params) do
    case Server.send_message(params["room_name"], params["text"]) do
      :ok ->
        json(conn, "Message sent")

      reason ->
        json(conn, "Error. Something goes wront. Reason: #{reason}")
    end
  end

  def join_room(conn, params) do
    case Member.join_room(params["room_name"], params["name"]) do
      :ok ->
        json(conn, "#{params["name"]} joined #{params["room_name"]}")

      reason ->
        json(conn, "Error. cant find members list. Something goes wront. Reason: #{reason}")
    end
  end

  def get_room_members(conn, params) do
    members_list = Server.get_room_members(params["room_name"])
    json(conn, members_list)
  end
end
