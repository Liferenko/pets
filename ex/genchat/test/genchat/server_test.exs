defmodule Genchat.ServerTest do
  use ExUnit.Case
  doctest Genchat.Server

  setup_all do
    Genchat.RoomRegistry.start_link()
    Genchat.UsernameRegistry.start_link()
    Genchat.Supervisor.start_link()
    # move both this calls to application.ex

    :ok
  end
end
