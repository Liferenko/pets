defmodule GenchatWeb.ApiControllerTest do
  @moduledoc """
  Mimic real-world usage of the chat with made-up users, rooms and messages
  """
  use ExUnit.Case, async: false
  import Phoenix.ConnTest

  @endpoint GenchatWeb.Endpoint

  setup_all do
    Genchat.Supervisor.create_room("Samuel_car") # TODO replace it with API call
    Genchat.Supervisor.create_room("Brett_room") # TODO replace it with API call
    
    # add to the room
    #post(build_conn(), "/api/v1/chat/room/#{room_name}/join", [user_id: 11, name: "Samuel"])
    #post(build_conn(), "/api/chat/room/#{room_id}/join", [user_id: 22, name: "Vincent"])
    #post(build_conn(), "/api/chat/room/#{room_id}/join", [user_id: 33, name: "Random guy"])
    #post(build_conn(), "/api/chat/room/#{room_id}/join", [user_id: 8, name: "The woman in a red dress"])

    :ok
  end

  describe "user Samuel L Jackson" do
  #  test "list all chat rooms" do
  #    conn = get(build_conn(), "/api/chat/list")
  #    
  #    {:ok, %{"msg" => msg}} = Jason.decode(conn.resp_body)

  #    assert msg == "No rooms found"
  #  end

  #  test "tried to join a chat which doesn't exist" do
  #    room_id = 99999
  #    # TODO create user
  #    conn = get(build_conn(), "/api/chat/room/#{room_id}/members")
  #    {:ok, response} = Jason.decode(conn.resp_body)
  #    
  #    assert response == []
  #  end

  #  test "joined a chat room" do
  #    room_id = 34
  #    {_user_id, user_name} = {11, "Samuel"}

  #    # add to the room
  #    #post(build_conn(), "/api/chat/room/#{room_id}/join", [user_id: user_id, name: user_name])

  #    # check members list
  #    conn = get(build_conn(), "/api/chat/room/#{room_id}/members")
  #    {:ok, response} = Jason.decode(conn.resp_body)
  #  
  #    assert response |> Enum.find(fn user -> user["name"] == user_name end)
  #  end

    test "said Whadap fellas" do
      room_name = "Brett_room"

      # Samuel enters the appartments
      post(
        build_conn(),
        "/api/v1/chat/room/#{room_name}/send_message",
        [text: "Hey, kids", to: room_name]
      )

      # Samuel says to seagul-hair guy
      post(
        build_conn(),
        "/api/v1/chat/room/#{room_name}/send_message",
        [text: "Hey, keep chillin'", to: room_name]
      )

      conn = get(build_conn(), "/api/v1/chat/room/#{room_name}/history")
      {:ok, response} = Jason.decode(conn.resp_body)
      assert response == ["Hey, keep chillin'", "Hey, kids"] 
    end
  end
  #
  #describe "user John Travolta" do
  #  test "joined a chat room" do
  #    room_id = 34
  #    {_user_id, user_name} = {22, "Vincent"}

  #    #post(build_conn(), "/api/chat/room/#{room_id}/join", [user_id: user_id, name: user_name])

  #    conn = get(build_conn(), "/api/chat/room/#{room_id}/members")
  #    {:ok, response} = Jason.decode(conn.resp_body)
  #  
  #    assert response |> Enum.find(fn user -> user["name"] == user_name end)
  #  end

  #  test "leave the chat room" do
  #    room_id = 34
  #    {user_id, user_name} = {22, "Vincent"}

  #    #Rooms.create_room(34, "Marcellos Walles' bar", [11, 66])


  #    # remove Vincent from the room
  #    delete(build_conn(), "/api/chat/room/#{room_id}/leave", [user_id: user_id, name: user_name])
  #    
  #    # check members list
  #    conn = get(build_conn(), "/api/chat/room/#{room_id}/members")
  #    {:ok, response} = Jason.decode(conn.resp_body)
  #  
  #    assert response |> Enum.find(fn user -> user["name"] == user_name end)
  #  end

  #  test "saw there is a Samuel in here" do
  #    assert true
  #  end

  #  test "received Whadap fellas from Samuel" do
  #    assert true
  #  end
  #  test "said Do you know Marcelles' wife?" do
  #    assert true
  #  end
  #end

  describe "A Dialog between" do
    test "Samuel, Vincent join the room with 3 fellas" do
      room_name = "Brett_room"

      post(build_conn(), "/api/v1/chat/room/#{room_name}/join", [name: "Samuel"])
      post(build_conn(), "/api/v1/chat/room/#{room_name}/join", [user_id: 12, name: "Vincent"])
      post(build_conn(), "/api/v1/chat/room/#{room_name}/join", [user_id: 13, name: "Fella 1"])
      post(build_conn(), "/api/v1/chat/room/#{room_name}/join", [user_id: 15, name: "Fella 2"])
      post(build_conn(), "/api/v1/chat/room/#{room_name}/join", [user_id: 17, name: "Fella 3"])

      # Check the members
      conn = get(build_conn(), "/api/v1/chat/room/#{room_name}/members")
      {:ok, room_members} = Jason.decode(conn.resp_body)
      dbg(room_members)
     

      assert  Enum.map(room_members, fn mmb -> mmb["name"] end) == ["Samuel", "Vincent", "Fella 1", "Fella 2", "Fella 3"]
    end

  #  test "young fella joined the room > Vincent shoot > young fella left the room"do
  #    # young fella joined the room >
  #    assert true
  #    # Vincent shoot
  #    assert true
  #    # young fella left the room
  #    assert true
  #  end
  end

  #describe "New chat room between Bruce Willis and John Travolta." do
  #  #test "Barman joined the chat room" do
  #  #  assert true
  #  #end
  #  #test "Butch joined the room named Marcellos Bar" do
  #  #  assert true
  #  #end
  #  #test "Butch asked barman for Red Apple cigarettes pack" do
  #  #  assert true
  #  #end

  #  #test "Vincent joined the chat" do
  #  #  assert true
  #  #end

  #  #test "Butch asked Any problems, bro?" do
  #  #  # Butch asks
  #  #  # check if Vincent heard Butch's question
  #  #  assert true
  #  #end

  #  #test "Butch left chat" do
  #  #  assert true
  #  #end

  @tag :this
  test "Lets watch all chat history" do
    room_name = "Samuel_car"
    msg_gotta_be_sent_from_Vincent = "Any problems, bro?"

    ### Vincent sent message to chat room "Samuel car"
    conn = post(build_conn(), "/api/v1/chat/room/#{room_name}/send_message", [text: msg_gotta_be_sent_from_Vincent, to: room_name])
    {:ok, response} = Jason.decode(conn.resp_body)
    assert response == "Message sent"

    conn = get(build_conn(), "/api/v1/chat/room/#{room_name}/history")
    {:ok, response} = Jason.decode(conn.resp_body)
    assert response == ["Any problems, bro?"] 
  end
end
