alias Heychat.Rooms
alias Heychat.State

Rooms.create_room(34, "Marcellos Walles' bar", %State{history: [], members: []})

# add to the room
post(build_conn(), "/api/chat/room/34/join", [user_id: 11, name: "Samuel"])
post(build_conn(), "/api/chat/room/34/join", [user_id: 22, name: "Vincent"])
post(build_conn(), "/api/chat/room/34/join", [user_id: 33, name: "Random guy"])
post(build_conn(), "/api/chat/room/34/join", [user_id: 8, name: "The woman in a red dress"])
