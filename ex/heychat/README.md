### Coding Challenge Guidelines

## How to test?
(http == httpie is an CLI alternative to curl)

>> http POST http://localhost:4012/api/chat/room/34/join user_id=1
>> http GET http://localhost:4012/api/chat/room/34/members

API blueprint
:rooms/list - List chat rooms
room/join?room_id - Join/Leave the chat room

scope "/api" do
  scope "chat" do
  
    scope "room/:room_id" do
      get "/history"
      get "/list"
      post "/join" params - user_id, room_id
      post "/leave" params - user_id, room_id
    end
  
    scope "/message" do
      post "/send"
      post "/pin:message_id" # optional feature
      delete "/:messaage_id"
    end
  end
end

## Schemas:
Message
    message_id
    message_from
    message_to
    content
    timestamps

Room
    id
    title
    timestamps

User
    id
    name
    timestamps

RoomMembers
    room_id
    user_id
    is_user_banned? (TODO: the DB type should be integer (0-1) or bool?)
    timestamps



## Task:
Let's implement a chat room using OTP framework for all our needs:
- [ ] Create a gen_server which will have a state which store last 50 messages
- [x] have a list of chat room participants (using join/leave functions)
- [ ] Send message which stores in last 50 messages history AND broadcasts message to a chat room members (using PID, of course)
- [ ] Please add a monitor by chat room on participants PID to handle room leave when client process dies.
- [ ] Add a ExUnit tests to cover basic chat room API
- [ ] Create a ETS table which could
    - [x] store a list of chat rooms
    - [ ] provide ability to find a proper chat room to:
        - [ ] join Chat Room
        - [ ] leave Chat Room
        - [ ] get Chat Room PID
- [ ] Create a HTTP API Interface to the Chat Room 

#### Let's implement a chat room using OTP framework for all our needs:

Create a gen_server which will have a state which
* store last 50 messages
* have a list of chat room participants (using join/leave functions)
* Send message which stores in last 50 messages history AND broadcasts message to a chat room members (using PID, of course)
* Please add a monitor by chat room on participants PID to handle room leave when client process dies.
* Add a ExUnit tests to cover basic chat room API
* Create a ETS table which could store a list of chat rooms and provide ability to find a proper chat room to join/leave and get Chat Room PID

#### Create a HTTP API Interface to the Chat Room
* List chat rooms
* Join/Leave the chat room
* Get chat history
* Send message to a chat room (ensure that user joined the room before)
* Let's cover up with the tests


QA:
We can have all chat rooms/history in memory only for now
We want to have chat room processes being supervised by supervisor (dynamic or normal)
We don't want to take care about loosing chat history/list of members


### Evaluation Criteria
Try to cover up your code with tests. 
ExUnit + Shell mode is our everything for testing 

### Useful Links
https://livebook.manning.com/book/elixir-in-action/chapter-10/162
https://livebook.manning.com/book/elixir-in-action/chapter-11/1
https://papercups.io/blog/genserver

### CodeSubmit

Please organize, design, test, and document your code as if it were
going into production - then push your changes to the master branch.

Have fun coding! 🚀
