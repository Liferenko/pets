defmodule Heychat.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @rooms_table Application.get_env(:heychat, :ets_rooms_table)
  @users_table Application.get_env(:heychat, :ets_users_table)

  @impl true
  def start(_type, _args) do
    # ETS for rooms and last 50 msgs
    :ets.new(@rooms_table, [:ordered_set, :public, :named_table])
    :ets.new(@users_table, [:ordered_set, :public, :named_table])

    #Heychat.ProcessRegistry.start_link([])

    children = [
      # Start the Ecto repository
      Heychat.Repo,
      # Start the Telemetry supervisor
      HeychatWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Heychat.PubSub},
      # Start the Endpoint (http/https)
      HeychatWeb.Endpoint,
      # Start a worker by calling: Heychat.Worker.start_link(arg)
      # {Heychat.Worker, arg}
      #{Heychat.Rooms, [key: 0]},
      Heychat.MessageSender,
      Heychat.ProcessRegistry,
      Heychat.DynamicSupervisor
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Heychat.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    HeychatWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
