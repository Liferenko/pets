defmodule Heychat.ProcessRegistry do
  @moduledoc """
  Needs for room registration with defferent room names
  """

  def via_tuple(key) when is_tuple(key) do
    {:via, Registry, {__MODULE__, key}}
  end

  def start_link(opts) do
    Registry.start_link(keys: :unique, name: __MODULE__)
  end

  # need it to start ProcessRegistry when app starts
  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]},
      type: :worker,
      restart: :permanent,
      shutdown: 500
    }
  end
end

defmodule Heychat.Rooms do
  @moduledoc """
  Logic for rooms
  TODO:
  - 
  """
  use GenServer
  require Logger
  alias Heychat.Helpers
  alias Heychat.ProcessRegistry, as: Reg
  alias Heychat.{Message, Member, State}
  @rooms_table Application.get_env(:heychat, :ets_rooms_table)
  @users_table Application.get_env(:heychat, :ets_users_table)

  @type t :: %State{
    history: [],
    members: []
  }

  def via_tuple(id) do
    Reg.via_tuple({__MODULE__, id})
  end

  ## Client API
  @spec start_link([]) :: {:ok, PID}
  def start_link(opts) do
    IO.puts("GenServer started | Chat room")

    GenServer.start_link(__MODULE__,
      %State{
        history: [], members: []
      },
      name: via_tuple(Keyword.get(opts, :key)))
  end

  @spec get_rooms_list() :: {:ok, []} | {:error, String.t}
  def get_rooms_list() do
    GenServer.call(__MODULE__, :get_rooms_list)
  end

  @spec add_user_into_room(integer, integer) :: {:ok, [String.t]} | {:error, String.t}
  def add_user_into_room(user_id, room_id) do
    GenServer.cast(__MODULE__, {
      :add_user,
      Helpers.convert_to_integer(user_id),
      Helpers.convert_to_integer(room_id)
    })
  end

  def get_chat_history(room_id) do
    # TODO how to get certain rooms history by room_id
    GenServer.call(__MODULE__, {:history, room_id})
  end
  

  ## Server
  @impl true
  def init(_) do

    {:ok, %State{}}
  end

  @impl true
  def handle_info({:DOWN, ref, :process, pid, reason}, state) do
    Logger.warning("Process #{inspect(pid)} down. Reason: #{inspect(reason)}")
    members = Enum.filter(state.members, fn m -> m.monitor != ref end)
    {:noreply, state}
  end

  @impl true
  def handle_call(:get_rooms_list, _, _), do: list_rooms()
  def handle_call({:get_members, room_id}, from, state) do
    case Enum.map(state.members, fn m -> %Member{name: m.name, pid: m.pid, monitor: m.monitor, joined_at: m.joined_at} end) do
      members_list ->
        {
          :reply,
          {:ok, members_list},
          %{history: state.history, members: members_list}
        }

      [] ->
        {
          :reply,
          {:ok, state.members},
          state
        }
    end

    # @depricated version
    #case :ets.lookup(@rooms_table, room_id) do
    #  [{_id, _title, members_list}] ->
    #    {:reply,
    #      {:ok, members_list},
    #      %{history: state.history, members_list: members_list}
    #    }

    #  _ ->
    #    {
    #      :reply,
    #      {:ok, "Room with id #{room_id} is not found"},
    #      %{history: [], members_list: []}
    #    }
    #end
  end

  def handle_call({:history, room_id}, _from, state) do
    # get history by room. We can find a room PID by room_id
    {:reply, {:ok, state.history}, state}
  end

  @impl true
  def handle_call({:join, name, pid}, _from, %State{} = state) do
    dbg()
    with true <- Process.alive?(pid),
         true <- name != "" do
      monitor = Process.monitor(pid)

      {
        :reply,
        :ok,
        %{
          state | members: [
            %Member{
              name: name,
              pid: pid,
              monitor: monitor
            } | state.members]
        }
      }
    else 
      _ ->
        {:reply, :error, state}
    end
  end
  # deprecated
  #@impl true
  #def handle_cast({:add_user, new_member_id, room_id}, room_state) do
  #  {:noreply, is_room_members_list_updated?(room_id, new_member_id)}
  #end

  def handle_call(:leave, pid, state) do
    # TODO get member_pid by member_id
    members = Enum.filter(state.members, fn members -> members.pid != pid end)
    dbg(members)

    {:reply, :ok, %{state | members: members}}
  end


  ## Helper methods
  @spec list_rooms() :: {:ok, [%{}]} | {:ok, []} | {:error, String.t}
  def list_rooms() do
    case :ets.lookup(@rooms_table, "rooms") do
      list_of_rooms -> {:reply, {:ok, list_of_rooms}, []}
      _             -> {:reply, {:error, "No rooms found"}, []}
    end
  end

  @doc """
  Room number is a key in DB
  """
  @spec create_room(integer, String.t) :: {:ok, PID}
  def create_room(room_number, room_title, members \\ []) do
    # Register new room
    {:ok, pid} = start_link([:key, room_number])
    dbg([pid, room_number])

    # Save it in ETS
    Task.start(fn ->
      :ets.insert_new(@rooms_table, {room_number, room_title, members})
    end)
  end

  @doc """
    Create user or users. List of tuples 
  """
  @spec create_users([{integer, String.t}]) :: :ok | :error
  def create_users(list_of_new_users) do
    Enum.each(
      list_of_new_users, 
      fn {user_id, user_name} ->
        :ets.insert_new(@users_table, {user_id, user_name})
      end
    )
  end

  @deprecated
  @spec is_room_members_list_updated?(integer, integer) :: bool
  def is_room_members_list_updated?(room_id, user_id) do
    case :ets.match(:rooms, {room_id, :"$1", :"$2"}) do
      [[room_title, members]] ->
        :ets.insert(@rooms_table, {room_id, room_title, [user_id | members]})

      [] -> false
    end

  end

end
