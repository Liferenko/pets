defmodule Heychat.MessageSender do
  @moduledoc """
  All for messages sending
  """
  use GenServer
  alias Heychat.Helpers
  alias Heychat.Message

  @typedoc """
  Message struct. Message_id should be unique and contains all data we might need (timestamp at least)
  """
  @type t :: %Message{
    uniq_msg_id: String.t,
    text: String.t
  }
  
  ## Client API
  @spec start_link([]) :: {:ok, PID}
  def start_link(opts) do
    IO.puts("msg GenServer started")
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  
  ## Server
  @impl true
  def init(state) do
    IO.puts("msg init started")
    {:ok, state}
  end

  @impl true
  def handle_call({:send_msg, room_id, text}, from_pid, state) do
    send_message_to_room(text, room_id)
    # TODO save message to room history
    
    {:reply, "msg sent from #{inspect(from_pid)}", []}
  end

  ## Helpers
  @doc """
    from - sender user_id 
    room_id - receiver room_id 
  TODO
  """
  @spec send_message_to_room(
    text :: String.t,
    room_id :: integer
  ) :: :ok | :error
  def send_message_to_room(text, room_id) do
    Helpers.print("Im almost sent \"#{text}\" to room #{room_id}", "me")
    member_pid = self() # Get room_pid by room_id

    send(member_pid, text)
  end

  @doc """
    from - sender user_id 
    to - receiver user_id 
  TODO
  """
  @spec send_message_to_user(
    text :: String.t,
    from :: integer,
    to :: integer
  ) :: :ok | :error
  def send_message_to_user(text, from, to), do: :ok

  @spec add_to_chatroom_msg_history(integer, Message.t) :: :ok | :error
  def add_to_chatroom_msg_history(room_id, %{msg_id: msg_id, text: text}) do

  end
end
