defmodule Heychat.Repo do
  use Ecto.Repo,
    otp_app: :heychat,
    adapter: Ecto.Adapters.Postgres
end
