defmodule Heychat.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :msg_id, :uuid
      add :message_from, :integer
      add :message_to, :integer
      add :content, :string

      timestamps()
    end
  end
end
