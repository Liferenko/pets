defmodule Heychat.Repo.Migrations.CreateRoomsMembers do
  use Ecto.Migration

  def change do
    create table(:rooms_members) do
      add :room_id, references(:rooms)
      add :user_id, references(:users)
      add :is_user_banned?, :integer

      timestamps()
    end
  end
end
