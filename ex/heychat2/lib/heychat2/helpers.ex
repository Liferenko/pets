defmodule Heychat2.Helpers do
  @moduledoc
  """
  Helps anywhere around the code
  """

  def convert_to_integer(payload)
  when is_integer(payload), do: payload

  def convert_to_integer(payload)
  when is_bitstring(payload) do
    String.to_integer(payload)
  end

  def convert_to_integer(_) do
    Logger.warning("Cant be converted")
    :error
  end

  
  @deprecated
  def owner_node(), do: to_string(node())
  @deprecated
  def address(room) when is_bitstring(room), do:  String.to_atom(room) |> address()
  @deprecated
  def address(room), do: {:chat, room}

  @deprecated
  @spec print(String.t, integer) :: String.t
  def print(text, from) do
    [">>", from, text]
    |> IO.ANSI.format(true)
    |> IO.puts()
  end

  def uniq_msg_id(), do: :crypto.strong_rand_bytes(11)
end
