defmodule Heychat2.Repo do
  use Ecto.Repo,
    otp_app: :heychat2,
    adapter: Ecto.Adapters.Postgres
end
