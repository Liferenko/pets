defmodule Heychat2.Member do
  defstruct name: nil,
            pid: nil,
            monitor: nil,
            joined_at: NaiveDateTime.utc_now()

  use GenServer

  @spec start_link([]) :: {:ok, PID}
  def start_link([name: name] = _opts) do
    IO.puts("GenServer started | Member #{inspect(name)}")
    GenServer.start_link(__MODULE__, %{status: "offline"}, name: String.to_atom(name))
  end

  def init(member_state) do
    {:ok, member_state}
  end

  def handle_info(something, from, mm_state) do
    dbg()
    {:noreply, "State", mm_state}
  end
end

defmodule Heychat2.Schemas.Members do
  @moduledoc """
  TODO:
  - maybe is_user_banned? as integer is not better idea than as a boolean
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Heychat2.Schemas.{Room, User}

  schema "rooms_members" do
    belongs_to :room_id, Room
    belongs_to :user_id, User
    field :is_user_banned?, :integer

    timestamps()
  end

  @doc false
  def changeset(room_members, attrs) do
    room_members
    |> cast(attrs, [:room_id, :user_id, :is_user_banned?])
    |> validate_required([:room_id, :user_id, :is_user_banned?])
  end
end
