defmodule Heychat2Web.ApiController do
  use Heychat2Web, :controller
  require Logger
  alias Heychat2.{Helpers, Rooms, MessageSender}

#  ## Chat
#  def list_rooms(conn, _params) do
#    case Rooms.get_rooms_list() do
#      {:ok, []} ->
#        json(conn, %{msg: "No rooms found"})
#
#      {:ok, list_of_rooms} ->
#        json(conn, %{msg: list_of_rooms})
#    end
#  end
#
  def show_room_members(conn, params) do
    # deprecated {:ok, members_list} = Rooms.get_members_by_room_id()
    GenServer.call(Rooms, {:get_members, Helpers.convert_to_integer(params["room_id"])})
    |> case do
      {:ok, members_list} ->
        json(conn, members_list)

      {:error, reason} ->
        Logger.error("Cant get room members list. Reason: #{inspect(reason)}")
        json(conn, "Cant get room members list. Reason: #{inspect(reason)}")
    end
  end
#
#  ## Room API
#  def join_room(conn, params) do
#    # response = Rooms.add_user_into_room(params["user_id"], params["room_id"])
#    # TODO are you sure self/0 is okay as a member pid?
#    {:ok, member_pid} = Heychat2.Member.start_link(name: params["name"])
#
#    # DynamicSupervisor.start_child(Heychat2.DynamicSupervisor, Heychat2.Member.start_link([name: params["name"]]))
#
#    response = GenServer.call(Rooms.via_tuple(34), {:join, params["name"], member_pid})
#    json(conn, response)
#  end
#
#  def leave_room(conn, params) do
#    response = GenServer.call(Rooms, :leave)
#    json(conn, response)
#  end
#
#  def get_room_history(conn, params) do
#    GenServer.call(Rooms, {:history, Helpers.convert_to_integer(params["room_id"])})
#    |> case do
#      {:ok, messages_list} ->
#        json(conn, messages_list)
#
#      {:error, reason} ->
#        Logger.error("Cant get room history. Reason: #{inspect(reason)}")
#        json(conn, "Cant get room history. Reason: #{inspect(reason)}")
#    end
#  end

  ## Message API
  def send_message(conn, %{"from" => _from, "room_id" => room_id, "text" => text}) do
    response = GenServer.call(Heychat2.MessageSender, {:send_msg, room_id, text})
    json(conn, response)
  end
end
