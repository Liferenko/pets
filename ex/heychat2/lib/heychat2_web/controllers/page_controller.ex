defmodule Heychat2Web.PageController do
  use Heychat2Web, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
