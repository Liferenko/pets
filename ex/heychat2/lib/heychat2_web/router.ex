defmodule Heychat2Web.Router do
  use Heychat2Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {Heychat2Web.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Heychat2Web do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", Heychat2Web do
    pipe_through :api

    scope "/chat" do
      get "/list", ApiController, :list_rooms

      scope "/room/:room_id" do
        post "/join", ApiController, :join_room
        delete "/leave", ApiController, :leave_room
        get "/history", ApiController, :get_room_history
        get "/members", ApiController, :show_room_members
      end

      scope "/message" do
        post "/send", ApiController, :send_message
        delete "/:message_id", ApiController, :delete_message_by_id
      end
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", Heychat2Web do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: Heychat2Web.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
