defmodule Heychat2Web.ApiControllerTest do
  use ExUnit.Case, async: false
  import Plug.Conn
  import Phoenix.ConnTest
  alias Heychat2.State

  @endpoint Heychat2Web.Endpoint

  @tag :this
  test "Get room members" do
    room_id = 34

    conn = get(build_conn(), "/api/chat/room/#{room_id}/members")
    {:ok, response} = Jason.decode(conn.resp_body)

    assert response == []
  end

  test "Lets watch all chat history" do
    room_id = 34
    msg_gotta_be_sent_from_Vincent = "Any problems, bro?"

    ## Vincent sent message to chat room 34
    post(build_conn(), "/api/chat/message/send", [text: msg_gotta_be_sent_from_Vincent, to: room_id])
    ##assert_receive "Any problems, bro?"

    #conn = get(build_conn(), "/api/chat/room/#{room_id}/history")
    #{:ok, response} = Jason.decode(conn.resp_body)

    #assert response == ["Any problems, bro?"]
    assert true
  end
end
