# Definition for a binary tree node.

defmodule TreeNode do
  @type t :: %__MODULE__{
    val: integer,
    left: TreeNode.t() | nil,
    right: TreeNode.t() | nil
  }
  defstruct val: 0, left: nil, right: nil
end

defmodule Solution do
  @spec check_tree(root :: TreeNode.t | nil) :: boolean
  def check_tree(root) do
    child_sum = root.left.val + root.right.val
    if root.val == child_sum do
      true
    else
      false
    end
  end

  def test_it() do
    check_tree(%TreeNode{val: 10, right: %TreeNode{val: 4, right: nil, left: nil}, left: %TreeNode{val: 6, right: nil, left: nil}})
  end
end

  end
end
