# Definition for a binary tree node.

defmodule TreeNode do
  @type t :: %__MODULE__{
          val: integer,
          left: TreeNode.t() | nil,
          right: TreeNode.t() | nil
        }
  defstruct val: 0, left: nil, right: nil
end

defmodule Solution do
  @spec get_minimum_difference(root :: TreeNode.t | nil) :: integer
  def get_minimum_difference(root) do
    get_in(root, [Access.all(), :val])
    # return the minimum absolute difference between the values of any two different nodes
  end

  def test_it() do
    map = %TreeNode{
      left: %TreeNode{
        left: %TreeNode{left: nil, right: nil, val: 1},
        right: %TreeNode{left: nil, right: nil, val: 3},
        val: 2
      },
      right: %TreeNode{left: nil, right: nil, val: 6},
      val: 4
    }
    |> Map.to_list

    IO.inspect(map, label: "33")
    get_minimum_difference(map)
  end
end
