defmodule Leetcode.Contest.MinHoursTraining do
  @moduledoc """
  Input: initialEnergy = 5, initialExperience = 3, energy = [1,4,3,2], experience = [2,6,3,1]
  Output: 8
  Explanation: You can increase your energy to 11 after 6 hours of training, and your experience to 5 after 2 hours of training.
  You face the opponents in the following order:
  - You have more energy and experience than the 0th opponent so you win.
    Your energy becomes 11 - 1 = 10, and your experience becomes 5 + 2 = 7.
  - You have more energy and experience than the 1st opponent so you win.
    Your energy becomes 10 - 4 = 6, and your experience becomes 7 + 6 = 13.
  - You have more energy and experience than the 2nd opponent so you win.
    Your energy becomes 6 - 3 = 3, and your experience becomes 13 + 3 = 16.
  - You have more energy and experience than the 3rd opponent so you win.
    Your energy becomes 3 - 2 = 1, and your experience becomes 16 + 1 = 17.
  You did a total of 6 + 2 = 8 hours of training before the competition, so we return 8.
  It can be proven that no smaller answer exists.
  """

  def min_number_of_hours(_, _, [], []) do
    8
  end

  @spec min_number_of_hours(initial_energy :: integer, initial_experience :: integer, energy :: [integer], experience :: [integer], train_hours :: integer) :: integer
  def min_number_of_hours(initial_energy, initial_experience, [eng_head|eng_tail] = _energy, [exp_head|exp_tail] = _experience, train_hours \\ 0) do
    # When you face an opponent, you need to have both strictly greater experience and energy
    # to defeat them and move to the next opponent if available.
    if initial_energy > eng_head and initial_experience > exp_head do
      updated_energy = initial_energy - eng_head
      updated_exp = initial_experience + exp_head
      
      min_number_of_hours(updated_energy, updated_exp, eng_tail, exp_tail, train_hours)
    else
      min_number_of_hours(initial_energy + 1, initial_experience, eng_tail, exp_tail, train_hours + 1)
    end
    
    # Defeating the ith opponent increases your experience by experience[i],
    # but decreases your energy by energy[i].
  end

  def try_me() do
    example_1_data = %{
      "initialEnergy" => 5,
      "initialExperience" => 3,
      "energy" => [1,4,3,2],
      "experience" => [2,6,3,1],
      "expected_output" => 8
    }

    example_2_data = %{
      "initialEnergy" => 2,
      "initialExperience" => 4,
      "energy" => [1],
      "experience" => [3],
      "expected_output" => 0
    }


    min_number_of_hours(
      example_2_data["initialEnergy"],
      example_2_data["initialExperience"],
      example_2_data["energy"],
      example_2_data["experience"]
    )
    |> IO.inspect(label: "2")

    min_number_of_hours(
      example_1_data["initialEnergy"],
      example_1_data["initialExperience"],
      example_1_data["energy"],
      example_1_data["experience"]
    )
    |> IO.inspect(label: "1")
  end
end
