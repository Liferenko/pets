defmodule Solution do
  @spec longest_common_prefix(strs :: [String.t]) :: String.t
  def longest_common_prefix([""]), do: ""
  def longest_common_prefix(strs) do
    [first | [second| third]] =
      strs
      |> Enum.map(fn string -> String.to_charlist(string) |> MapSet.new() end)

    # how to find similar 
    find_intersection(first, second)
    |> find_intersection(Kernel.hd(third))
    |> MapSet.to_list
    |> List.to_string

    #MapSet.to_list |> List.to_string |> longest_common_prefix()
  end

  def find_intersection(mapset1, mapset2) do
    MapSet.intersection(mapset1, mapset2)
  end

  def try_me() do
    longest_common_prefix(["flower","flow","flight"])
    |> IO.inspect

    longest_common_prefix(["dogs","racecar","car"])
    |> IO.inspect

    longest_common_prefix(["strada","striker","string","satellite"])
    |> IO.inspect

    longest_common_prefix(["bca","bcaar","bacar","satellite"])
    |> IO.inspect

    longest_common_prefix([""])
    |> IO.inspect

    longest_common_prefix(["a"])
    |> IO.inspect
  end
end
