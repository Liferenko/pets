defmodule SolutionV2 do
  @spec longest_common_prefix(strs :: [String.t]) :: String.t
  def longest_common_prefix([""]), do: ""
  def longest_common_prefix(strs) do
    case strs |> Enum.map(fn string -> String.to_charlist(string) end) do
      [first|rest] ->
        asdf =
          find_intersection(first, Kernel.hd(rest))
          |> Keyword.get_values(:eq)
          |> List.to_string

        [head|tail] = Kernel.tl(rest)
        longest_common_prefix([asdf, List.to_string(tail)])

      [_|[]] ->
        "-= finish =-"
      _ ->
        "-= finish =-"

    end
  end

  def find_intersection(list1, list2) do
    List.myers_difference(list1, list2)
  end

  def try_me() do
    longest_common_prefix(["flower","flow","flight"])
    |> IO.inspect

    longest_common_prefix(["dogs","racecar","car"])
    |> IO.inspect

    longest_common_prefix(["strada","striker","string","satellite"])
    |> IO.inspect

    longest_common_prefix(["bca","bcaar","bacar","satellite"])
    |> IO.inspect

    longest_common_prefix([""])
    |> IO.inspect

    longest_common_prefix(["a"])
    |> IO.inspect
  end
end
