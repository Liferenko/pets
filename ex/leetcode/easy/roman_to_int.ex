defmodule Solution do
  @example_map %{1 => "I", 5 => "V", 10 => "X", 50 => "L", 100 => "C", 1000 => "M"}

  @spec convert_letter_to_digit(String.t) :: integer
  def convert_letter_to_digit(roman_letter) do
    case roman_letter do
      # add exception (IV, IX, CM)
      "I" -> 1
      "V" -> 5
      "X" -> 10
      "L" -> 50
      "C" -> 100
      "D" -> 500
      "M" -> 1000
    end
  end

  @doc """
  Check and resolve pair of letters when letter I stands before any other
  """
  def handle_roman_exceptions(roman_string) do
    # scan string to find pairs
    exceptions = ["IV", "IX", "XL", "XC", "CD", "CM"]
    String.contains?(roman_string, exceptions)
  end

  def convert_exception_pair_letters(pair_letters) do
    pair_letters
    |> String.replace("IV", "IIII")
    |> String.replace("IX", "VIIII")
    |> String.replace("XL", "XXXX")
    |> String.replace("XC", "LXXXX")
    |> String.replace("CD", "CCCC")
    |> String.replace("CM", "DCCCC")
  end

  def exclude_pair_from_list(list) do
    #Enum.find_index()
  end

  @spec roman_to_int(s :: String.t) :: integer
  def roman_to_int(s) do
    case handle_roman_exceptions(s) do
      true ->
        s
        |> convert_exception_pair_letters()
        |> roman_to_int()
      _ ->
        list_of_letters = String.split(s, "", trim: true)

        Enum.map(list_of_letters, fn letter -> convert_letter_to_digit(letter) end)
        |> Enum.sum()
    end

  end

  def try_me() do
    [
      "III",
      "LVIII",
      "MCMXCIV",
      "MMXXII"
    ]
    |> Enum.map(
      fn list -> roman_to_int(list) end
    )
    
  end
end
