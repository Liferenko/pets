#!/bin/bash
input="file.txt"
# solution 1
#sed -n '10p' $input

# Solution 2 (unfinished)
#while IFS= read -r line
#do
#    acc=1;
#    if [[ $acc == 10 ]]; then
#        echo "$line";
#        break;
#    else 
#      acc=$acc+1;
#      echo $acc;
#      continue;
#    fi
#done < "$input"

# Solution 3
#tail -n+10 $input | head -n1

# Solution 4
perl -e '$cntr = 1; while(<>) { print $_ if $cntr == 10; $cntr++; }' file.txt
