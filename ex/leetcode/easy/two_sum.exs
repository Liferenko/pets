defmodule Solution do
  @spec two_sum(nums :: [integer], target :: integer, acc :: integer) :: [integer]
  def two_sum([head|tail] = nums, target, acc \\ 0) do
    IO.inspect(nums, label: "step #{acc} - ")
    # get n and n+1 
    expect = target - head
    case Enum.find_index(tail, fn x -> x == expect end) do
      nil ->
        two_sum(tail, target, acc + 1)

      second_digit ->
        [acc, second_digit + acc + 1]
    end
  end

  def try_me() do
    two_sum([2,4,11, 9], 6) |> IO.inspect(label: "2,4,11,9")
    two_sum([3,2,3], 6) |> IO.inspect(label: "3,2,3")
    two_sum([3,2,4], 6) |> IO.inspect(label: "3,2,4")
  end
end
