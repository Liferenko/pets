defmodule Solution do
  @spec longest_common_prefix(strs :: [String.t]) :: String.t
  def longest_common_prefix(strs) do
    strs
    |> Enum.map(fn string -> String.to_charlist() end)

  end

  def try_me() do
    longest_common_prefix(["flower","flow","flight"])
    longest_common_prefix(["dogs","racecar","car"])
  end
end
