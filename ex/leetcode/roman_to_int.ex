defmodule Solution do
  @example_map %{1 => "I", 5 => "V", 10 => "X", 50 => "L", 100 => "C", 1000 => "M"}

  @spec convert_letter_to_digit(String.t) :: integer
  def convert_letter_to_digit(roman_letter) do
    case roman_letter do
      "I" -> 1
      "V" -> 5
      "X" -> 10
      "L" -> 50
      "C" -> 100
      "M" -> 1000
    end
  end
    
  @spec roman_to_int(s :: String.t) :: integer
  def roman_to_int(s) do

    list_of_letters = String.split(s, "", trim: true)

    Enum.map(list_of_letters, fn letter -> convert_letter_to_digit(letter) end)
    |> IO.inspect(label: "=-=-=-=-")
  end
end
