#!/bin/bash
input="file.txt"
while IFS= read -r line
do
case $line in
    1 | 2 | 3 | 4 ) echo "| --------| ";;
    * ) echo $line;;
esac
done < "$input"

