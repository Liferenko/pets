defmodule Manage do
  @moduledoc """
  Documentation for `Manage`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Manage.hello()
      :world

  """
  def hello do
    :world
  end
end
