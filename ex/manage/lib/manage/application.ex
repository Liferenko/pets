defmodule Manage.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    Manage.Supervisor.start_link()
  end
end

defmodule Manage do
  def new(restaurant_name) do
    {:ok, pid} = DynamicSupervisor.start_child(Manage.Supervisor.DynamicSupervisor, {Manage.Worker.Server, restaurant_name})
    pid
  end
end

defmodule Manage.Supervisor do
  use Supervisor

  @name __MODULE__

  def start_link do
    Supervisor.start_link(__MODULE__, [], name: @name)
  end

  def init(_) do
    children = [
      # Starts a worker by calling: Manage.Worker.start_link(arg)
      {DynamicSupervisor, strategy: :one_for_one, name: Manage.Supervisor.DynamicSupervisor},
      Manage.Worker.Server,
    ]

    opts = [strategy: :one_for_one]
    Supervisor.init(children, opts)
  end
end

defmodule Manage.Worker.Server do
  use GenServer
  alias Manage.Restaurant

  def start_link(restaurant_name) do
    GenServer.start_link(__MODULE__, restaurant_name)
  end

  @impl true
  def init(restaurant_name) do
    {
      :ok, Restaurant.new(restaurant_name)
    }
  end
end

defmodule Manage.Restaurant do
  defstruct(
    name: "",
    status: "open"
  )

  def new(name) do
    %__MODULE__{name: name}
  end

  def open?(%__MODULE__{status: "open"}), do: true
  def open?(%__MODULE__{} = _not_open_restaurant), do: false

  def close!(%__MODULE__{} = restaurant) do
    %__MODULE__{restaurant | status: "closed"}
  end
end
