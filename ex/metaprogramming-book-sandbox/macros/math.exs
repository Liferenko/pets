defmodule Math do
  defmacro say({:+, _, [lefthandside, righthandside]}) do
    quote do
      lefthandside = unquote(lefthandside)
      righthandside = unquote(righthandside)
      result = lefthandside + righthandside

      IO.puts("#{lefthandside} plus #{righthandside} is #{result}")
      result
    end
  end

  defmacro say({:*, _, [lefthandside, righthandside]}) do
    quote do
      lefthandside = unquote(lefthandside)
      righthandside = unquote(righthandside)
      result = lefthandside * righthandside

      IO.puts("#{lefthandside} times #{righthandside} is #{result}")
      result
    end
  end

end
