# MinimalServer

**TODO:

1. [x] Init with [Sauce I worked with -](https://blog.lelonek.me/minimal-elixir-http2-server-64188d0c1f3a)
2. [ ] Continue from a part "Binding them together" (51% of post)

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `minimal_server` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:minimal_server, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/minimal_server](https://hexdocs.pm/minimal_server).

