# MusicBroadcaster

**TODO:**

v1:
- [ ] let user join the stream (from list "Streams around")
- [ ] let dj create a stream with sound from their device (video, music app etc)
  - [ ] find out the ways one device can catch the stream from a broadcaster (wifi/bluetooth/LoRi/any other way)


v2:
  - [ ] find out how dj_user can choose which content to stream from their device (all sounds, music app, Youtube sounds etc)

v3:
- [ ] device-agnostic service: you may join from mobile, from tablet, from vehicle's device or from your laptop in a caffee

## Use cases
- Traffic jam. You as a driver is looking for a dj around. Found few of streams. Join certain stream and see some vibers around. Mood up!
- You are in a subway. Found few streams around. Join certain stream and see few people on the same vibe. Mood up!
- You are in a caffee. And you exhausted of your music already. Join certain stream. Catch the eye of person who's streaming it. Mood up!


## Diagram 
```mermaid
  sequenceDiagram
    dj_user->>+broadcast_server: "create_stream"
    broadcast_server-->>+audience: "notify_audience_about_new_stream/1"
    audience->>+broadcast_server: "subscribe_to_stream/1"
    user->>+audience: "join_stream(stream_id)"

    user-->>+audience: "leave_stream(stream_id)"
```

## Stack
- Audio streaming - Membrane Framework

