defmodule MusicBroadcaster do
  @moduledoc """
  Documentation for `MusicBroadcaster`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> MusicBroadcaster.hello()
      :world

  """
  def hello do
    :world
  end
end
