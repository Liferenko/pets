defmodule Walkingaround do
  @moduledoc """
  Documentation for Walkingaround.
  """
  use GenServer

  # Client
  def open() do
    GenServer.start_link(__MODULE__, 0, name: __MODULE__)
  end

  def store(amount) do
    # Here I may call a heave requeest
    GenServer.cast(__MODULE__, {:store, amount})
  end

  def withdraw(amount) do
    GenServer.cast(__MODULE__, {:withdraw, amount})
  end

  def get_balance() do
    # Here I may ask for result
    GenServer.call(__MODULE__, :balance)
  end


  # Server
  def init(balance) do
    {:ok, balance}
  end

  # for Exsurance and heavy requests: GenServer.cast -> handle_cast() -> GenServer.call

  def handle_cast({:store, amount}, balance) do
    :timer.sleep(8000) # Here I may exec a heavy request
    {:noreply, balance + amount}
  end

  def handle_cast({:withdraw, amount}, balance) do
    {:noreply, balance - amount}
  end

  def handle_call(:balance, _from, balance) do
    {:reply, balance, balance}
  end










  """
  """

  @doc """
  Hello world.

  ## Examples

      iex> Walkingaround.hello()
      :world

  """
  def hello do
    :world
  end

  @doc """
  Quick math



      iex> Walkingaround.quick_math([3,2])
      5

      iex> Walkingaround.quick_math([700,190])
      890
  """
  def quick_math([left, right]) do
    left + right
  end
end
