# Pbt

**TODO: Add description**
My personal notes:
- Elixir users may have to remove the _build/propcheck.ctex file before
rerunning tests to ensure fresh runs every time. || after this I can 'mix test'



## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `pbt` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:pbt, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/pbt](https://hexdocs.pm/pbt).

