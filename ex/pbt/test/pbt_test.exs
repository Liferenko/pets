defmodule PbtTest do
  use ExUnit.Case
  use PropCheck

  ########### Properties #################
  # Example #2 (page 35)
  property "here Im free to write any test descriptoons I need" do
    forall type <- my_type() do
      boolean(type)
    end
  end

  property "finds the biggest element" do
    forall x <- non_empty( list(integer()) ) do
      biggest(x) == List.last(Enum.sort(x))
    end
  end

  # Example #2 (page 35)
  property "finds the biggest element" do
    forall x <- non_empty( list(integer()) ) do
      Pbt.biggest(x) == model_biggest(x) 
    end
  end


  ############ Helpers ###################
  defp boolean(_) do
    true
  end

  def biggest( [head | tail] ) do
    biggest(tail, head)
  end
  defp biggest([], max) do
    max
  end
  defp biggest([head | tail], max) when head >= max do
    biggest(tail, head)
  end
  defp biggest([head | tail], max) when head < max do
    biggest(tail, max)
  end
  
  # Example #2 (page 35)
  def model_biggest(list) do
    List.last(Enum.sort(list))
  end

  # Generators? G of what? Random data?
  def my_type() do
    term()
  end
end
