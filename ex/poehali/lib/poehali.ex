defmodule POEHALI do
  @moduledoc """
  Documentation for POEHALI.
  """

  @doc """
  Hello world.

  ## Examples

      iex> POEHALI.hello()
      :world

  """
  def hello do
    :world
  end
end
