# PoehaliPhx

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## TODO [sauce](https://gitlab.com/Liferenko/pets/-/boards/1335330)

  - [x] Continue reading and doing - [here](https://hexdocs.pm/phoenix/adding_pages.html)
  - [ ] Write 2 "R-G-Rf" functions [should continue from this part](https://hexdocs.pm/phoenix/testing_schemas.html#content)
