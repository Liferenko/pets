use Mix.Config

# Configure your database
config :poehali_phx, PoehaliPhx.Repo,
  username: "postgres",
  password: "postgres",
  database: "poehali_phx_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :poehali_phx, PoehaliPhxWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
