defmodule PoehaliPhx.Repo do
  use Ecto.Repo,
    otp_app: :poehali_phx,
    adapter: Ecto.Adapters.Postgres
end
