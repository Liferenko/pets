defmodule PoehaliPhxWeb.PageController do
  use PoehaliPhxWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
