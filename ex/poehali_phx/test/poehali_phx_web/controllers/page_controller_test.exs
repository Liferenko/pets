defmodule PoehaliPhxWeb.PageControllerTest do
  use PoehaliPhxWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Теперь тут хотя бы шото внятное написано. Да, Пабло?!"
    assert html_response(conn, 200) =~ "A productive web "
  end
end
