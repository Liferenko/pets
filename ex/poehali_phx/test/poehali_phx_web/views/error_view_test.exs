defmodule PoehaliPhxWeb.ErrorViewTest do
  use PoehaliPhxWeb.ConnCase, async: true

  @moduletag error_view_case: "TODO"

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  @tag another_one: "TODO"
  test "renders 404.html" do
    assert render_to_string(PoehaliPhxWeb.ErrorView, "404.html", []) == "Not Found"
  end

  @tag we_can_write_here_whateve_we_want: "TODO"
  test "renders 500.html" do
    assert render_to_string(PoehaliPhxWeb.ErrorView, "500.html", []) == "Internal Server Error"
  end
end
