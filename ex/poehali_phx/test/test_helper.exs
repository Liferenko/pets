ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(PoehaliPhx.Repo, :manual)

ExUnit.configure(exclude: [error_view_case: true])
