defmodule RabbitmqT do
  @moduledoc """
  Documentation for `RabbitmqT`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> RabbitmqT.hello()
      :world

  """
  def hello do
    :world
  end
end
