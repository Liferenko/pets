defmodule RabbitMQ.Sandbox do
  require AMQP

  def send_msg(msg) do
    with {:ok, conn} = AMQP.Connection.open(),
      {:ok, channel} = AMQP.Channel.open(conn) do
      AMQP.Queue.declare(channel, "default_queue") # Here we declared an


      # the queue name needs to be specified in the `routing_key` param
      AMQP.Basic.publish(channel, "", "default_queue", msg)

      IO.puts("msg has been sent")
        IO.puts("pub: [x] msg has been sent")

      AMQP.Connection.close(conn)
    end
  end

  def receive_msg() do
    with {:ok, conn} = AMQP.Connection.open(),
      {:ok, channel} = AMQP.Channel.open(conn) do
      AMQP.Queue.declare(channel, "default_queue") # Here we declared an
      AMQP.Basic.consume(channel, "default_queue", nil, no_ack: true)

      wait_for_msg()
    end
  end

  def wait_for_msg() do
    receive do
      {:basic_deliver, payload, meta} ->
        IO.inspect(payload, label: "subs: [x] msg:")
        IO.puts("press CTRL+C to exit")

        wait_for_msg()
    end
  end
end
