with {:ok, conn} = AMQP.Connection.open(),
  {:ok, channel} = AMQP.Channel.open(conn) do
  AMQP.Queue.declare(channel, "default_queue") # Here we declared an


  # the queue name needs to be specified in the `routing_key` param
  AMQP.Basic.publish(channel, "", "default_queue", "my message is here")

  IO.puts("msg has been sent")
   
  AMQP.Connection.close(conn)
end
