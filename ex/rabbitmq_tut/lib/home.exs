defmodule RabbitmqTut.Home do
  use AMQP

  def warm_start() do
    {:ok, connection} = AMQP.Connection.open()
    {:ok, channel} = AMQP.Channel.open(connection)
    
    {:all_good, connection, channel}
  end
end
