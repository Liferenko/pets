{:ok, connection} = AMQP.Connection.open()
{:ok, channel} = AMQP.Channel.open(connection)

AMQP.Queue.declare(channel, "oi_task_q", durable: true)

message = 
  case System.argv do
    [] -> "Oi mate!"
    words -> Enum.join(words, " ")
  end

AMQP.Basic.publish(channel, "", "oi_task_q", message, persistent: true)
IO.puts("[x] Sent msg #{message}")

AMQP.Connection.close(connection)
