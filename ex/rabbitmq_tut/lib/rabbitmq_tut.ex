defmodule RabbitmqTut do
  @moduledoc """
  Documentation for `RabbitmqTut`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> RabbitmqTut.hello()
      :world

  """
  def hello do
    :world
  end
end
