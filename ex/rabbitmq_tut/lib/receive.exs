defmodule Receiver do
  def wait_for_msg do
    receive do
      {:basic_deliver, payload, meta} ->
        IO.puts("Received a message - #{payload}")
        wait_for_msg()
    end
  end
end

{:ok, connection} = AMQP.Connection.open()
{:ok, channel} = AMQP.Channel.open(connection)
AMQP.Queue.declare(channel, "oi")
AMQP.Basic.consume(channel, "oi", nil, no_ack: true)
IO.puts " [*] Waiting for messages. To exit press CTRL+C, CTRL+C"

Receiver.wait_for_msg()
