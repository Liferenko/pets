{:ok, connection} = AMQP.Connection.open
{:ok, channel} = AMQP.Channel.open(connection)

queue = "oi"
payload = DateTime.utc_now() |> DateTime.to_string()
AMQP.Queue.declare(channel, queue)
|> IO.inspect
AMQP.Basic.publish(channel, "", queue, payload)
|> IO.inspect

IO.puts("[x] Sent #{payload}")

AMQP.Connection.close(connection)
