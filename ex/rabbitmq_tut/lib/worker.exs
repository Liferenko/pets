defmodule Home do
  use AMQP

  def warm_start() do
    {:ok, connection} = AMQP.Connection.open()
    {:ok, channel} = AMQP.Channel.open(connection)
    
    {:all_good, connection, channel}
  end
end

defmodule Worker do
  def wait_for_msg(channel) do
    receive do
      {:basic_deliver, payload, meta} ->
        IO.puts("Received #{payload}")
        payload
        |> to_charlist()
        |> Enum.count(fn x -> x == ?. end)
        |> Kernel.*(1000)
        |> :timer.sleep()
        IO.puts("Done. #{DateTime.utc_now |> DateTime.to_string}")

        wait_for_msg(channel)
    end
  end
end

{:all_good, connection, channel} = Home.warm_start()

AMQP.Queue.declare(channel, "oi_task_q", durable: true)
AMQP.Basic.consume(channel, "oi_task_q")
IO.puts "[<>] Waiting for messages"

Worker.wait_for_msg(channel)
