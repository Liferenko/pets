import Config

config :smartass, Repo,
  database: "smartass_dev",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  # OR use a URL to connect instead
  url: "postgres://postgres:postgres@localhost/smartass_dev"
