defmodule Mix.Tasks.CreateModule do
  use Mix.Task

  @shortdoc "Creates a new module + test file in the current project."
  @moduledoc """
  Creates a new module in the current project with draft test file.

  ## Examples

      mix gen.module MyModule
      mix gen.module MyModule --sup
      mix gen.module MyModule --sup --app my_app
  """

  @impl Mix.Task
  def run([file_name]) do
    app_dir = File.cwd!()
    app_name = Mix.Project.config()[:app] |> Atom.to_string()
    new_file_path = Path.join([app_dir, "lib", app_name, "#{file_name}.ex"])
    new_test_file_path = Path.join([app_dir, "test", app_name, "#{file_name}_test.exs"])

    if File.exists?(new_file_path) do
      IO.puts("File already exists: #{new_file_path}")
    else
      # create module file
      with :ok <-
             File.write(
               new_file_path,
               module_file_content(
                 "#{String.capitalize(app_name)}.#{String.capitalize(file_name)}"
               ),
               [:write]
             ) do
        IO.puts("Created file: #{new_file_path}")

        # create test file
        File.write(
          new_test_file_path,
          test_file_content("#{String.capitalize(app_name)}.#{String.capitalize(file_name)}Test"),
          [:write]
        )

        IO.puts("Created test file: #{new_test_file_path}")
      else
        error ->
          IO.puts("Error: #{inspect(error)}")
      end
    end
  end

  def module_file_content(module_name) do
    """
    defmodule #{module_name} do
      @moduledoc # TODO

      @spec TODO() :: TODO
      def TODO()  do
        # TODO
      end
    end
    """
  end

  # create test file
  def test_file_content(module_name) do
    """
    defmodule #{module_name} do
      use ExUnit.Case

      describe "TODO" do
        test "TODO" do
          assert true
        end
      end
    end
    """
  end
end
