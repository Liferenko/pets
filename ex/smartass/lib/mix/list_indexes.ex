defmodule Mix.Tasks.ListIndexes do
  use Mix.Task
  import Ecto.Query
  alias Smartass.{Repo}

  """
  Source - https://abulasar.com/creating-tabular-representation-of-database-indexes-using-elixir-mix-task?utm_source=elixir-merge
  """

  @shortdoc "To get DB indexes"

  def run(_args) do
    Mix.Task.run("app.start", [Repo])

    query =
      from(p in "pg_indexes",
        select: %{index_name: p.indexname, table_name: p.tablename},
        where: p.schemaname == "public"
      )

    rows = Repo.all(query)
  end

  # Helpers
  def longest_columns_values() do

  end
end
