defmodule Smartass do
  @moduledoc """
  Documentation for `Smartass`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Smartass.hello()
      :world

  """
  def hello do
    :world
  end
end
