defmodule Bmi do
  @moduledoc """
  Documentation for Bmi.
  """
  def compute(weight_kg, height_m) do
    weight_kg / (height_m * height_m)
  end

  def prefix(first_word) do
    fn (second_word) ->
      "#{first_word} #{second_word}"
    end
  end
end
