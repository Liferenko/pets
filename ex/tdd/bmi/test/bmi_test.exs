defmodule BmiTest do
  use ExUnit.Case
  doctest Bmi

  test "The BMI for a person 1.80m, 75kg is 23.1" do
    assert Bmi.compute(75, 1.80) == 23.148148148148145
  end
  test "The BMI for a person 1.86m, 104kg is 30.06" do
    assert Bmi.compute(104, 1.86) == 30.061278760550348
  end
  test "The BMI for a person 1.50m, 70kg is 31.1" do
    assert Bmi.compute(70, 1.5) == 31.11111111111111
  end
end
