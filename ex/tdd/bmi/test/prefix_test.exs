defmodule PrefixTest do
  use ExUnit.Case
  
  prefix = fn first_word -> fn second_word -> "#{first_word} #{second_word}" end end
  mrs = prefix.("Mrs")
  

  test "add mrs to prefix", mrs do
    assert mrs.("Smith") == "Mrs Smith"
  end
  
  test "add second word in anon func", prefix do
    assert prefix.("Elixir").("Rocks") == "Elixir Rocks"
  end
end
