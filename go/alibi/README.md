Native Andr\iOS app powered by Go

Main goal:
- to record all data to save an owner's ass in extreme situations.
    - Data: short audios, GIS data, data from fromt amd rear cams. 


How to start it:
~instuction's coming soon~


Structure:
```
- Web application (http request -> funcs -> http response)
    - Use-case (request to the input -> func -> result to the output)
        - Service (there is data to the input -> result to the output)
            - Low-level operation (some input-> func -> output)
            - Low-level operation 
            - Low-level operation 
        - Service     
            - Low-level operation (some input-> func -> output)
            - Low-level operation 
            - Low-level operation 
        - Service
            - Low-level operation (some input-> func -> output)
            - Low-level operation 
            - Low-level operation 
    - Use-case (request to the input -> func -> result to the output)
        - Service (there is data to the input -> result to the output)
            - Low-level operation (some input-> func -> output)
            - Low-level operation 
            - Low-level operation 
        - Service     
            - Low-level operation (some input-> func -> output)
            - Low-level operation 
            - Low-level operation 
        - Service
            - Low-level operation (some input-> func -> output)
            - Low-level operation 
            - Low-level operation 
```


**Architecture**:
- /app
    -- main.go // starts all this code
    -- data-manager.go (CRUD data)
    -- logger.go
    -- /handlers
        -- audio.go
        -- gps.go
        -- video.go
    -- big-red-button.go // emergency DOs runner 
- /platforms
    -- /android
        -- /build
    -- /ios
        -- /build
    


How to deploy it to a device:
[official instuction](https://github.com/golang/go/wiki/Mobile#native-applications)
