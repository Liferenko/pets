/*

Source of documentation:
    - portAudio - https://godoc.org/github.com/gordonklaus/portaudio

*/

package audio

import (
	"github.com/gordonklaus/portaudio"
)

// TODO read more about func DefaultInputDevice() (*DeviceInfo, error)
func check_audio_device() {
	// input - audio device ID

	// return: bool (is the microphone enable or disable)

	// output "True\False about audio device state"
}

func connect_to_device() {
	// what's on the input?
	// device_ID
	// channels
	// file_format

	// what is output?
	// output is "Connecting is successful."
}

func record() {
	// what is input?
	// start new record

	// stop new record:w
	// Say to a logger:"This record stopped and "

	// what is output?
}
