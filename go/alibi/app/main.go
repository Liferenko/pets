/*
    Description: Alibi is a blackbox for small devices.
    This file is a root: it roles all parts together as a small workers.

    Author: Paul Liferenko (https://t.me/Liferenko)

Doc source - https://github.com/golang/go/wiki/Mobile#native-applications


*/

package main

import (
	"fmt"
	"handlers/audio"
)

func main() {
	// start the app
	if command.start == True {
		// here we needed of connection with UI - button with text "start it"
		// another case - when this app is starting in a background each time phone is on.

		// start a logger
		audio.record()
	} else {
		// stop the app (manually or before phone will be off)
		fmt.Println("Program stopped. Are you sure you want it?")
	}

}

func save_rec_to_folder() {
	// TODO create folder
}

func collect_roasted_record() {
	// drop roasted record to the trash folder

	// remove the records which already useless (15 min limit)
}
