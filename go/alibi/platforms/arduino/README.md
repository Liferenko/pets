**Project status**
2/9/2019 - Arduino Uno + necessary part were ordered (I'm waiting for delivery)
2/10/2019 - Init the idea

**Stuff:**
- Arduino Uno 
- SD card
- ISD1820 PROPAGANDA

What should it do:
*first itereation*
- to record a sound [/ a part ISD1820 PROPAGANDA](https://chipster.ru/catalog/arduino-and-modules/audio/2411.html)
- to remove an old record
- to work 24/7

*second iteration*
- to detect a sound for no shooting a silence [/parts needed](https://chipster.ru/catalog/arduino-and-modules/sensor-modules/2061.html)
- to make video records
- to make "Send this rec to" manipulated from button (android\ios)
- to send an important rec to Google Drive \ AWS \ Mail.Cloud

*third iteration*:
- Erlang + arduino
- make few arduinos work as a cluster (messaging w/ Erlang OTP)


**Sources**:
- [official Go-hardware packages](https://github.com/rakyll/go-hardware)
- [Go и ардуино/ RU](https://4gophers.ru/articles/go-i-arduino/#.XGCPD8szZhE)
- [Control your arduino with golang](https://www.reddit.com/r/golang/comments/488wcz/what_about_compiling_go_code_to_arduino_code/)
