/*
  Echo sketch
  Day 1 of N with Arduino Uno
*/
int ledPin = 13;

void setup() 
{
    pinMode(ledPin, OUTPUT);  
}

void loop() 
{
  digitalWrite(ledPin, HIGH);
  delay(1000);
  digitalWrite(ledPin, LOW);
  delay(300);
}
