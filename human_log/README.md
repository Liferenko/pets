## Human.log - logging completely all data you do/in/out.

** How to install **
- [ ] Be a human
- [ ] Install `pipl install humanlog` as a root in your mind
- [ ] Accept to monitor your all 5 senses
- [ ] Check if all in- and out-activities are writing in log-file
