** Algorithms and Data Structures Interview questions **

[Source of questions ->](https://amcbridge.com.ua/ua/questions)a
TODO: continue from #4 to #9

[Big-O cheetsheet](http://bigocheatsheet.com/)

*1. What main data structures do you know?*

- Linear data structures
-- arrays 
-- lists

- [Trees](https://medium.com/the-renaissance-developer/learning-tree-data-structure-27c6bb363051)

- [Hashes](https://www.geeksforgeeks.org/hashing-data-structure/#easyHashing)

- [Graphs](http://interactivepython.org/runestone/static/pythonds/SortSearch/Hashing.html)
[visual algo ->](https://visualgo.net/en/matching?slide=1)

[nice one for Pythonians](https://devopedia.org/python-data-structures)

*2. As a data container, what are the main differences between array and list?*

The biggest difference is in the idea of direct access Vs sequential access. Arrays allow both; direct and sequential access, while lists allow only sequential access. And this is because the way that these data structures are stored in memory.

In addition, the structure of the list doesn’t support numeric index like an array is. And, the elements don’t need to be allocated next to each other in the memory like an array is. 

[More ->](https://www.quora.com/What-is-the-difference-between-an-ARRAY-and-a-LIST)
[more about array and lists in NumPy ->](https://docs.scipy.org/doc/numpy/glossary.html#term-array)


*3. What is the difference between singly linked list and doubly linked list data structures?*

Advantages over singly linked list
1) Can be traversed in both forward and backward direction.
2) The delete operation is more efficient (take a look at p4 below and contrast it with the delete operation for a doubly linked list).

Disadvantages over singly linked list
1) Every node requires extra space for a previous pointer.
2) All operations require an extra pointer to be maintained.



*4. What is the difference between stack and queue data structures?*

My answer: main difference lay into data extracting process. Stack - LIFO; queue is FIFO. Stack example - ammo bullet magazine. Queue example is ... queue or line in market :)

[More ->](https://techdifferences.com/difference-between-stack-and-queue.html)




*5. What is algorithm complexity?*

Big O. It shows how many steps we need to making main operations with data set: CRUD. 

[More ->](https://devopedia.org/algorithmic-complexity)



*6. What are associative unordered and ordered containers?*


-Ordered Associative Container

--Standard Traversal encounters elements in sorted order

-- Order predicate may be specified

--Default order predicate is "less than", defined using operator< for the element type

--Popular implementations: OrderedVector, BinarySearchTree

--Search operations required to have O(log n) runtime

--Insert, Remove operations should either be seldom used or have O(log n) runtime



-Unordered Associative Container

--Standard Traversal encounters elements in unspecified order

--Search, Insert, Remove operations should have average-case constant runtime

--Popular implementations use hashing



[Source ->](http://www.cs.fsu.edu/~lacher/courses/REVIEWS/cop4531/containers2/slide04.html)


**7. Explain what the binary tree is.**

A binary tree is made of nodes, where each node contains a "left" reference, a "right" reference, and a data element. The topmost node in the tree is called the root. 

Binary trees are a common data structure for accessing data quickly. 


*Advantages of trees*
--Trees reflect structural relationships in the data
--Trees are used to represent hierarchies
--Trees provide an efficient insertion and searching
--Trees are very flexible data, allowing to move subtrees around with minumum effort 

[Source + more ->](https://www.cs.cmu.edu/~adamchik/15-121/lectures/Trees/trees.html)

[More ->](http://btechsmartclass.com/DS/U5_T1.html)

*8. What is the search time complexity for binary tree? Why? Is it guaranteed?*

BST is a special type of binary tree in which left child of a node has value less than the parent and right child has value greater than parent. 

[Source + more ->](https://www.geeksforgeeks.org/complexity-different-operations-binary-tree-binary-search-tree-avl-tree/)

Complexity formula: O(log n).
Main concept of search - "halving at every step".

[Source ->](https://github.com/tim-hr/stuff/wiki/Time-complexity:-Binary-search-trees)


The time complexity for a single search in a balanced binary search tree is O(log(n)). Maybe the question requires you to do n searches in the binary tree, hence the total complexity is O(nlog(n)).

The worst case complexity for a single search in an unbalanced binary search tree is O(n). And similarly, if you are doing n searches in the unbalanced tree, the total complexity will turn out to be O(n^2).


[Answer (should be double checked) ->](https://stackoverflow.com/questions/41054981/what-is-the-time-complexity-of-searching-in-a-binary-search-tree-if-the-tree-is)

*9. What is the difference between depth-first and breadth-first searches for binary tree?*

<img src='https://steemitimages.com/DQmRtHYU5s8z6Q4RqFTWLWiu99QkqxDQ3ujT6BTT99GF5BS/BreadthDepth_Upgrade.jpg'>

Breadth first and depth first traversal are two important methodologies to understand when working with trees.
Breadth-first search uses the opposite strategy as depth-first search, which instead explores the highest-depth nodes first before being forced to backtrack and expand shallower nodes. 
[Youtube answer ->](https://www.youtube.com/watch?v=zaBhtODEL0w)

[Source + more ->](https://medium.com/employbl/depth-first-and-breadth-first-search-on-trees-in-javascript-58dcd6ff8de1)
