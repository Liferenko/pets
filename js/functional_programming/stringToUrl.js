// usual style (impetrative)

var string = "Here is the regular sentence";
var urlFriendly = "";

for (var i = 0; i < string.length; i++) {
    if (string[i] === " ") {
        urlFriendly += "-"
    } else {
        urlFriendly += string[i];
    };
};

console.log( "--- OOP ---" );
console.log( urlFriendly );
console.log( "---     ---" );





// functional style

const fpString = "Here is the functional style sentence";

const fpUrlFriendly = fpString.replace(/ /g, "-");

console.log( "--- FP ---" );
console.log( fpUrlFriendly );
console.log( "---     ---" );
