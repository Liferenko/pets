# Pavel Liferenko

- Date of birth:  7/8/1992
- Tbilisi, Georgia (ready to relocate) 
- Phone/telegram +995 568 718093
- <a href="mailto:liferenko.it@gmail.com">liferenko.it@gmail.com</a>
- [Gitlab](gitlab.com/Liferenko), [Github](github.com/Liferenko)

## Experience

#### Backend Elixir developer, 
###### Project #1:
Stack: gRPC, Protobuf 


#### Backend Elixir developer, RIA.com — Jan. 2021 - present
###### Project #1:

**Problems I worked with and solved:**
I'm building a backend to connect Ukrainian insurance partners with our Android/iOS apps. 
For our clients the process looks like they are chatting with a chatbot inside an app.
The backend is sending message structures as Server-driven UI components (it means everything user may interact with was compiled on backend)
- Connect with a billing service by SOAP.
- Connect with partners' API by REST.

Stack: Elixir, Phoenix, Ecto + MySQL


###### Project #2
An admin service which manages all channels like the insurance service I described above. 
**Problems I worked with and solved:**
-  I'm maintaining this service with two colleagues. Tech stack is pretty the same: Elixir, Phoenix, MySQL, rabbitmq, docker-compose, and from time to time there were LiveView frontend tasks.
- <span style="color: red;">????</span> API на стороні ProjectName для видалення каналу із списку в юзера ??????????????? Maintain a service <span style="color: red;">????</span> 
- Update documentation
- Unit tests
- Cassandra's nodes load testing??????????
- <span style="color: red;">????</span>  Add monitoring metrics and health-check pipelines <span style="color: red;">????</span> 
- This was the first time I tuned in Supervisor to restart stopped???? nodes 














#### Full-stack Elixir developer, Weamarry.com — June 2020 - Jan. 2021
Tasks: maintaining and developing an umbrella project,
which contains client application, two partners-only content management system applications and a mail sending app.
BE is implemented in Elixir with heavy use of Ecto, FE relies heavily on Phoenix LiveView with a touch of vanilla JS. 
Problem I solved: 











#### Independent contractor, 2 clients, Georgia — May 2021- Dec.2021
Payment gateway, AWS Lambda, AWS CloudFormation, payment processing, local bank, Tilda, </em>
**Problems I’ve solved**
- TBC bank with no ready-to-use payment processing + Tilda-based eCommerce shop + snail-running support from both side</em>
- making an AWS CDK project to deploy configured Lambdas (written on Python 3) for connecting Tilda-based web shops with local banks’ payment gateway (the banks didn't have a build-in plugins for Tilda)















#### Full-stack Python developer, REG.ru, Kiev, Ukraine — Jan. 2018 - May 2020

Tasks:
- development of microservices for working and maintaining code inside the company's workspace,
- recasting of Perl-modules in Python and their subsequent replacement,
- code support, documentation writing.


###### Project #1
<span style="color: red;">????</span>Stack: Python+Django, FE: VueJS + Vuex, Infrastructure: Docker, Gitlab CI
###### Project #2
<span style="color: red;">????</span>Stack: Tornado, AsyncIO, Docker(-compose), asynctest). 





















#### Independent contractor, SKILLS events, Kiev, Ukraine — Mar. 2016- Sep. 2017
Main goal - a steady flow of customers for events and products of the company: coaching, yachting, 2 championships of Ukraine.
To do this, we did the following:
- <span style="color: red;">????</span>  web development of skills.events pages,
- setting up and analyzing the advertising campaigns Facebook / Instagram,
- email-funnel for heating a new clients database and working with existing customers base,
- introducing CRM Pipedrive into the company’s workflow.


#### Independent contractor, Goltisacademy.com, Kiev, Ukraine — Jun. 2016- Feb. 2017
The task was one - to bring customers (old and new) to the company's products: all-in-one health system trainings and programs.
To do this, we've done:
- development and testing of product and event pages,
- setting up and analyzing advertising campaigns to attract targeted traffic,
- email funnel sales and a sequence of letters to work with the database (~ 13,500 subscribers),
- work with the GetCourse CRM system,
- the implementation of the workflow management system with Gantt chart,
- the selection and conducting of work with freelancers (SMM, traffic, design)


#### Independent Contractor, chuykova-agency.com.ua / gate.agency/ pjimage.in.ua / poehali.info / elektromobil.tk / ivchik.com / White Sales School / leo-dent.com.ua - 2014-2018

- Work dedicated to solving technical issues: development and testing of online resources of the project;
- setting up and analysis of advertising campaigns to attract targeted traffic;
- building e-mail and architecture to work with the subscriber base;

#### Technical Support Specialist / Frontend developer, Genius Marketing Ukraine, Kiev, Ukraine - 2013-2014
Client support in every timezones, landing pages developing :)

<hr>

# ONLINE SKILLS
- Elixir/OTP + Phoenix + PostgreSQL (in production)
- Elixir/OTP + Absinthe + GraphQL (in production)
- Python 3 + Django + VueJS + MySQL (in production);
- Go + Gin + ReactJS (for pet-project);
- Erlang/OTP (for pet-project); 
- React + FastAPI (for pet project)
- AWS (Lambda, S3, EC2), Azure (VMs)
- Bash
- Kibana

<hr>

# OFFLINE SKILLS
- Wide work experience as a part of distributed Scrum team;
- Experience in developing online and offline marketing projects;
- Significant experience in "cold" sales: (non-food sector - presentation equipment, vehicles (Nissan Leaf, Tesla model S); FMCG - coffee products;

###### Languages: 
- English, Advanced,
- Ukrainian and Russian, Native
- German, Basic
- Georgian, Elementary

# EDUCATION</strong>
1. National Aviation University, Kiev
— Bachelor's degree of logistics and supply chain management, 2013

<span style="color: red;">????</span> Your Marks/Grades (AND include the scale or the system of grades so its understandable, e.g. 4.4 out of 5 (where 5 = excellent, or in %, you can convert it yourself, or say pass/fail/completed/with honours etc) \

2. AWS Cloud Practitioner Essentials Day, 2021


# REFERENCES</h1>
<pre class="prettyprint">
Andrey Zaika
Co-owner and CTO of Weamarry
T: +7 985 318 10 04  
E: anzaika@gmail.com

Anisa Safi
Director of travel agency "Poehali s nami"
T: +380 67 830 65 38  
E: safi@poehalisnami.com

Vadim Martsenko
Founder of SKILLS.events and Martsenko Sales School, 
T: +38 067 549 5451 
E: vadim@martsenko.com

Pavel Denisov
Founder of 10millionov.com and elektromobil.tk 
T: +38 067 500 1876
S: elektromobil.tk
</pre>
</table>


