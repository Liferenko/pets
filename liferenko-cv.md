---
layout: cv
title: Pavel Liferenko's CV
---
# Pavel Liferenko
Software Engineer, Solution Architech

<div id="webaddress">
<a href="eleven.krsk@gmail.com">eleven.krsk@gmail.com</a>
| <a href="https://gitlab.com/Liferenko">Gitlab</a>
</div>


## Currently

Elixir developer, RIA.com

### Specialized in

Backend, DB, Serven-drive UI


### Hard Skills
Elixir + Phoenix + PostgreSQL
(in production)
- Python 3.* + Django + VueJS + MySQL (in production);
- Go + Gin + ReactJS (for pet-project);
- Erlang/OTP (for pet-project);
- React + FastAPI (for pet project)
- Bash,
- Kibana,
- AWS (S3, EC2, Lambdas)


## Education

`2021-2022`
__PariMatch Tech Academy, Online.__

`2017-2018`
__BetInvest Bootcamp, Kyiv__

`2009-2013`
__National Aviation University, Kyiv__



## Occupation

`2019-...`
__Tbilisi__, Georgia

`2009-2019`
__Kyiv__, Ukraine

`1992-2009`
__Krasnoyarsk__, Russia


<!-- ### Footer

Last updated: December 2021 -->


