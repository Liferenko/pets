#Whole changelog of my path as a software engineer.

### Day 440: August 23, 2021
##### (project: post-COVID rehabilitation)

**Today's Progress**: 
        1) I visit a doc. She said all good, I'm a healthy unit :) Siberian health inside
        2) I'm still experiencing brutal boredom. Can't find a source of interest, a source of endorphins. That IS definitely something new for me, for my body

**Thoughts:**  
        1) I'm proud for my body. It really did HUGE work with Covid + with pneumonia with ZERO antibiotics and any other outcomemmers. Fasting + own immune system. I really appreciate it. And I think one part of it lays in no-alco\no-drugs lifestyle
        2) About boredom. I still think it relates to a lack of vitamins or minerals. I lost 10 kg by 12 days so my body doesn't understand how to handle it 

**Stack:** 
        1) Nothing
        2) Nothing

**Link to work:** NDA


---

### Day 37: June 9, 2020
##### (project: NDA)

**Today's Progress**: 
        1) practiced if-statement <%= if @something, do: that %>
        2) practiced a search in LiveView

**Thoughts:**  
        1) Sometimes I will need debugging tool in browser console. To do that I need to add `liveSocket.enableDebug()` in apps/pal/assets/js/app.js
        2) It turns out I can forget about Backspace, because in Vim I can use Ctrl+h. Damn it feels goooooooooooooooooooooooooooooood!

**Stack:** 
        1) Elixir
        2) Phoenix

**Link to work:** NDA

---

### Day 36: June 8, 2020
##### (project: NDA)

**Today's Progress**: 
        1) Worked with Elixir's maps %{}

**Thoughts:**  

**Stack:** 
        1) Elixir
        2) Phoenix

**Link to work:** NDA

---

### Day 35: June 7, 2020
##### (project: NDA)

**Today's Progress**: 
        1) Phoenix videocourse
        2) Programming Phoenix book
        3) Fixing vimrc with disabled $mod+d

**Thoughts:**  
        1) Pragmatic Programmers have well-done video materials
        2) "user events handle by handle_event/3; internal messages handle by handle_info/2"(c)PragmaticsStudio Phoenix, Example #3: Dashboard, 6:45

**Stack:** 
        1) Elixir
        2) Phoenix

**Link to work:** NDA

---

### Day 34: June 6, 2020
##### (project: NDA)

**Today's Progress**: 
        1) +1 task solved
        2) Modal popup fixing

**Thoughts:**  
        1) I got how live_component works and why there were issues with modals
        2) Ctrl+Shift+O and Ctrl+Shift+I are the gamechangers!

**Stack:** 
        1) Elixir
        2) Phoenix

**Link to work:** NDA

---

### Day 33: June 5, 2020
##### (project: NDA)

**Today's Progress**: 
        1) A book "Programming Elixir". A chapter 23 - Linking Modules: Behavio(u)rs and use - p.322
        2) I found out how to use a Erlang debugger with Elixir
        2.1) iex -S mix >>> :debugger.start() >>> :int.ni(HERE_IS_THE_NAME_OF_MODULE_AS_IT_CALLED_IN_defmodule)

**Thoughts:**  
        1) There are no needs to call handle_event/3 INSIDE live_component
        2) Nice explenation of liveView, LiveComponent and events inside them - http://blog.pthompson.org/phoenix-liveview-livecomponent-modal
        3) Debugger.

**Stack:** 
        1) Elixir
        2) Phoenix
        3) Ecto

**Link to work:** NDA

---

### Day 32: June 4, 2020
##### (project: NDA)

**Today's Progress**: 
**Thoughts:** 
        1) I think I can stop on Programming Ecto - Chap2, because I've already stepped out of my needs of knowledges in Ecto at that moment. I will continue the book, but now I need to get my feet wet.
        2) На Ecto.Query в книжке я пока остановился, так как вижу по коду из apps/al что уже появилось понимание что там происходит. Дальше книгу про Ecto ставлю на паузу, чтоб набить руку на уже усвоенном по части работы с БД. 
        3) Покурил Liveview (доку на hex и видос с конфы). Тоже нужно руками опробовать. Есть вопрос: судя по грепу, шаблоны у нас формата *.eex. А в LiveView часто упоминают *.leex. В коде работу с LiveView я вижу, но шаблонов *.leex нет. Таких файлов у нас нет, потому что не написали\не перенесли ещё из-за свежести LiveView или есть другая причина почему ими не орудуем? Это мне так, для понимания что да как.
        4) Разбирался с handle_event. Микрошажками двигаюсь в сторону понимания связки компонентов, ивентов, состояний. Вижу аналогии с Реактом и Вью, но к результату пока не пришёл. 
        В консоли при запущенном `mix phx.server` и при нажатии на открытие сессии или на иконку "удалить" падает ошибка, которая почему-то пытается ловить мой ивент в файле выше и на других handle_event'ах. Уверен, что корень тут лежит в понимании. Нужно просто чуть больше раскурить механику работы. Завтра этим и буду заниматся.
        Выхлоп консоли вот (на посмотреть) - https://pastebin.com/95Kzsifj

        Жесткого тупняка и блока у меня сейчас нет, поэтому и не дёргаю тебя вопросами. Пока мне самому нравится как идёт процесс вливания. 

**Stack:** 
        1) Elixir
        2) Phoenix
        3) Ecto

**Link to work:** NDA

---

==============================


## 6 weeks of Erlang - Log
### Day 31: June 3, 2020
##### (projects: Erlang course)


**Today's Progress**:  

**Thoughts:** :  
        1) We can use IO.inspect as a debugger thing (aka print() in Py or change.log() in fucking JS)
        
**TODO** :
        [ ] To finish a last third of Erlang Guidelines [bookmark](https://github.com/inaka/erlang_guidelines#write-function-specs)


**Stack:** : 
        2) Ecto
        3) Elixir

**Link to work:**
        1) [Ecto sandbox](https://gitlab.com/Liferenko/ecto-sandbox)









### Day 30: June 2, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Learing stopped (want to dive in Elixir)

**Thoughts:** :  
        1) I prefer to dive in Ecto+Elixir with real-world service. It means I have no chance to continue that Erlang class
        
**TODO** :
        [ ] To finish a last third of Erlang Guidelines [bookmark](https://github.com/inaka/erlang_guidelines#write-function-specs)


**Stack:** : 
        2) Ecto
        3) Elixir

**Link to work:**
        1) [Ecto sandbox](https://gitlab.com/Liferenko/ecto-sandbox)









### Day 29: June 1, 2020
##### (projects: Erlang course)


**Today's Progress**:  

**Thoughts:** :  
        1) I was dancing with Elixir+PostgreSQL environment
        
**TODO** :
        [ ] To finish a last third of Erlang Guidelines [bookmark](https://github.com/inaka/erlang_guidelines#write-function-specs)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 28: May 31, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) File indexing. Tests. Still shit, but whatever...

**Thoughts:** :  
        
**TODO** :
        [ ] To finish a last third of Erlang Guidelines [bookmark](https://github.com/inaka/erlang_guidelines#write-function-specs)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 27: May 30, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Refactoring a code according to Erlang Guidelines

**Thoughts:** :  
        1) If I want to use _Variable - why don't use just _ instead, hah?:)
        
**TODO** :
        [ ] To finish a last third of Erlang Guidelines [bookmark](https://github.com/inaka/erlang_guidelines#write-function-specs)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 26: May 29, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Elixir + Phoenix + new schema in PostgreSQL. Unstable yet, but... But something :)

**Thoughts:** :  
        1) I am still so surprised about commands like "mix phx.gen.html" and so on :) Looks like uncontrollable magic and it scares a bit :)
        
**TODO** :
        [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 25: May 28, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Dances with Phoenix app

**Thoughts:** :  
        1) I definitely have no energy for Erlang course. :/ Shit :/
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang
        2) Elixir + Phoenix

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 24: May 27, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Sorry, Erlang. But there is NASA launch :)

**Thoughts:** :  
        1) Sorry, Erlang. But there is NASA launch :)
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 23: May 26, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Refactoring take/2

**Thoughts:** :  
        2) Instead of using a variable Result I may use an idea like that one - [H|take(Number - 1, T))]
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 22: May 25, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) "take" function
        2) Debugging

**Thoughts:** :  
        1) I see I started to use Zeal more often (and that fact is really nice new habit)
        2) It turns out Erlang debugger might be finally my buddy :D thanks that dude (https://vimeo.com/32724400). I saw a blind side of my working function. And it would has been imposible to recognise without debugger 
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 21: May 24, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Rest day
        2) Worked with dirs and paths in Erlang. Unfinished

**Thoughts:** :  
        1) Need to exhale a bit
        2) Looks like ct_run has different (an unobvious) paths to the files
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 20: May 23, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Tests for direct/tail recursions
        2) Simple playin-around with week2 recursion

**Thoughts:** :  
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 19: May 22, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Init a property-based pet project
        2) replace tests from CommonTest to PropEr

**Thoughts:** :  
        1) I refreshed the old knowledges about Property-based testing and am trying to use it with that course
        2) Rebar3. Again troubles with exporting him in $PATH. I hope now it works as expected (will see after reboot)
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 18: May 21, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Refactored fib/3 function. There is no need to test 3-argumented func, because it is already used in single-argumented func.
        2) Perimeter for circle and square

**Thoughts:** :  
            1) About exporting one-argumented function when there is another one with 3 arguments inside: we can export func/1 if we use func/3 only inside and as a support func for func/1. Sounds simple and logic. Now I know.
            2) Im not sure if Im doing okay with pattern matching and recursion. I feel like Im just doing cases instead of recursion.
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 17: May 20, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) green fib/3 tests 
        2) Refactoring the 2 fibonacci functions into one
        3) TODO: head mismatch. To solve 

**Thoughts:** :  
        1) Im not sure I got an idea of fibonacci3/3 (especially the part with third argument).
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)
    [x] Type check and declaring


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 16: May 19, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Tail recursion (Week2)
        2) Type check (with spec and with "when N>0")

**Thoughts:** :  
        1) Looks like I can use exception as an type checker. It is more like "if statement", but at least it works :)
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)
    [x] Type check and declaring


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 15: May 18, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Week3. Init tests and stupid logic
        2) Week2. Fibonacci + tests
        3) Paper folding calculation

**Thoughts:** :  
        1) I'm about to start losing a motivation
        2) I feel unfamiliar with type declaring in Erlang. My tests are still failing when there are floats and negative integers.
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)
    [ ] Type check and declaring


**Stack:** : 
        1) Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 14: May 17, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) updated factorial method with TDD (week 2)

**Thoughts:** :  
        1) I need to solve a type spec issue. There is still an open wound with negative integers in factorial.
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang
        2) Common Test
        3) Zeal

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 13: May 16, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) TDD for recursion block (week 2)

**Thoughts:** :  
        1) I forgot TDD workflow. Again. I wrote a method and only after that I realized I forgot tests))
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 12: May 15, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) TDD in Erlang - counting a BMI. Green tests
        2) TDD in Elixir - counting a BMI. Green tests

**Thoughts:** :  
        1) It turns out var names are folowwing not same logic: Ex - all vars should start with lower char; Erl - with capital char
        2) One more baby step in TDD and FP
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell
        2) Common Test Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 11: May 14, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Really baby step in TDD with Common Tests

**Thoughts:** :  
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell
        2) Common Test Erlang

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 10: May 13, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) finished week 1 and quize
        2) did simple unit test without framework
        3) refactired unit tests with Common Test 

**Thoughts:** :  
        1) TDD in Erlang. Finally! Yet another baby step to TDD maniac year goal :) [Sauce](http://erlang.org/doc/apps/common_test/run_test_chapter.html) 
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 9: May 12, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) practicing in pattern-matching. Function maxThree
        2) maxThree refactoring
        3) simplo-stupid unit testing :)

**Thoughts:** :  
        1) "Equal sign is like pattern matching sign."(c)
        2) Need to find how in tests start 3+ lines of assertion (what sign should I use: comma and semicolon)
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 8: May 11, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) some warmups with `erlang:binary_to_.../2`;
        2) was read much about lists:___().

**Thoughts:** :  
        1) NB! "...atoms are not garbage collected. So, from a security reason, we need to be careful not to convert user input into atoms. This is a typical concern in web programming where the user input can never be trusted"(c) Antonio Cangiano, from Erlang course  
        2) It was something not so obvious for me: I thought when you compiled parent module and his children are uncompiled yet - the children will be compiled as well. But no.
        You need to compile them before. That is logical and now I know for sure :)
        3) Atoms are cool as "tags" or "flags". "... no matter what algorithm is used for pattern-matching, matching strings/binaries can not be faster than matching atoms"
        4) About atoms again: A common Erlang idiom: use the 1st field to indicate what sort of data in the tuple
        5) Tuples == fixed size; Lists == variable size
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell
        2) Zeal doc app

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 7: May 10, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) Read an Erlang guideline (as an alternative to Python's PEP8)
        2) 

**Thoughts:** :  
        1) I like the way Erlang is working with case syntax. It looks like breaking DRY rule, but after a second look you're starting to see why that way is quite good. [There is an example](https://github.com/inaka/erlang_guidelines/blob/master/src/smaller_functions.erl). I hope I will not forgot that :)
        2) Whoa, I didn't know I already have soooo many bad cases regarding to ErlGuite :)
        3) 
        
**TODO** :
    [ ] It's better to continue reading from that [bookmark](https://github.com/inaka/erlang_guidelines#dont-use-_ignored-variables)


**Stack:** : 
        1) Erlang/OTP 22 shell

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)









### Day 6: May 9, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        1) refactoring of liferenko.erl (rewrite a logic of a printing usage)
        2) unsuccessful tries of pattern matching using. I tried to separate caltucalte's "ok"-status from answer.

        3) init a client's app Elixir\Phoenix inside docker container
        4) config DB (unfinished. I stopped on 10:00:15.954 [error] GenServer #PID<0.238.0> terminating)


**Thoughts:** :  
        1) Good Saturday. Needless to say, it was perfect learning day :)
        2) I think I finally found why and where to use a book "Property Driven Development in Erlang/Elixir". It's time to restart reading :)


**Stack:** : 
        1) Erlang/OTP 22 shell
        2) Docker
        3) PostgreSQL

**Link to work:**
        1) [Sandbox](https://gitlab.com/Liferenko/pets/-/tree/master/erl/kent_university_course)








### Day 5: May 8, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - add hypotenuse calcilation with clean+show

**Thoughts:** :  
        1) Looks like I've got wrong answer. Need to double check :))


**Stack:** : 
        - Erlang/OTP 22 shell

**Link to work:**









### Day 4: May 7, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - io:format()
        - utf/8 and Cyrillic letters
        - printing and debug tracing 

**Thoughts:** :  
        1) I did refresh in my mind an old and archived knowledges about printing in Erlang
        2) Finally moved forward some debugging skill in FP. I remember 2 years ago I unfortunately skipped that topic. My bad. 
        3) I did one more baby step in Zeal DocApp. One more step shows how powerful that thing is. 
        4) TODO: Paul, remember about your TDD hopes and fears :)


**Stack:** : 
        - Erlang/OTP 22 shell
        - Zeal application

**Link to work:**









### Day 3: May 6, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - wrote some simple erlang modules (multiply, double)
        - wrote a clear command
        - read a block about memory size of different data types (founded in Zeal->Erlang->Guides->Advanced->10.1 Memory)

**Thoughts:** :  
        - finally found out how to clear an Erlang shell. It was nasty method. Guess what? I did it more nasty :)
        Finally there is some code written by myself :D
        - in short future I need to slowread a part "Application" from Zeal->Erlang->Guides->Application
        - One erlang process's size is 338 words. It's a lot of memory. I think I will need it in real highload project.
        - it would be nice to read and to replicate code form a guide "Concurrent Programming" in Zeal->Erlang->Concurrent Programming. Why? Because there is ready-to-test code

**Stack:** : 
        - Erlang/OTP 22 shell

**Link to work:**









### Day 2: May 5, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - two videos done
        - some plays with toys inside erl console. Just warming up :)
        - I asked a question I didn't answered 2 years ago. The question is about clear the erl console. I mean it's not a big deal to get an answer. I did it more for deep connection with that course.

**Thoughts:** :  
        - I was excited about super-simple thing: help(). I knew about inline help in evety lib and langs, but in Erlang console I feel it helpful as hell :) This simple little command e(N) made me feel like a child who got something new in his mind)
        - In console 'erl' we may use Ctrl+R as we're using in shell

**Stack:** : 

**Link to work:**









### Day 1: May 4, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - Three videos from Week 1 are done.  
        - I did some configurations for multi-monitors in i3wm. I hope I'm back to i3 and will use it during that course.

**Thoughts:** :  
1) Hello there! I'm Paul. I started to date with BEAM in Jan 2018. It was something special for my OOP mindset. It was like they gave you steering wheel from spaceship while you had an experience to drive BWM all the time :) As you can see I can't enjoy driving BMW anymore))
2) It turns out there is a good and meaty conversations in comments block. I guess it would be useful to read it each time

**Stack:** : 
        - Vim 8 

**Link to work:**









### Preparation Day -1: May 3, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - one day before course's starting. I guess I'm ready. Teammates are ready as well

**Thoughts:** :  Nice! Erlang/OTP 22 is installed, Alles gut.

**Stack:** : 
        - Vim 8 

**Link to work:**









### Preparation Day -8: April 26, 2020
##### (projects: Erlang course)


**Today's Progress**:  
        - there is a team from r/erlang, which ready to do steps in that course together

**Thoughts:** :  there are 5 tips and tools for social learning. I think I did it before I found that official tips)) Nice! 

**Stack:** : 
        - Vim 8 

**Link to work:**
        - [5 tips and tools for social learning](https://www.futurelearn.com/info/blog/5-tips-tools-social-learning)


===================================================================







### Day 64: December 18, 2019
##### (projects: Null)


**Today's Progress**:
        - I tried to dive in Perl. It looks surprisely good
        - Vimrc. FINALLY Esc to Capslock    


**Link to work:**








### Day 63: June 22, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Smalltalks
        - Work day. Ordinar work day.  
        - Vimrc. FINALLY Esc to Capslock    


**Link to work:**




















### Day 62: June 21, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Smalltalks
        - Work day. Ordinar work day.  
**Link to work:**












### Day 61: June 20, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Smalltalks + meeting with arab guy + chatting + welcome speech for Berlin hosts :)
        - Work day. 2 corporate seminars. It was kinda learn day.  
        - Food. It was fat-majority food day. I surprized that I need ~200 g of cheese for beeing as full as after full-size dinner. 
**Link to work:**



















### Day 60: June 19, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Smalltalks with chef :)
        - Default work day. 
        - Morning. I woke up at 4:##. It was raining outside and I made my mood for this morning. 
**Link to work:**













### Day 59: June 18, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Chatting + smalltalks with chef :)
        - Cooking. Gazpacho
        - Yoga in park. It was nice, but I'm again try to hate all I do :/ Can't keep concentration inside :/
        - Ziferblat. We watched The Movie: Monster Inc. Oh, what a movie! Love it!

**Link to work:**














### Day 58: June 16, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. A whole day was in English: small talks, shop, board games, dinner :)
        - It was an  I-am-totally-melted day. Headache and lazyness. I guess it was all about the weather :/ 
        - Cooking. I cooked a kompot without sugar. Sour, aserb, acidic, tart. But it was done in 4 hours :)

**Link to work:**















### Day 57: June 15, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Chef + Estonian citizens + Syrian citizens :/
        - Lazy day :/ 
        - Cooking. I cooked a kompot (boiled apple juice)

**Link to work:**













### Day 56: June 14, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. No english at all :/
        - Yoga. It was first time in park. Oh, my! It's awesome! Dogs protect us, wind keeps us well-cooled. I guess this type of yoga for me :)  
        - Workday. A lot of confcalls


**Link to work:**











### Day 55: June 13, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Smalltalks 
        - Workday. A lot of confcalls


**Link to work:**









### Day 54: June 12, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Domino's + cooking with chef 
        - I accidantly found that my phone has a NFC for payments. Nice one! So I configured few cards for paying with phone. Oh, yeah 


**Link to work:**







### Day 53: June 10, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. A bit.
        - Music class. I detected a spectrum hole, whick need to be filled by instruments. 


**Link to work:**









### Day 52: June 9, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Uno + cafe + bowling + English club + market barring.
        - New phone. iPhone SE. Black. As I order) 


**Link to work:**







### Day 51: June 7, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Uno + cafe 


**Link to work:**










### Day 50: June 6, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. L.P.M bar + Friday night in style "Me and the boys...". It was nice bar-UNO-only-English night :)
        - Music. I spend 2 hours in a studio completely alone. I worked under 2 tracks: 78x78 and old beam. After all this time I still didn't satisfied of this tracks. It still sounds plastic :/

**Link to work:**






### Day 49: June 5, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Chatting + smalltalking. And music class.
        - Music. Music class. Homework: to make structure of 78x78 goes to "less is more". DnB homework: to add more instruments.

**Link to work:**









### Day 48: June 5, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. Music school and smalltalks with hostel owner
        - Music. DJ practice + piano practice + DnB structure upgrading.
        - Morning tennis. I'm late 'cause I went to wrong place. But even with this lating it was nice. I surprised that my body remember some basic moves and poses

**Link to work:**















### Day 47: June 4, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Eng. NaN
        - Music. Hm, morning? 
        - Yoga. Yes. What's with me wrests?

**Link to work:**















### Day 46: June 3, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. Music + food
        - Music. Finally it's time for track structure. I worked with "old beam" and "78x78"
        - Food. 1st meal was at 16:00. Strange that I wasn't starving till this time. And I guess it was OMAD day
**Link to work:**

















### Day 45: June 2, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. Brunch + bowling + Eng.Speaking club + Uno. Nice Eng. day
        - Music. Update a bit of 78x78
**Link to work:**














### Day 44: June 1, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. Almost nothing. Only Subnautica (game)
        - Newcommers. Nike 6.0. welcome to team! 
        - Music. I created a nice dnb Audio-like sound
**Link to work:**














### Day 43: May 31, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. Shops.
        - Yoga. +1 lesson. I found out that my left ankle and instep are less flexible then the light ones. I guess my right foot is more flexible because of that sprained ankle.  
        - Music. I created a nice dnb Audio-like sound
**Link to work:**















### Day 42: May 30, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. Music class.
        - Music. I created a nice guitar solo for one new idea.
**Link to work:**















### Day 41: May 28, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. A few sentences for whole day.
        - Yoga. Nice and energy-feeded lesson. It made me empty for whole day (adain this phrase, yeah)
        - Work day. Main part of a day.

**Link to work:**












### Day 40: May 27, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. Music class 
        - No by default. Friend of mine requested me a short-term participation, which looks profitable and risky at the same time. 
        Yeah, little profit is nice but all another (non-money) sides of this situation look risky for far perspective for relationships. So answer was "no". And this answer respawn in my mind as a first flicker. 
**Link to work:**









### Day 39: May 26, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. Kitchen #1 + English speaking club in Z + Kitchen #2 and table chatting :D
        - It was a gaming day. Im satisfied)
        - Thoughts become real. My persian-pants-"order" was complete. Oh, it's nice)

**Link to work:**













### Day 38: May 25, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. Only in gaming. 
        - It was a gaming day. Im satisfied) 

**Link to work:**












### Day 37: May 23, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Music. DJ practice + piano practice + music prod lesson
        - I bought a ticket from Tallin to Berlin. 
        - English. Hostel + music lesson + gamezone

**Link to work:**



















### Day 36: May 22, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Thoughts are phisical. I was order new shoes 2 days ago and recieve them today. No money. Old ones were out, new ones - in. *click* Noice!
        - Music. Preparation to tomorrow's dj practice. A lot of works with hot cues, harmony mixing etc. 
        - Work. It was valueable wokr hours for me. I satisfied a bit ;)

**Link to work:**










### Day 35: May 21, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. Couchsurfing meeting. Talking a lot (with London guy and with 2 persons from US)
        - Music 2. Few minutes of piano practice in Ziferblat at the morning. I guess my left hand's speed goes up (or it can be better to say "speed increases")



**Link to work:**
















### Day 34: May 20, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. Many English in music class, in cafe, on a street. I'm satisfied. 
        - Music. Sick lesson. It was all about bass and kick. My classmates made an incredible techno sound. Industrial sound. I like that. Me? My Berliner was good too, but I thought I made bass, but it was 2 versions of subbass. Need practice)
        - Music 2. I added to calendar 1.5 hours of DJ practice this week AND 1 hour of piano practice 



**Link to work:**















### Day 33: May 19, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        English. Day-off. It was valueable



**Link to work:**














### Day 32: May 18, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. New hostel (Eng owners), board games (Eng friends), new roommates (German and Polish dudes, Eng and Deutsch speaking) :D
        - Headphones searching. No results. I see it's not so easy to find any good dj and musician stuff here. Damn it!
        - new experience. I found a place, where I can play X-box's Forza Horizon with full contact stearing wheel and pedals. 5 laris per hour. Amazing and day-off chilling)
        - Food. 2 kilos of strawberry. Damn, it was hard to finish) But I did it. 
        - Music. I took 25 loops of drum break. Nice and useful 

**Link to work:**













### Day 31: May 17, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Music class. Wrong day and wrong time. Good news: I found dj place for practicing and piano practice
        - Kitchen. It was a day with chef Aaron Kaya. He made veggie dishes and I made драники
        - English. L.P.M bar, Australian and Urugvai guys, backgammons and small talks :D


**Link to work:**












### Day 30: May 16, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Music class. Day-off. But I worked with Serum synth and his plugins. Now it works ok.


**Link to work:**












### Day 29: May 15, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Work  day. Check-in for quartal. Done
        - English practice. Chatting with Japan and NYC :) 
        - Make space between sound. 
        - Lesson 1 is done. Preparation to lesson 2 - done. HaCo and tech house.
        - Cake day. Just a cake :) With no reason)
**Link to work:**











### Day 28: May 14, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Sick day. After yoga it was killing zone with no energy. 
        - English practice. Couchsurfing meeting in L.P.M bar
        - Music. There was a nice midnight action, in which I decoded a song Flute - River of glass to structure.


**Link to work:**









### Day 27: May 13, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - payday. I paid for music production course.



**Link to work:**










### Day 26: May 12, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - English. English club and kitchen-English :)
        - Greencard. Still can't find my conf.number :/ 
        - Brunch. This morning was spended in a kitchen and I like it :) 



**Link to work:**








### Day 25: May 11, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - New journey. I'll come to Narva on (as usual) strange path :) Thanks God, my b-day will in unknown country.
        - Payday. I paid for a hostel for next 9 days. 
        - English. Again. Too much English and I'm excited :D 




**Link to work:**






### Day 24: May 10, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Payday. I paid for yoga class, for study in Estonia, for... for food?)
        - Ableton + Plug-ins. I made it works together. As a bonus: Splice's working perfect for Ableton.
        - Greencard. It still gives me no info about that. And no answers too. Ok, I'm not on a rush) 

**Link to work:**





### Day 23: May 9, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - i3 window manager. Now it works and works more friendly for me) 
        I guess I'm starting to feel satisfaction and comfy from i3. I'm on the way)
        - Narva college. I pass a test exam with 19 of 25 correct answers. Looks nice and I feel I can continue "Move to Estonian college" journey
        - Music production course. I'm signed up. We start next wednesday. I've got a bunch of plugins I should find and install before course start. 
        - End of day. B-day party of Tbiliisi Ziferblat's co-founder. It was full of English, full of tasty dishes, full of "Think Big" people. I feel it was like in a movie "Midnight in Paris", when the main character,- Gil, encounters a group of strange -- yet familiar -- revelers, who sweep him along, apparently back in time, for a night with some of the Jazz Age's icons of art and literature.
        It might be almost the same situation in far future :)


**Link to work:**




### Day 22: May 8, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - i3 window manager. Some stupid practice with it)
        

**Thoughts:** :  
    - nothing with the project. 

**Stack:** : 

**Link to work:**



### Day 21: May 7, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Yoga class. Oh, fuck. It was hard;
        - Workhours. Damn it! I spend almost 3 days because of I didn't know what exactly doing "position: sticky; right: 0;". My bad :/


        

**Thoughts:** :  
    - nothing with the project. 

**Stack:** : 

**Link to work:**






### Day 20: May 6, 2019
##### (projects: poehali_info API)


**Today's Progress**:
        - Georgian lesson (I boost my Eng while learn Georgian)
        - signed to yoga class
        - Still. No. Steps. In project :/


        

**Thoughts:** :  
    - nothing with the project. 

**Stack:** : 

**Link to work:**








### Day 19: May 5, 2019
##### (projects: poehali_info API)


**Today's Progress**:
    - many-many English practice. English speaking club + talks about dogs (of course, damn)) 
    Boardgame, movie in English. 
    - Plus Andrei and I were talking about Tbilisi dev meetup. It seems like I'll take some speech on it. 
    I have to prepare speech about Vim :) Oke, oke



**Thoughts:** :  
    - nothing with the project. 

**Stack:** : 

**Link to work:**







### Day 18: May 4, 2019
##### (projects: poehali_info API)


**Today's Progress**:
    - many-many English practice.
    Boardgame "Snake oil", talking with NY chef, talking with Syrian mate, talking with sound producer about his course



**Thoughts:** :  
    - nothing with the project. 

**Stack:** : 

**Link to work:**






### Day 17: May 3, 2019
##### (projects: poehali_info API)


**Today's Progress**:
    - nothing. There was just the one thing: English practise.
        Oh, and one more thing: Danish and Austrian Universities (Computer Science). They said I need A-level marks in Math and at least B-level Physics. Damn it! :/ 

    - aaaaand... another one thing. New card: National Parliamentary Library of Georgia. Now I have an access to this place.
    *click* Noice! :)

**Thoughts:** :  
    - nothing with the project. 

**Stack:** : 

**Link to work:**






### Day 16: May 2, 2019
##### (projects: poehali_info API)


**Today's Progress**:  

**Thoughts:** :  
    - nothing with the project. Looong loong pause with relocation 

**Stack:** : 

**Link to work:**






### Day 15: March 30, 2019
##### (projects: poehali_info API)


**Today's Progress**:  

**Thoughts:** :  
    - transport day only. Nothing code activity


**Stack:** : 

**Link to work:**




### Day 14: March 29, 2019
##### (projects: poehali_info API)


**Today's Progress**:  

**Thoughts:** :  Conf day numero 3. 
        - I'm on s REG.Summit. And there is a lot of talks about tests, about QA and almost each speech contains "Quality" context. I guess it's time to say NO to testless development. It's time to stop it)
        Here is links to work with:
        1) https://hub.docker.com/r/pandelegeorge/python-tdd/
        2) ($40, but with preview) - https://testdriven.io/courses/microservices-with-docker-flask-and-react/

**Stack:** : 

**Link to work:**



----------

#Erlang #100 Days of Code












### Day 14: March 29, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - nthing

**Thoughts:** :  Conf day numero 3. 

**Stack:** : 

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)












### Day 13: March 26, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - nothin'

**Thoughts:** :  Conf day numero 0. 

**Stack:** : 

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)











### Day 11: March 21, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - chill with no API

**Thoughts:** :  Chill. Stoping is still keeping go

**Stack:** : Erlang + Cowboy

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)











### Day 10: March 20, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - rewrite Routes for Admin API

**Thoughts:** :  Keep going

**Stack:** : Erlang + Cowboy

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)












### Day 8: March 18, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - added routes for admin/tour API

**Thoughts:** :  Keep going

**Stack:** : Erlang + Cowboy

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)













### Day 7: March 17, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - small maraphon at Sunday. 
        deep dive into the Actors model

**Thoughts:** :  Keep going

**Stack:** : Erlang

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)










### Day 6: March 16, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - Working with Handlers  
        https://ninenines.eu/docs/en/cowboy/2.6/guide/handlers/

**Thoughts:** : Losted :/

**Stack:** : Erlang + Cowboy

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)









### Day 5: March 15, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - Working with Handlers  
        https://ninenines.eu/docs/en/cowboy/2.6/guide/handlers/

**Thoughts:** : CodeMorning losted. It was an after-working-hours try) Speed up

**Stack:** : Erlang + Cowboy

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)





















### Day 4: March 14, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - Working with Routes  
        

**Thoughts:** : CodeMorning again) Speed up

**Stack:** : Erlang + Cowboy

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

















### Day 3: March 13, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - Morning with Cowboy HTTP initialization. 
        Finally working. Now its time to wirk with protocol-specific headers 


**Thoughts:** : CodeMorning again) baby step again)

**Stack:** : Erlang + Cowboy

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

















### Day 2: March 12, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        - Morning with Cowboy HTTP initialization. 
        Have some (predicted) troubles with Erlang.mk and project start. 

**Thoughts:** : CodeMorning again) Nice to see you, pal)

**Stack:** : 

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)










### Day 1: March 11, 2019
##### (projects: poehali_info API)


**Today's Progress**:  
        Let's think it's my 1st time with Erlang\OTP. 
        Startin' here - https://ninenines.eu/docs/en/cowboy/2.6/guide/introduction/

**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:**
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)






-------------
#My first #100 Days Of Code

















### Day 155: March 3, 2019
##### (projects: P83)


**Today's Progress**:  
        - added new topic to decoding. Nice example of phone scam included to card.

**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 























### Day 154: March 2, 2019
##### (projects: chillin)


**Today's Progress**:  
        - chillin'. Project's break. Officially  

**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 






















### Day 153: March 1, 2019
##### (projects: chillin)


**Today's Progress**:  
        - I :/  

**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 















### Day 152: February 28, 2019
##### (projects: chillin)


**Today's Progress**:  
        - I guess I'm diving too deep in chillin'. I have no desire to work on any pet :/  

**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 


















### Day 151: February 26, 2019
##### (projects: chillin'n'production)


**Today's Progress**:  
        - it was production day: no code - just troubleshooting
        - after work hours - zero code and healin' chillin'


**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 


















### Day 150: February 25, 2019
##### (projects: chillin')


**Today's Progress**:  
        - healing chilling
        - no-code-mood


**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 















### Day 149: February 24, 2019
##### (projects: chillin')


**Today's Progress**:  
        - healing chilling


**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)



















### Day 148: February 22, 2019
##### (projects: VIM in VScode)


**Today's Progress**:  
        - actually nothing :/


**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)











### Day 147: February 20, 2019
##### (projects: VIM in VScode)


**Today's Progress**:  
        - actually nothing :/


**Thoughts:** : Baby step. This is a third day I feel emptyness and frustration about pet project's works. 
It scares a little. Beside, this isn't a first time I feel it. And I know it's a cicle process. Keeep going)

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)















### Day 146: February 19, 2019
##### (projects: VIM in VScode)


**Today's Progress**:  
        - oh, dio! Now vim-style works in VScode. Fuck, it's really kinky stuff))))
        Now it looks really creepy and almost like religion cult :)

        - bonus: I worked on Search component for this project. This work was in another repo, but it would relocate ASAP


**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)








### Day 145: February 18, 2019
##### (projects: VIM in win10)


**Today's Progress**:  
        - damn, its nice to see Vim in Win10. He is really my old friend, which better then VScodes and IDEAs :)

**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)


















### Day 144: February 17, 2019
##### (projects: new configs)


**Today's Progress**:  
        - nhothin'

**Thoughts:** : Baby step. Keeep going)

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)














### Day 143: February 11, 2019
##### (projects: new configs)


**Today's Progress**:  
        - just a configuration of notebook. 
        A lot of Windows 10 and Ubuntu 18.10 installs/reinstalls

**Thoughts:** : Baby step. Keeep going)

**Stack:** : OS

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
















### Day 142: February 10, 2019
##### (projects: Alibi + vimRC)


**Today's Progress**:  
        - added a new platform to the project Alibi - a blackbox for Arduino Uno
        - Elm playground (just for first-look)
        - .vimrc was updated: new async linter + Elm configs


**Thoughts:** : Baby step. Keeep going) Sunday chillin' coding.

**Stack:** : VimRc + Elm + Markdown

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)























### Day 141: February 8, 2019
##### (projects: Poehali.info)


**Today's Progress**:  
        - nothing  


**Thoughts:** : Baby step. Keeep going)

**Stack:** : Chill

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)























### Day 140: February 7, 2019
##### (projects: Poehali.info)


**Today's Progress**:  
        - init the tinder-like functionality  


**Thoughts:** : Baby step. Keeep going)

**Stack:** : React + JSX

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)























### Day 139: February 2, 2019
##### (projects: Go)


**Today's Progress**:  
        - new realization of Alibi project. Go + Android\iOS 


**Thoughts:** : Baby step. Keeep going)

**Stack:** : Go 

**Link to work:** 
- [go/alibi repository](https://gitlab.com/Liferenko/pets/tree/master/go/alibi)












### Day 138: January 19, 2019
##### (projects: learning)


**Today's Progress**:  
	- Elixir's koan 


**Thoughts:** : Baby step. 

**Stack:** : 

**Link to work:** 
- [P83](https://gitlab.com/Liferenko/P83)
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---







### Day 137: January 17, 2019
##### (projects: Vim)


**Today's Progress**:  
	- vim's source exploring and syntax files researching 


**Thoughts:** : Baby step. 

**Stack:** : 

**Link to work:** 
- [P83](https://gitlab.com/Liferenko/P83)
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---











### Day 136: January 15, 2019
##### (projects: P83)


**Today's Progress**:  
	- nothing ;/// 


**Thoughts:** : Baby step. 

**Stack:** : 

**Link to work:** 
- [P83](https://gitlab.com/Liferenko/P83)
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---








### Day 135: January 14, 2019
##### (projects: P83)


**Today's Progress**:  
	- Here is a problem with id_rsa.pub and my personal Gitlab acc (on my another SSD). Strange things =/ 


**Thoughts:** : Baby step. 

**Stack:** : 

**Link to work:** 
- [P83](https://gitlab.com/Liferenko/P83)
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---





### Day 134: January 12, 2019
##### (projects: P83)


**Today's Progress**:  
	- I found exactly needed account data. Now I'm /p83 - nice!


**Thoughts:** : Baby step. 

**Stack:** : 

**Link to work:** 
- [P83](https://gitlab.com/Liferenko/P83)
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---





























### Day 133: January 11, 2019
##### (projects: P83)


**Today's Progress**:  
	- the first step in 2019


**Thoughts:** : Baby step. This holidays kicked me off from my work mode :/ But they gave me Knut's book and way to basis knowledges. Not bad, as for me) Keeeep going! Baby step 2019

**Stack:** : 

**Link to work:** 
- [P83](https://gitlab.com/Liferenko/P83)
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---























### Day 132: December 31, 2018
##### (projects: P83)


**Today's Progress**:  
	- nothing but project management


**Thoughts:** : Baby step. Keep going!

**Stack:** : 

**Link to work:** 
- [P83](https://gitlab.com/Liferenko/P83)
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---


















### Day 131: December 30, 2018
##### (projects: P83)


**Today's Progress**:  
	- nothing but project management


**Thoughts:** : Baby step. Keep going!

**Stack:** : 

**Link to work:** 
- [P83](https://gitlab.com/Liferenko/P83)
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---














### Day 128: December 27, 2018
##### (projects: P83)


**Today's Progress**:  
	- nothing but project management


**Thoughts:** : Baby step. Keep going!

**Stack:** : 

**Link to work:** 
- [P83](https://gitlab.com/Liferenko/P83)
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---












### Day 125: December 24, 2018
##### (projects: Git)


**Today's Progress**:  
	- finally solved problem with git@gitlab.com and git@altssh.gitlab.com. All my projects' folders work properly and in order mode.

**Thoughts:** : Baby step. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 122: December 21, 2018
##### (projects: Go)


**Today's Progress**:  
	Go:
	- One more translation and audioguiding in this project. I recorded an audioguide of [Effective Go](https://github.com/Konstantin8105/Effective_Go_RU#%D0%BA%D0%BE%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%82%D0%BE%D1%80%D1%8B-%D0%B8-%D1%81%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BD%D1%8B%D0%B5-%D0%BB%D0%B8%D1%82%D0%B5%D1%80%D0%B0%D0%BB%D1%8B) and here is the link where I stopped. 
	Go docs + audioguide


**Thoughts:** : Baby step. Keep going!

**Stack:** : Go and documentation

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










#Day 121: 20 December 2018
bacgo - FE + BE Go React


### Day 120: December 19, 2018
##### (projects: Go)


**Today's Progress**:  
	Go:
	- One more translation and audioguiding in this project. I recorded an audioguide of [Effective Go](https://github.com/Konstantin8105/Effective_Go_RU#%D0%BA%D0%BE%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%82%D0%BE%D1%80%D1%8B-%D0%B8-%D1%81%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BD%D1%8B%D0%B5-%D0%BB%D0%B8%D1%82%D0%B5%D1%80%D0%B0%D0%BB%D1%8B) and here is the link where I stopped. 
	- I started with Websockets, ReactJS and Golang for vacation practice.


**Thoughts:** : Baby step. Keep going!

**Stack:** : Go and documentation

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---














### Day 119: December 18, 2018
##### (projects: Go)


**Today's Progress**:  
	Go:
	- I recorded an audioguide of [Effective Go](https://github.com/Konstantin8105/Effective_Go_RU#%D0%BA%D0%BE%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%82%D0%BE%D1%80%D1%8B-%D0%B8-%D1%81%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BD%D1%8B%D0%B5-%D0%BB%D0%B8%D1%82%D0%B5%D1%80%D0%B0%D0%BB%D1%8B) and here is the link where I stopped. 
	
**Thoughts:** : Baby step. Keep going!

**Stack:** : Go and documentation

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 116: December 15, 2018
##### (projects: global Gitlab)


**Today's Progress**:  
    - try to solve SSH access's issue
	- finally it solved. And I wrote a solve TODO to Gitlab support.



	
**Thoughts:** : Baby step. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 115: December 14, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  
    - nothing

**Thoughts:** : Baby step. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 114: December 13, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  
    - Im in solving problem with new policy of SSH access to Gitlab (RSA512)

**Thoughts:** : Baby step to the FuncProgramming. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---












### Day 113: December 12, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  
    - Erlang practice on local machine (webdev)
    - project management for pet proj (Blackbox)

**Thoughts:** : Baby step to the FuncProgramming. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 112: December 11, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  
    - Erlang practice on local machine (webdev)

**Thoughts:** : Baby step to the FuncProgramming. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---








### Day 110: December 9, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    - notin 

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---








### Day 108: December 7, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    - project management and issue solving

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---
















### Day 107: December 6, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    - only project management and issue solving

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---













### Day 106: December 4, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    - nothing :///

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---













### Day 105: December 4, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    - nothing :///

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 104: December 3, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    - post's measurement

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---











### Day 103: Decemner 2, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    - new spot

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---








### Day 102: December 1, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 101: November 29, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    backend: 
	- 

    management:
        - init account and content plan (3 post to acc)

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---












### Day 100: November 28, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    backend: 
	- 

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 99: November 27, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    backend: 
	- 

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---














### Day 98: November 26, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    backend: 
	- 

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---











### Day 97: November 25, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    backend: 
	- Golang practice

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 96: November 24, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    frontend:
        - nothing

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---







### Day 95: November 23, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    frontend:
        - nothing

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---







### Day 94: November 22, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    frontend:
        - 

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : JSX

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---













### Day 93: November 21, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    frontend:
        - columns for buttons

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : JSX

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---












### Day 92: November 20, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    frontend:
        - almost done with AddToFavorite button

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : JSX

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 91: November 19, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    frontend:
        - solve grid issue and justify-content-space-evenly

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : JSX

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---











### Day 90: November 18, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    frontend:
        - solve grid issue

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : JSX

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---








### Day 89: November 17, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    frontend:
        - SCSS + JSX

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : SCSS + JSX

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---






### Day 88: November 16, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
    
    frontend:
        - SCSS

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : SCSS

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 87: November 15, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  
    
    frontend:
        - SCSS

**Thoughts:** : Baby step to the frontend. Keep going!

**Stack:** : SCSS

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 86: November 14, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

	merge:
	    - solve the conglicts in branches
            - go refactoring

**Thoughts:** : Baby step to backend. Keep going!

**Stack:** : Go

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 85: November 13, 2018
##### (projects: poehali.info v2 client-side


**Today's Progress**:  

	frontend:
		- component Feed changed styles

**Thoughts:** : Baby step to backend. Keep going!

**Stack:** : JSX

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 84: November 12, 2018
##### (projects: poehali.info v2 server-side


**Today's Progress**:  

        backend:
            - MySQL + MySQL Workbanch + SQL Indexes


**Thoughts:** : Baby step to backend. Keep going!

**Stack:** : SQL

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---












### Day 83: November 11, 2018
##### (projects: poehali.info v2 server-side


**Today's Progress**:  

        backend:
            - new Erlang webserver (powered by Cowboy + Ranch)


**Thoughts:** : Baby step to backend. Keep going!

**Stack:** : Erlang

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 82: November 10, 2018
##### (projects: poehali.info v2 client-side + server-side


**Today's Progress**:  

        frontend:
            - Redux
            - Restructuring

        backend:
            - new Erlang webserver (powered by Cowboy + Ranch)


**Thoughts:** : Baby step to backend. Keep going!

**Stack:** : JSX + Erlang

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 81: November 9, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  

        frontend:
            - SCSS.
            - cleaning


**Thoughts:** : Baby step to backend. Keep going!

**Stack:** : SCSS

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 80: November 8, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

        backend:
            - Erlang playaround.


**Thoughts:** : Baby step to backend. Keep going!

**Stack:** : Erlang + FP

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---








### Day 79: November 7, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

        backend:
            - DB designing and blueprinting. Big thank to Denys for this prototype.

        designing:
            - user stories and board


**Thoughts:** : Baby step to backend. Keep going!

**Stack:** : Python3 + aiohttp + software design

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 78: November 6, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

        backend:
            - DB designing and blueprinting. Big thank to Denys for this prototype.


**Thoughts:** : Baby step to backend. Keep going!

**Stack:** : SQL

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 77: November 5, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

        backend:
            - Python + asyncio + scrapper. Didn't work yet.


**Thoughts:** : Baby step to backend. Keep going!

**Stack:** : Python3

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 76: November 4, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

        backend:
            - Database designing and sending to mentor's review


**Thoughts:** : Keep going!

**Stack:** : SQL

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 75: November 3, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

        backend:
            - Database designing and sending to mentor's review


**Thoughts:** : Keep going!

**Stack:** : SQL

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---








### Day 74: November 2, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

        backend:
            - Aiohttp


**Thoughts:** : Still empty :/ Ok... Keep going!

**Stack:** : Python3

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 73: November 1, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

        backend:
            - There were added data donors' Python scrapper (powered by asyncio)


**Thoughts:** : Still empty :/ Ok... Keep going!

**Stack:** : Python3

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---












### Day 72: October 31, 2018
##### (projects: poehali.info v2 client-side + server-side)


**Today's Progress**:  

        frontend:
            - There were added limit/offset as URL params

        backend:
            - WIP: params for JSON 


**Thoughts:** : Still empty :/ Ok... Keep going!

**Stack:** : React + Go

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 71: October 30, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  

        frontend:
            - 2 branches merged and conflict solved. Now it works good for working with infinity scroll and 1000 items


**Thoughts:** : I feel empty. And this feeling started when I opened a PhpStorm IDE. I guess it is a milestone: will I working in Vim or I'm ready to IDE? Strange feeling. IDE isfrustrating me. Ok... Keep going!

**Stack:** : React

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---










### Day 70: October 29, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  

        frontend:
            - there were added a TourPage container + button "ReadMore" + TODO for container


**Thoughts:** : Not so easy day. It was mindblowing study day with Database theory. Ok... Keep going!

**Stack:** : React

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 69: October 28, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

        backend:
            - there were added data donors for future parsing


**Thoughts:** : Sunday eve' coding.

**Stack:** : Python

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---








### Day 68: October 27, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  

        backend:
            - there were changed a data source from hard-coded-in-GO JSON to mock data JSON file. It means I can change a source anytime (file, URL, SQL data base etc.) and it will work properly.

        frontend:
            - it's to understand that my ApiHandler component has weak relations between FE and BE in the part of data transport. It looks mature.
        
        data science:
            - I started my DS path right this day. Jupyter & Pandas & Kaggle.com


**Thoughts:** : Nice day. Nice results. Keep moving.

**Stack:** : JSON + Go + Jupiter + Pandas

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 67: October 26, 2018
##### (projects: poehali.info v2 client-side)


**Today's Progress**:  
        frontend:
            - WIP: search component. Works with JSON parcing

**Thoughts:** : Baby step. Night. I feel that I'm emply a bit :/ Second time per 100DaysOfCode. Keep moving...

**Stack:** : React

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---








### Day 66: October 25, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  
            poehali-backend:
                - Go+MySQL = now they're connected. WIP.
                - Added func addNewEmployee() to db_connect.go
                - Added func showEmployees() to db_connect.go
            
            poselenie-frontend:
                - WIP: Button "Zoom in" was added. Logic works, but unpredictable yet.


**Thoughts:** : Baby step. Morning. Baby step. Night.

**Stack:** : Go + SQL + React

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

- [NASA hackathon repository](https://gitlab.com/greenwich/automatic-settlement-detector/) + [work-in-progress page](http://poselenie.surge.sh)

---











### Day 65: October 24, 2018
##### (projects: poehali.info v2 server-side)


**Today's Progress**:  
            poehali-backend:
                - Go+MySQL = now they're connected. WIP.
                - I created a mock data for employees in Mockaroo


**Thoughts:** : Finally I've back to SQL :D Oh, I missed you, dear pretty-face SQL :*

**Stack:** : Go + SQL 

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

---









### Day 64: October 23, 2018
##### (projects: poehali.info v2 client-side  / poselenie.surge.sh client-side)

**Today's Progress**:  
            poehali-frontend:
                - WIP: Search container. Some work-in-progress JSON jobs

            poselenie-frontend:
                - Done: Donate button for settlement supporting was added.  

**Thoughts:** : I'm feeling that my body clocks become better then early: I want to sleep at 8-9 AM and waking up at 5-6. Like in my best summer seasons :) Is it a side effect of Hackathon Night? I hope so :)


**Stack:** : React + JSON

**Link to work:** 
- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)

- [NASA hackathon repository](https://gitlab.com/greenwich/automatic-settlement-detector/) + [work-in-progress page](http://poselenie.surge.sh)

---









### Day 63: October 22, 2018
##### (projects: poehali.info v2 - backend-side)

**Today's Progress**:  
            frontend:
                - WIP: Search container. Can't handle a JSON for it

            structure-NASA:
                - init kanban board
                - added 17 tasks to a board
                - I made all preparations for comfortable team work  

**Thoughts:** : 


**Stack:** : JSX + Gitlab GUI

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---






### Day 62: October 21, 2018
##### (projects: poehali.info v2 & NASA hackathon - server-side & client-side)

**Today's Progress**:  
            frontend-NASA:
                - React + Google Map API for showing a poor settles in Syria
                - code refactoring


**Thoughts:** : I wanna new hackathon :)


**Stack:** : React + JSON 

**Link to work:** 

- [NASA hackathon repository](https://gitlab.com/Liferenko/nasa-hackathon) + [work-in-progress page](http://poselenie.surge.sh)
---








### Day 61: October 20, 2018
##### (projects: poehali.info v2 & NASA hackathon - server-side & client-side)

**Today's Progress**:  
            backend-poehali:
                - some cleaning changes in django/setting.py
                - added more valid structure for Go side (change all-in-one file structure to Do-One-Thing conception)
                - WIP: added MySQL connector in main.go    


            frontend-NASA:
                - init React app
                - working with raw data from Data scientists
                - React + Leaflet + Google Map API for showing a poor settles in Africa and Asia


**Thoughts:** : Hackathon equals skill boosting.


**Stack:** : Go + structure + React + JSON 

**Link to work:** 

- [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
- [NASA hackathon repository](https://gitlab.com/Liferenko/nasa-hackathon) + [work-in-progress page](http://poselenie.surge.sh)
---













### Day 60: October 19, 2018
##### (projects: poehali.info v2 - backend-side)

**Today's Progress**:  
            frontend:
                - Added Dockerfile for Py-backend. Python will working with tour websites parsing 

**Thoughts:** : 


**Stack:** : Django + Docker

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---













### Day 59: October 18, 2018
##### (projects: poehali.info v2 - client-side)

**Today's Progress**:  
            frontend:
                - Search container. Worked under axios.get() for search

**Thoughts:** : I feel I'm empty with one side of axios and React components.


**Stack:** : JSX + JSON

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---














### Day 58: October 17, 2018
##### (projects: poehali.info v2 - project structure)

**Today's Progress**:  
	structure:
		- some cleaning in work directory. Delete rubbish, replace Gruntfile to /app and another small changes.  
 

**Thoughts:** : Cleaning midnight working :/ Keep going


**Stack:** : project structure

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---













### Day 57: October 16, 2018
##### (projects: poehali.info v2 - deploy)

**Today's Progress**:  
    deploy:
        - Dockerfile FE sees an index.html, but still looking for all another files in unexisted dir /liferenko/. It problem, I guess, laying inside nginx.conf or something like that.

 

**Thoughts:** : One step closer to Docker FE working file. 


**Stack:** : Docker

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---











### Day 56: October 15, 2018
##### (projects: poehali.info v2 - deploy)

**Today's Progress**:  
    deploy:
        - docker-compose.yml starts BE, db and adminer locally with well-done process. So on VPS it works the same
        - Dockerfile FE still in a process

 

**Thoughts:** : Well-comfortable hour of midnight coding. I'm on 99% before Docker FE proper starting :) I guess I'll finish it next morning


**Stack:** : Docker

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---










### Day 55: October 14, 2018
##### (projects: poehali.info v2 - deploy)

**Today's Progress**:  
    deploy:
	- now it works right. BE containers works proper


 
**Thoughts:** : 1 hour of Sunday coding right in VPS. So now it works nice and predictable. 


**Stack:** : Docker

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---









### Day 54: October 13, 2018
##### (projects: poehali.info v2 - server-side + client-side)

**Today's Progress**:  
    frontend:
        - ID added to FavoriteStar component for post item tracint
        - added 2 params (limit and offset) in axios.get for proper working of infinite scroll
        - WIP: added InfiniteScroll component
        - changed LoadMore component from button to dimmer

    backend:
        - added param handler + error monitoring


 
**Thoughts:** : 8 hours of Saturday code. It feels like 2-3 hours of morning coding from home) 


**Stack:** : JSX + GO

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---












### Day 53: October 12, 2018
##### (projects: poehali.info v2 - client-side)

**Today's Progress**:  
    frontend:
        - SCSS. Few variables changed. 1 issue fixed
        - Component LoadMore replaced from PostItem to the right place (container Feed)

**Thoughts:** : Friday's night coding. Stupido time block for coding. 


**Stack:** : SCSS + JSX

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---











### Day 52: October 11, 2018
##### (projects: poehali.info v2 - client-side)

**Today's Progress**:  
    frontend:
        - WIP: I worked with Search container and JSON for this block

**Thoughts:** : I have some blocks with this task. It's hard a little for reworking React Semantic UI code for new JSON from Go+Git. 


**Stack:** : JSX + JSON

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---











### Day 51: October 10, 2018
##### (projects: poehali.info v2 - deploy)

**Today's Progress**:  
    merged branch iss41 (DOckerfile BE) with master
    merged branch with Redux changes
    deployed to test server


**Thoughts:** : midnight coding. Meeeh... :/


**Stack:** : Git + Merge Requests

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---













### Day 50: October 9, 2018
##### (projects: poehali.info v2 - client-side)

**Today's Progress**:  
	frontend:
            - WIP: Redux - Worked button only with ADD_TO_FAVORITE.
            - Now it works nice. 
            - TODO: to separate addToFav from all items to each item by ItemIndex

**Thoughts:** : morning code - totally awesome!!! And it made my night code awesome, too :) Friedns of mine call it "БЛАГВУ" - быстро, легко, а главное, в удовольствие (it is rapid, simple and, last but not least, joyful)


**Stack:** : Redux + JSX

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---














### Day 49: October 8, 2018
##### (projects: poehali.info v2 - client-side)

**Today's Progress**:  
	frontend:
            - WIP: Redux - worked with ADD_TO_FAVORITE function.

**Thoughts:** : I was skip a morning. Result: unproductive code-day. Not good. Keep going 


**Stack:** : Redux

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---
















### Day 48: October 7, 2018
##### (projects: poehali.info v2 - client-side)

**Today's Progress**:  
	frontend:
		- Added variables to SASS (fonts, colors, font-sizes)
		- connect variables and fonts with Search component


**Thoughts:** : It was day-off-alike work. I worked from Olya's notebook, directly from VPS, which contains master branch of project. Meh, looks nice but I feel I'm off from schedule. Wanna back 


**Stack:** : SASS

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---













### Day 47: October 6, 2018
##### (projects: poehali.info v2 - Docker + design)

**Today's Progress**:  
    Docker:
        - In docker-compose.yml FE starts and keeps up - that is nice
    design:
        - added screenshots from Pinterest to task cards. It helps to working better with SASS for user's page, item page, favorite posts' page and search 

**Thoughts:** : This task "Docker + CI/CD" discouraged me. I was forced to choose another tasks 
and to leave this uncompleted task in a middle of the process. 


**Stack:** : Docker + web design + project management 

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---












### Day 46: October 5, 2018
##### (projects: poehali.info v2 - Docker)

**Today's Progress**:  
    Docker:
        - reworked files FE, BE, DB Dockerfiles + well-upped docker-compose. 

**Thoughts:** : I am slow moving... but still moving. Dockerization - nice and true way. I feel I became better.  


**Stack:** : Docker

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---











### Day 45: October 4, 2018
##### (projects: poehali.info v2 - deploying)

**Today's Progress**:  
    deploy:
        - add Dockerfile for FE, BE and DB writted from scratch. It works nice and right.

**Thoughts:** : Coding from lessons - nice, but... Not so safety. 


**Stack:** : Docker

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---










### Day 44: October 3, 2018
##### (projects: poehali.info v2 - client-side)

**Today's Progress**:  
    frontend:
        + Added LoadMore button 
        + cleaned unused Header.


**Thoughts:** : A few morning works on client-side. Can't say I'm pleased. 


**Stack:** : React

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---









### Day 43: October 2, 2018
##### (projects: poehali.info v2 - deploying, client-side)

**Today's Progress**:  
    deploying:
        + WIP: Dockerfile to FE and BE build properly and with right WORKDIRs.
    frontend:
        + WIP: I separated my JSON parse function (ApiHandler) in own component.


**Thoughts:** : I feel some procrastination with 100DaysOfCode. I think it is because of many days without little victories (deploying, Docker, CI/CD etc.). Keep going 


**Stack:** : React + Docker

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---








### Day 42: October 1, 2018
##### (projects: poehali.info v2 - server-side, client-side)

**Today's Progress**:  
    deploying:
        + WIP: added Dockerfile to Frontend and Backend dirs. Paired them in docker-compose
    frontend:
        + WIP: I tried to connect JSON from Go+Gin to full word search component. I guess I have to separate my JSON parse function in own component. After that I could use it independently in each container.


**Thoughts:** : Oh, it wasn't easy. It was simple, but many-step-lenghted. And, as always, it's satisfy me a lot. It's working! :) 


**Stack:** : React + Docker

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---








### Day 41: September 30, 2018
##### (projects: poehali.info v2 - it works)

**Today's Progress**:  
    deploying:
        + Nginx added and now it works properly.
        + with command "nohup go run main.go & exit" Go server works and translate its API

**Thoughts:** : Oh, it wasn't easy. It was simple, but many-step-lenghted. And, as always, it's satisfy me a lot. It's working! :) 


**Stack:** : VPS + Nginx + some Bash

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---







### Day 40: September 29, 2018
##### (projects: poehali.info v2 deploying)

**Today's Progress**:  
    deploying:
        + added dirty solving of this goal. I see my app in a browser tab, but it hard-coded and started manually (no CI/CD)
    frontend:
        + changed (actually hard-coded) apiUrl
        + removed component "Counter". It's unused.


**Thoughts:** : I'm stuck in a Big and Shiny goal "CI\CD and rainbow unicorns". Only after my downgrading and phrase "Should I stepping back and do my page ready-to-show with manual starting style?". Only after that phrase my progresshas moved againg.


**Stack:** : VPS + hosting + Docker 

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) + [work-in-progress page](liferenko.ml)
---






### Day 39: September 28, 2018
##### (projects: poehali.info v2 client-side)

**Today's Progress**:  
    frontend:
        + WIP: Added pre-Redux funtcion for calculating "Does post added to favorite or not?".


**Thoughts:** : Morningo-day code-time. I'm not pleased for it :/ I should separate my pet-code-time and work-code-time more strictly.


**Stack:** : vanilla JS

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---






### Day 38: September 27, 2018
##### (projects: poehali.info v2 server-side)

**Today's Progress**:  
    backend:
        + WIP: A lot of works with CI and Docker-compose. Paused on a deploying of ready-to-start project in VPS
        
    frontend:
        + SCSS. Added grid styles to Feed. It made all post items fit right for each screenscale.
        + Added variable of margin. It helps DRY.


**Thoughts:** : Morning gives me not so big fun, not so big victory. Keep going


**Stack:** : GitLab + CI/CD + VPS  +  SCSS + Semantic UI

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---








### Day 37: September 26, 2018
##### (projects: poehali.info v2 server-side)

**Today's Progress**:  
    backend:
        + Added GET, POST, PUT, DELETE, OPTIONS operations
        + Amazing working proccess with CI/CD, VPN and GitLab Runner. 
        + Bought a VPS for testing this project's CI/CD.
 
**Thoughts:** : Baby steps. Again. Today it was really baby-stepped-walking to the Continuous Integration Mountain. I stopped in a"CI-pipelines-work-properly" basecamp - it stands right before the way to Docker-Compose peak. It's insane to go to the peak at night. I should start at early morning :D  

**Stack:** : Go + SQL + GitLab + CI/CD + VPS

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---




### Day 36: September 25, 2018
##### (projects: poehali.info v2 server-side)

**Today's Progress**:  
    backend:
        + CORS problem fully solved.
        + WIP: Add data for pair "MySQL-GO" (OldPost data from SQL)



**Thoughts:** : It was strange a little: today was a uneven, scrabrous day with coding. Not so butter-slidie working process. Morning was good and useful against after-work-hours of code. Ok, got it. Keep going :D  

**Stack:** : Go + ORM + SQL

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---





### Day 35: September 24, 2018
##### (projects: poehali.info v2 server-side, client-side)

**Today's Progress**:  
    backend:
    + Done: Tour JSON is generated and ready-to-use (available in /api/tours)
    + WIP: client-side: Changed post template structure. Run with error

    frontend:
    + tour posts shows from my Go app on 3011 port
    + added Redux in project (branch structure)
    + created minimum worked Redux part (Counter) - TODO add this functions to AddToFav func ()

Tickets #24, #23 are solved.

**Thoughts:** : Morning 08:00-09:45 code-time when it's raining after window! Dubtechno in headphones, xmas sweater warms me. 
Oh, there are my personal "rush-hours in a subway" :) 


**Stack:** : Go + JSON + Go data types + JSX

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---




### Day 34: September 24, 2018
##### (projects: poehali.info v2 server-side)

**Today's Progress**:  
    backend:
    + Work In Progress: Converted JSON to type TourPost struct in Go
    + I was read a lot of data types in Go

    frontend:
    + tour posts shows from my Go app on 3011 port
    + added Redux in project
    + created minimum worked Redux part (Counter) - TODO add this functions to AddToFav func



**Thoughts:** : Morning diving into Go - useful shit. 2-hours-dive and your day's clear :D Noice!


**Stack:** : Go + JSON + Go data types

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---








### Day 35: September 24, 2018
##### (projects: poehali.info v2 server-side, client-side)

**Today's Progress**:  
    backend:
    + Done: Tour JSON is generated and ready-to-use (available in /api/tours)
    + WIP: client-side: Changed post template structure. Run with error

    frontend:
    + tour posts shows from my Go app on 3011 port
    + added Redux in project (branch structure)
    + created minimum worked Redux part (Counter) - TODO add this functions to AddToFav func ()

Tickets #24, #23 are solved.

**Thoughts:** : Morning 08:00-09:45 code-time when it's raining after window! Dubtechno in headphones, xmas sweater warms me. 
Oh, there are my personal "rush-hours in a subway" :) 


**Stack:** : Go + JSON + Go data types + JSX

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---






### Day 34: September 23, 2018
##### (projects: poehali.info v2 server-side)

**Today's Progress**:  
    + Work In Progress: Converted JSON to type TourPost struct in Go
    + I was read a lot of data types in Go


**Thoughts:** : Morning diving into Go - useful shit. 2-hours-dive and your day's clear :D Noice!


**Stack:** : Go + JSON + Go data types

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---






### Day 33: September 22, 2018
##### (projects: poehali.info v2 server-side + client-side)

**Today's Progress**:  
    Refactoring:
    server:
    - replaced old struct name "Joke" to "Employee"
    - replaced /api/jokes to /api/employees
    
    client:
    - added avatar to Cards


**Thoughts:** : My workflow feels like honey: it flows slowly and continiously. And this process didn't give any backthoughts about "Do I doing it right?". Total confidence. No doubt. Oh, what a nice feeling :) 

**Stack:** : Go + Gin + React + JSON

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---







### Day 32: September 21, 2018
##### (projects: poehali.info v2 server-side + client-side)

**Today's Progress**:  Go generates JSON correctly and gives it to all world. 
+ added JSON structure inside Go+Gin
- removed authorisation modules from Go

+ axios' error handling
+ reading JSON 
- ports and CORS problem :/


**Thoughts:** : Morning. Again. Noice! :) Backend morning :) 

**Stack:** : Go + Gin + React + JSON + Axios

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---






### Day 31: September 20, 2018
##### (projects: poehali.info v2 client-side)

**Today's Progress**:  Added Axios to React app. Ticket #13 solved.
Plus two branches merged with master. All works!

**Thoughts:** : Baby steps move me faster then "I-CAN-DO-IT"-steps. I guess Chill&Code behavior is more profitable then Do-Or-Die behavior. 

**Stack:** : JSX + React + JSON + Axios

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---





### Day 30: September 19, 2018
##### (projects: poehali.info v2 client-side)

**Today's Progress**:  Big changes:
+ replace pages to right dirs, containers to right dirs
+ added react-router and his logic to Navigation and pages 

**Thoughts:** :  Big and massive changes, which I'm making Just In Time, walk long but not difficult/complex. They really walk. And I feel it goes right

**Stack:** : JSX + React + ES6 + Project sructure

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---





### Day 29: September 18, 2018
##### (projects: poehali.info v2 client-side)

**Today's Progress**:  Each post contains right data inside :) Plus added some styles for postItems ( :hover animation and some margin). 

**Thoughts:** :  Finally each post contains neccessary data :)

**Stack:** : JSX + React + ES6

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---





### Day 28: September 17, 2018
##### (projects: poehali.info v2 client-side)

**Today's Progress**:  react-router, change structure part: replace all containers into App container (before that they were in index.js). And finally this.props.id starts working how I want :) 

**Thoughts:** :  I understood how props work :)

**Stack:** : JSX + React + ES6

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---







### Day 27: September 16, 2018
##### (projects: poehali.info v2 client-side)

**Today's Progress**: I worked with pair "Feed + PostItem". Still can't separate them to showing each post with own data. But today I checked that API works right and my problem was only in this.state and React's methods. 

**Thoughts:** : Morning took me code-hour-guarantee; eve gave me profitable 2-hours-code-set. I'm pleased :D 

**Stack:** : JSX + React

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---






### Day 26: September 15, 2018
##### (projects: poehali.info v2 client-side)

**Today's Progress**: Nice rework of containers and components. I moved footer, search, post and nav block from wrong folder (components) to move understandable folder Conrainers.
Plus I added container Feed for all posts. Now posts rendering works bad (copy-paste post adding). Next time I should make post adding through lodash.times()

And I added scss to proper working in project.

**Thoughts:** : Mornings better then nights. Why I remember that only... at night?

**Stack:**  Structure + SCSS

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---





### Day 25: September 14, 2018
##### (projects: poehali.info v2 client-side + deploy)

**Today's Progress**: Worked with vue-components, but only in work project. 
AND! It will useful for poehali.info - I worked with CI/CD.
So I will add .gitlab-ci.yml to all my projects 


**Thoughts:** : Morning code - the best and profitable part of a day. 

**Stack:**  Grunt + SCSS

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---





### Day 24: September 13, 2018
##### (projects: poehali.info v2 client-side + Algos and Data structures)

**Today's Progress**: 

Added yet another 5 answers to algorithm and data structures questions: Binary trees(BT), BT search algos.

**Thoughts:** : Morning code - the best and profitable part of a day. 

**Stack:**  Grunt + SCSS

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---




### Day 23: September 12, 2018
##### (projects: poehali.info v2 client-side + Basic knowledges - Algos and Data structures)

**Today's Progress**: add .scss-files with structure logic. I guess I have to separate @imports of containers and components right inside of dedicated page .scss file. 

Plus I started a new repo with algos and data structures interview questions. I found (and understood) difference between linked list types.   

**Thoughts:** : Morning code - the best and profitable part of a day. 

**Stack:**  Grunt + SCSS

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---


### Day 22: September 11, 2018
##### (projects: poehali.info v2 client-side)

**Today's Progress**: Cypher and drum'n'bass. I add yet another few disks from my DJ foldet to graph database. Why? 'cause I need it :) Why are you just did't use Rekordbox? 'cause I need GraphDB skill :D 

**Thoughts:** : Morning code - the best and profitable part of a day. 

**Stack:**  GraphDB + Cypher

**Link to work:** : [DJ DnB music GraphDB](https://gitlab.com/Liferenko/pets/tree/master/py/liferenko/head/brain/memory/long-term/dj_music_folder)
---
 
---





### Day 21: September 10, 2018
##### (projects: poehali.info v2 client-side)

**Today's Progress**:  Add Grunt + SASS + Watcher to project. They help me with stylish part of client-side poehali.info. 
Grunt watch works properly, listens right dirs and makes "hot changes" inside of code. "*click* Noice!"(c) :D

**Thoughts:** : A hour of code at the evening - shitty idea. Morning gives more!

**Stack:**  Grunt + SCSS

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---






### Day 20: September 9, 2018
##### (projects: poehali.info v2 client-side, pet project with graph DB)

**Today's Progress**: 2 hours of Cypher work. It was nice :) But it learned me that I have to remember about "git commit". It crashs my mood :/ 

Another code-practice - CSS and SCSS in poehali.info. 

**Thoughts:** : Make "git commit" each time. EACH! TIME! Because beside you will lost your 2-hours-work. My comp was cruch and even after saving it saved broken file. And after my stupid and childish removing of file I had lost my CYpher file with tracks DB.  

**Stack:**  CSS + SCSS / Cypher

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) , [DJ DnB music GraphDB](https://gitlab.com/Liferenko/pets/tree/master/py/liferenko/head/brain/memory/long-term/dj_music_folder)
---





### Day 19: September 8, 2018
##### (projects: poehali.info v2 server-side, pet project with graph DB)

**Today's Progress**: I worked on Auth0 API (now it works properly, but gives me access_denied back). Plus blueprinting of server-side (Go+Gin) was finish. And as a bonus, I was taking 2 incredible hours with Cypher and graph DB for my another nano-pet-project. Cypher really looks friendly and useful. I guess I should continue my progress with pair "Erlang(BE)+Cypher(DB)+N2O(FE)"    

**Thoughts:** : When I start my morning early and have an early-hour-coding - it makes my day twice longer and less worryable about progress. Quote from life - "Damn, I have to coding lterally anything to save my progress". Resume: "Morning" equals "baby step wider then usually".

**Stack:**  React + Auth0 API + Go / Cypher + Neo4j

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info) , [DJ DnB music GraphDB](https://gitlab.com/Liferenko/pets/tree/master/py/liferenko/head/brain/memory/long-term/dj_music_folder)
---






### Day 18: September 7, 2018
##### (project: poehali.info v2 client-side)

**Today's Progress**: I worked with Unsplash API for getting countries` photos for each post. I was get access to API, get raw JSON, but didn't parse it inside React compnent (,ain problem is that I should add Unsplash API methods to my API methods. It gives me "undefined" - and it is right answer for my wrong request) 

**Thoughts:** : Baby step! Yet another baby step :) I'm lovin` it

**Stack:**  React + Unsplash API

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---






### Day 17: September 6, 2018
##### (project: poehali.info v2 client-side)

**Today's Progress**: Finally replacing from Reactstrap to Semantic-UI works. And works properly, with JSON data binding. 

**Thoughts:** : I worked before cinema :D And this is really my path - working in strang places)) "*click* Noice!"(c)

**Stack:**  React + Semantic UI

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---





### Day 16: September 5, 2018
##### (project: poehali.info v2 client-side)

**Today's Progress**: Progress is just in a errors. New error "Element rype is invalid: expected a string, but got: undefined."
I got this error when I tried to change Reactstrap JSX to Semantic UI React. 

**Thoughts:** : Angry! :/

**Stack:**  React 

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---





### Day 15: September 4, 2018
##### (project: poehali.info v2 server-side)

**Today's Progress**: all code-time was spended to searching trouble, which gives me 404 error in localhost:3000. 
Minus -  no results; Plus - level-up in React Props and super(), this.%whatever%

**Thoughts:** : Angry! :/

**Stack:**  React + Go 

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---






### Day 14: September 3, 2018
##### (project: poehali.info v2 server-side)

**Today's Progress**: Add authorization function (Auth0.js) into app. It helps to signing in with Google/Github/Twitter. 

**Thoughts:** : We worked from park today. So we finished when the sun went down. It looks totally romantic. "Romantique? With mosquitos?"-she said. :D  

**Stack:**  Go + Gin 

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
---






### Day 13: September 2, 2018
##### (project: edmhistory app, server-side + client-side & poehali.info v2 server-side)

**Today's Progress**: Initialized basic client- and server-side modules, added React + Go + Gin to repo. Go&Gin have a properly worked local server, which understood GET and POST requests. Plus now this project has a kanban board for progress monitoring and goals decostructing.  

Plus with default project (poehali.info v2) I added the same GET+POST requests listener and temp React(CDN) viewer. TODO add Go+Gin port to already worked React (non-CDN) tours app.

**Thoughts:** : I was surprised that React can work with CDN. And another surprise for me was that I can working with POMODORO :D Nice! 

**Stack:**  Go + Gin + React + REST api + Gitlab Kanban board.

**Link to work:** [EDM history app (a backup of old Flash site)](https://gitlab.com/Liferenko/pets/tree/master/go/history-of-edm)

---




### Day 11: August 31, 2018
##### (project: poehali.info v2, client-side)

**Today's Progress**: Installed search component with half-worked JSON file. Next time I will make this JSON working as tour catalog.

**Thoughts:** : Summer moved on :D It was amazing 92 days of my season. And thanks Goat it was code-season. This summer gives me React, Go, Flask(Python), ES6, Ubuntu 18.04, Puppy Linux. One bad thing of this summer was Mackbook Pro 2010 death (minus SSD, SSD cable and display's matrix). But it's not the end of MBP :)

**Stack:**  React + JSON

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

---



### Day 10: August 30, 2018
##### (project: poehali.info v2, server-side + client-side)

**Today's Progress**: I was continue with Go+Gin (added few functions for /api/, but without connecting to tours and travels). And after this server-side-work I worked with Yarn + Semantic-UI + React. So 90% of time was spended for Ubuntu's terminal and yarn activating. But thanks Goat it successfully finished.


**Thoughts:** : Even famous libraries have semi-complete documentation and not so clear "Getting started" instuction. So, as always... Dig it, dig it, dig it deeper :D

**Stack:**  Go + Gin framework + SemanticUI + React + Yarn

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

---





### Day 9: August 29, 2018
##### (project: poehali.info v2, server-side)

**Today's Progress**: Working with initializing Go and Gin parts for server-side of poehali.info project. It wolked harder then it was before. 
But it keeps going... :)

Source for Go/Gin/React - https://hakaselogs.me/2018-04-20/building-a-web-app-with-go-gin-and-react

**Thoughts:** : Midnight coding of server-side part sucks :/ Especially when you are morning person :)

**Stack:**  Go + Gin framework

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

---




### Day 8: August 28, 2018
##### (project: poehali.info v2, client-side)

**Today's Progress**: I changed an old stupid view in Footer to new beautify-looking footer)  
Next time I should to do connecting database with React app for showing Post data. (And I HAVE TO use Neo4j as a database. I know that this type of DB (its graph DB) is overresoursed in my case. But I HAVE TO try this DB again, because I already worked with SQL)

Source for starting - https://expressjs.com/en/guide/database-integration.html#neo4j


**Thoughts:** : I guess here is a gun in my leg. I have to try some overqualified stuff (graph db against relational db)

**Stack:**  JS(React) + Reactstrap + JSX

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

---



### Day 7: August 27, 2018
##### (project: poehali.info v2, client-side)

**Today's Progress**: add Footer to my project's pages. Looks shitty, but I'll have done it by 8PM. 

**Thoughts:** : Dear Me-in-future, please don't forget about commas and semicolons :D  

**Stack:**  JS(React) + Reactstrap + JSX

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

---

### Day 6: August 26, 2018
##### (project: python random code, server-side)

**Today's Progress**: try to work with NumPy (Python lib) and his .npy-format for data storage. It shows me I can CRUD any numerical data like data.npy instead of data.txt. For example, txt-file with 1M float numbers can be readed at 1.4702 sec. At the same time same data.npy can be readed at 0.0306 sec. It will be useful when I need to use big dataset multiple times. 

**Thoughts:** : Frankly noone knows about my 100daysOfCode activity. I'm talking about my offline-crowd. And I guess it's profitable for my results.

**Stack:**  Python3 + NumPy

**Link to work:** [pets/py repository](https://gitlab.com/Liferenko/pets/tree/master/py/liferenko/head/brain/consciousness/calculation/numpy_file_format)

---


### Day 5: August 25, 2018
##### (project: poehali.info v2, client-side)

**Today's Progress**: minor changes - just add nav.js component, replace html to JSX. 

**Thoughts:** : Working from cafe with loud shitty electro house and over-warm wether - equals non-productive working :/  

**Stack:**  JS(React) + Reactstrap + JSX

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

---

### Day 4: August 24, 2018
##### (project: poehali.info v2)

**Today's Progress**: add reactstrap, change directory's name from view to components, replace old sintax in Post to reactstrap syntax (Card, Col etc.) 

**Thoughts:** : It really goes cool when you're working with programming stream on a background (today I was working with SirMrE's twitch stream).  

**Stack:**  JS(React) + Reactstrap

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

---



### Day 3: August 23, 2018
##### (project: poehali.info v2)

**Today's Progress**: make index.js shows correctly my 3 components(App, Header and Post) + add variables to Post (next step will change this object from const to data grabbing from DB).
Plus I found an useful command for Vim - it helps replace few words.


How it works:

- move cursor to word

- press * and type 'ciw'

- type new word

- press Esc and press n to come down to next old word

- press . to repeat last command


**Thoughts:** I was working with streams on second screen while I'm coding. This was Winderton (dev stream) and freeCodeCamp ReactJS stream. It really tunes me to the correct mood. Code mood. Nice and easy)

**Stack:**  JS(ReactJS) + Vim 

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

---

### Day 2: August 22, 2018
##### (project: poehali.info v2)

**Today's Progress**: add scss header file + move Header structure from old file to React-alike syntax in class Header + move Gruntfile for sass worker and watcher + add structure for post.js (template for each tour post)

**Thoughts:** This changes weren't so heavy and time-spanding. But I feel it moves me.

**Stack:**  SCSS + Task runner(Grunt)

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)

---

### Day 1: August 21, 2018
##### (project: poehali.info v2)

**Today's Progress**: Init React app template for this project. Add blueprint test files, draft SCSS-files, draft class Header.

**Thoughts:** That moment when I close my overthinking mind process with just one thing - git add -> git commit -> git push. And all thoughts about 100daysOfCode just started :D Hmm, maybe I become marute?:)

**Stack:** JS(React) + HTML + CSS(SCSS) + Tests(QUnit)

**Link to work:** [poehali.info repository](https://gitlab.com/Liferenko/poehali_info)
