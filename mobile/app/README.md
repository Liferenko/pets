Use cases:
- you are in a subway. You see someone is enjoying their music. You may feel it, you may see it. You wanna join. Open The App, choose "Listen" and choose a broadcaster from playlist
- you are in a subway with friends. It's loud as hell but you all wanna "feel the flow, keep the vibe". You open The App, choose "Share", open Spotify and start your playlist.
You hear it. Friends hear it. Dance!


General TODO:
- P2P connection without Wi-Fi - https://developer.android.com/training/connect-devices-wirelessly/nsd-wifi-direct
- Latency and delays. Will it be an issue? Does Wi-Fi allow to share music p2p with as less delay as possible?

Share (Publisher) TODO:
-
- show how many listeners you have right now

Listen (Subscribers) TODO:
- List of broadcasters near me - https://developer.android.com/guide/topics/connectivity/wifip2p#discover-peers


Plans:
- PubAndroid and many SubsAndroid
- Pub_CarPlay_Androud and Subs_Carplay_Androids
- Pub_Android and SubsIOS