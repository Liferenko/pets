# OPTIMIZE: rename with cucumber convention

Story: device owner has small voicerecords on their device the way it works in vehicle's dashcams.
As a Mobile device owner with anxiety and fear of be in a middle of some harsh situation
I want to be able to have voicerecords in my device. Just in case.
	Scenario: device owner can find a 15-minutes long voicerecords on their device.
		Given owner's device is on
			And owner knows the device is recording
		When owner opens a voice records directory
		Then they have an access to the voice records
		And the records are ordered by created_at timestamp

Feature Performace optimization
	Scenario: device's battery life
		Given owner's device is continiously recording
			And owner knows the device is recording
		When owner is using their device in usual mode
		Then owner does not feel any overhear
			And owner does not see leak of battery of their device
			And owner does not see a leak of storage size of their device

	Scenario: device's storage size for our application
		Given owner's device is continiously recording
			And owner knows the device is recording
		When owner is using their device in usual mode
		Then owner does not see a leak of storage size of their device
			And owner does not feel any overhear

Feature: Loop mode
	Scenario: every voice recording is less than 15 minutes long
		Given owner's device is continiously recording
			And a reserved storing space on the device is locked for N gigabytes
		When voice recorgint length >= 15 minutes
		Then the device finished current recorging process
			And saves voice recording as a file in a store
			And starts new voice recording

Feature: Cloud sync
	Scenario: device's cloud syncronization
		Given owner's device is continiously recording
			And owner knows the device is recording
			And owner enabled a "Cloud sync" feature in our application settings
		When owner has some dificulties to access the voice records on a device
		Then owner can find the voice records uploaded in owner's cloud provider (GDrive, OneDrive, AWS S3 etc)
