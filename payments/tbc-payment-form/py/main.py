import requests
import json
import os
import logging
import urllib

logger = logging.getLogger()
logger.setLevel(logging.INFO)
# Demo code sample. Not indended for production use.

# See instructions for installing Requests module for Python
# http://docs.python-requests.org/en/master/user/install/

########## REFACTOR
# TODO move it to ENV vars

client_id = 70****0,
client_secret = "7****9"
########## REFACTOR

apikey = '5oyW**********************fBaVVw'
print(apikey)
def access_token():
  requestUrl = "https://api.tbcbank.ge/v1/tpay/access-token"
  requestHeaders = {
    "apiKey": apikey,
    "Accept": "application/json",
    "Content-Type": "application/x-www-form-urlencoded"
  }
  requestBody = {
    "client_id": client_id,
    "client_secret": client_secret
  }
  request = requests.post(requestUrl, data=requestBody, headers=requestHeaders)
  access_token = json.loads(request.content)
  return access_token["access_token"]


api_auth_info = access_token()
params = {
            "currency": "GEL",
            "total": 0.22,
            "subtotal": 0.22,
            "tax": 0.18,
            "shipping": 0.23

        }

def get_payment_url(total_price, currency, products):
  requestUrl = "https://api.tbcbank.ge/v1/tpay/payments"
  requestHeaders = {
    "apiKey": apikey,
    "Authorization": "Bearer %s" % (api_auth_info),
    "Accept": "application/json",
    "Content-Type": "application/json"
  }
  requestBody = json.dumps({
          'amount': {
              'currency': currency,
              'total': total_price,
              'subtotal': params["subtotal"],
              'tax': params["tax"],
              'shipping': params["shipping"],
              },
          'returnurl': "https://tamara.market",
          'methods': [4, 5, 7], # payment methods #TODO add 9 (ApplePay)
          'installmentProducts': [
              {
                  'name': products,
                  'price': total_price,
                  'quantity': 1
              }
          ],
          'callbackUrl': "asdf",
          'preAuth': False,
          'language': "RU",
          'merchantPaymentId': "7245413_1", #Merchant-side payment identifier. Ask client about it
          'skipInfoMessage': False,
          'saveCard': False
          })
  request = requests.post(requestUrl, data=requestBody, headers=requestHeaders)
  response = json.loads(request.content)
  redirect_url = response["links"][1]["uri"]
  logger.info(redirect_url)
  return redirect_url

def lambda_handler(event, context):
  logger.info(f'event body == {event["body"]}')

#Parse event.body to needed payment params
  request_body = urllib.parse.urlparse(urllib.parse.unquote_plus(event["body"]))
  query = urllib.parse.parse_qs(request_body.path)
  params = {k: v[0] for k, v in dict(query).items()}

  tilda_payment_data = params
  logger.info(tilda_payment_data)
  #client_id = tilda_payment_data["tilda-login"] I may use it to avoid hardcoded client_id + secret
  currency = tilda_payment_data["currency"]
  #lang = tilda_payment_data["language"]
  total = tilda_payment_data["total"]
  products = tilda_payment_data["description"]

  # Redirect to payment page
  response = {}
  response["statusCode"] = 302
  response["headers"] = {'Location': get_payment_url(total, currency, products)}
  data = {}
  response["body"] = json.dumps(data)
  return response

