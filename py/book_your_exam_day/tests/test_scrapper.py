import unittest
from src.scrapper import ask_about_data

class ScrapperTestCase(unittest.TestCase):
    # take a response
    def test_response_sended_back(self):
        print(f'Ответ - {ask_about_data().text}')
        self.assertEqual(ask_about_data().status_code, 200)
