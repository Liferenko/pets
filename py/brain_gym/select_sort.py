def find_smallest(arr):
    """TODO: Docstring for find_smallest.

    :arr: array imported_data
    :returns: index of smallest item in array

    """
    smallest = arr[0]
    smallest_index = 0

    for i in range( 1, len(arr) ):
        if arr[i] < smallest:
            smallest = arr[i]
            smallest_index = i
    return smallest_index

def select_sort(arr):
    """TODO: Docstring for select_sort.

    :arr: TODO
    :returns: new sorted array

    """
    new_array = []
    for i in range( 1, len(arr) ):
        smallest = find_smallest(arr)
        new_array.append( arr.pop(smallest) )
    return new_array


if __name__ == '__main__':
    # import numpy as np

    imported_data = [1,6,33,2,16,100,87,56,999,4,443,192993,2,2,444,5,3]

    print(select_sort(imported_data))
