categories = [
    {
        "title": "Food",
        "subcategories": [
            {"title": "Bread"},
            {
                "title": "Meat",
                "subcategories": [
                    {"title": "Pork",
                     "subcategories":[
                         {"title": "White Pork"}, 
                         {"title": "Red Pork"}
                     ]
                     },
                    {"title": "Beef"},
                ],
            },
            {"title": "Cheese"},
        ],
    },
    {"title": "Drinks"},
]
#
#
# Expected output
#Food
#-Bread
#-Meat
#--Pork
#---White Pork
#---Red Pork
#--Beef
#-Cheese
#Drinks

def pretty_category_prnt(cat_list, acc = 0):
    dash = "-"
    for i in cat_list:
        print(f"{dash * acc} {i['title']}")

        if i.get("subcategories"):
            acc += 1
            pretty_category_prnt(i["subcategories"], acc)


pretty_category_prnt(categories, 0)

