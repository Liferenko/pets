import datetime
import pytz
import arrow
import json

# Открыл JSON файл
with open('id_cleaned_cities_countryCode_EU_CIS_USA_Canada.json', "r", encoding='utf-8') as original_json:
    cities = json.load(original_json)
    old_list = list( map(lambda city : city['time_zone'], cities) )
    #print(old_list)

    for city in cities:
        if city['time_zone'] in old_list:
            print( 'old value - {}'.format(city['time_zone']) )
            city['time_zone'] = arrow.utcnow().to(city['time_zone']).format('ZZ')
            print( 'new value - {}'.format(city['time_zone']) )

with open('id_cleaned_cities_countryCode_EU_CIS_USA_Canada.json', "w") as updated_json:
    json.dump(cities, updated_json, indent=2, ensure_ascii=False)

# сохраняю результат в файл

