# замена таймзон на UTC
#   TODO:
#   - читать и печатать таймзоны
#   - конвертить в дата-время-utc
#   - вычленить чисто utc
#   - записывать вместо таймозы utc

import datetime
import pytz
import json

def open_json_with_cities(address_to_file):
    """TODO: Docstring for open_json_with_cities.

    :address_to_file: Address to JSON file
    :returns: TODO

    """
    with open(address) as json_file:
        cities = json.load(json_file)
        for city in cities:
            print( city['time_zone'] )

def write_new_utc_instead_of_timezone(current_timezone):
    """TODO: Docstring for write_new_utc_instead_of_timezone.

    :current_timezone: TODO
    :returns: TODO

    """

    open_json_with_cities('id_cleaned_cities_countryCode_EU_CIS_USA_Canada.json')
    data = pytz.timezone(current_timezone)
    print(data)
    
def update_data_in_file(address_to_file):
    """TODO: Docstring for update_data_in_file.

    :address_to_file: TODO
    :returns: updated JSON file

    """
    

if __name__ == "__main__":
    items = ['Asia/Yerevan', 'Europe/Vienna']
    for item in items:
        write_new_utc_instead_of_timezone(item)

    print("Ready")
