import re

 
# TODO вот тут валидируем, но стоит попробовать не валидировать левую часть до @-sign
emails = ['норм@домен.ру', 'bru..nofff@mail.ru', '1..--@mail.ru', "@asdf"]
for email in emails:
      try:
          print(re.match(r"^[\w\d\.\-]+[@{1}][^@][\w\d\.\-]+\.[\w\d\.\-]+$", email))
      except Exception as e:
          raise AssertionError('Invalid email {}'.format(email))

