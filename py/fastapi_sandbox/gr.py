from graphene import ObjectType, String, Schema, Int, Float
from fastapi import FastAPI
from starlette.graphql import GraphQLApp


class Query(ObjectType):
    """ I've no idea what to write here :D """
    hello = String(name=String(default_value="stranger"), sum=Float(default_value=2.0)) # TODO: rework def value
    age = Int(number=Int(default_value=4))

    def resolve_hello(self, info, name, sum):
        """docstring for resolve_hell
        Args:
            info (?)
            name (str):

        Returns:
            String
        """
        return "Hello " + name + "!", f'Your extra money is {sum}'

    def resolve_age(self, info, number):
        """docstring for resolve_age

        Returns:
            Int
        """
        return number


app = FastAPI(
    title="FastAPI + Graphene = bff", #TODO make it dynamic
    description="Ho ho ho, lets check this out",
    version="4.8.15"
)
app.add_route("/", GraphQLApp(schema=Schema(query=Query))) #TRY is .add_route same as @app?
