from graphene import ObjectType, String, Schema

class Query(ObjectType):
    """description"""
    hello = String(argument=String(default_value="stranger"))
    goodbye = String()


    def resolve_hello(self, info, argument: str) -> str:
        """docstring for resolve_hello
        Args:
            info: donno
            argument (str): name to say

        Results:
            (str) just a hello
        """
        return f'Hello {argument}'

    def resolve_goodbye(self, info):
        """docstring for resolve_goodbye"""
        return 'See ya'



schema = Schema(query=Query)

result = schema.execute('{hello}')
print(result.data['hello'])

result = schema.execute('{ hello (argument: "graph") }')
print(result.data['hello'])

result = schema.execute('{goodbye}')
print(result.data['goodbye'])
