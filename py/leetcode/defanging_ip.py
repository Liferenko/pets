def defangIPaddress(address: str) -> str:
    result_ip_address = list(address)
    dot_index = address.index('.')
    covered_dot = '[.]'
    result_ip_address.insert(dot_index, covered_dot)
    print(''.join(result_ip_address))

if __name__ == "__main__":
    defangIPaddress('192.168.11.173')

