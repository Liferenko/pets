-module(calculate).
-export([bmi/2]).

%%% TODO:
%%% -[x] Add metric system
%%% -[ ] Add imperial (703 * weight(lbs)/[height(in)]^^2)
%%% -[ ] To add atoms to define which system is used


bmi(Weight, Height) ->
    Weight / (Height * Height).
