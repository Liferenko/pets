-module(tests_SUITE).
-export([all/0]).
-export([bmi31/1, bmi22/1, bmi30/1]).

%% TODO need to refactor that part to suites using
all() -> [bmi31, bmi22, bmi30].

bmi31(_) ->
    31.11111111111111 = calculate:bmi(70, 1.5),
    {comment, "Green test"}.

bmi22(_) ->
    22.22222222222222 = calculate:bmi(50, 1.50),
    {comment, "Green test"}.

bmi30(_) ->
    30.061278760550348 = calculate:bmi(104, 1.86),
    {comment, "Green test with my measurements"}.
