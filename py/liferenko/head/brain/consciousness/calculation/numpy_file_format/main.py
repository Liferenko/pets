'''
Author of code: https://github.com/tirthajyoti 

Source: https://towardsdatascience.com/why-you-should-start-using-npy-file-more-often-df2a13cc0161


Maintain of exact this file: Paul Lirefenko
'''


import numpy
import time

test_num_sample = 1000000

with open('data_for_test.txt', 'w') as data_for_test:
    for _ in range(test_num_sample):
        data_for_test.write(str(10*numpy.random.random()) + ',')


        
time_start = time.time()
with open('data_for_test.txt', 'r') as data_for_test:
    datastr = data_for_test.read()


list_of_readed_data = datastr.split(',')
list_of_readed_data.pop()
array_from_list = numpy.array(list_of_readed_data, dtype=float).reshape(1000,1000)
time_finish = time.time()

numpy.save('fnumpy.npy', array_from_list)

time_start_npy = time.time()
array_reloaded = numpy.load('fnumpy.npy')
time_finish_npy = time.time()

# Reshape a dataset
time_start_reshape = time.time()
array_reshape = numpy.load('fnumpy.npy').reshape(10000, 100)
time_finish_reshape = time.time()


print("-"*11)
#print(array_from_list)
print('\nShape: ', array_from_list.shape)
print(f"Time took to read without .npy: {time_finish - time_start} seconds.")
print("-"*11)
print(".npy was save")
print("-"*11)
#print(array_reloaded)
print('\nShape: ', array_reloaded.shape)
print(f"Time took to load .npy: {time_finish_npy - time_start_npy} seconds.")
print("-"*11)
print('\nReshape: ', array_reshape.shape)
print(f"Reshape the npy-dataset: {time_finish_reshape - time_start_reshape} seconds.")
print("-"*11)
