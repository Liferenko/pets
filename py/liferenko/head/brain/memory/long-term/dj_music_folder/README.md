This is a digital version of my own dj music folder with 51 CDs of DnB and 19 CDs with """some""" non-DnB music)

Goal of this repository: to save all my DnB tracks from CDs to graphDB.
Why graph db? Because of harmony mixing theory. Each track in my folder has two columns: traditional tune sign (ex: Ebm or Am or F#m) and Camelot Wheel tune view (ex: 2A or 8A or 11A).
So each track can be mixed with neighbor tune track. Example: track 6A will be sound better with track 5A, 6A, 6B or 7A. 

So in a graph I can show it better. And this graph will show me which track I can use next in a dj set.  

So for this project I'm using Neo4j as a DBMS and Cypher as a programming language. 
