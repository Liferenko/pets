= GraphGist
:neo4j-version: 0.0.1
:author: Pavel Liferenko 
:twitter: https://twitter.com/mr_Liferenko
:gitlab: https://gitlab,com/Liferenko
:style: #54A835/#1078B5/white:Colorized(name)

= ASCIIdoc formatting



//setup
//hide
[source,cypher]
----
CREATE

    (d11_2:   Track {author: 'Decem',           name: 'Don\'t sleep',                   camelotPosition: '2A',  tune: 'Ebm'}),    
    
    (d20_8:   Track {author: 'Benny Page',      name: 'Can\'t test me',                 camelotPosition: '2A',  tune: 'Ebm'}),    
    


    (d25_7:   Track {author: 'Logistics',       name: 'Every beat of the heart',        camelotPosition: '2A',  tune: 'Ebm'}),    

    (d31_9:   Track {author: 'Bcee & Lomax',    name: 'Dusk \`til dawn',                camelotPosition: '4A',  tune: 'Fm'}),    
    (d31_11:  Track {author: 'Danny Byrd',      name: 'Control freak',                  camelotPosition: '3A',  tune: 'A#m'}),    
    

    (d33_2:   Track {author: 'Lenzman',         name: 'Caught up',                      camelotPosition: '2A',  tune: 'Ebm'}),    

    (d34_4:   Track {author: 'Mutated forms',   name: 'Behind the scene',               camelotPosition: '2A',  tune: 'Ebm'}),    
    (d34_5:   Track {author: 'Nu:tone',         name: 'Soul flower',                    camelotPosition: '5B',  tune: 'D#'}),    
    (d34_11:  Track {author: 'Tyler Straub',    name: 'Walk with you',                  camelotPosition: '2A',  tune: 'Ebm'}),    
    
    (d35_6:   Track {author: 'Raw Q',           name: 'Summer rain',                    camelotPosition: '2A',  tune: 'Ebm'}),    
    
    (d36_2:   Track {author: 'Saburuko',        name: 'Warped',                         camelotPosition: '3A',  tune: 'A#m'}),    
    
    (d37_9:   Track {author: 'Logistics',       name: 'Blackout',                       camelotPosition: '6A',  tune: 'Gm'}),    
    
    (d38_9:   Track {author: 'Furney',          name: 'Soh Cah Toa',                    camelotPosition: '1A',  tune: 'G#m'}),    

    (d40_1:   Track {author: 'SPY',             name: 'Feel the music',                 camelotPosition: '4A',  tune: 'Fm'}),    
    (d40_2:   Track {author: 'Hydro',           name: 'Ride with me',                   camelotPosition: '11B', tune: 'A'}),    
    (d40_3:   Track {author: 'Mindscape',       name: 'Maniac',                         camelotPosition: '10A', tune: 'Bm'}),    
    (d40_4:   Track {author: 'Dyamorph',        name: 'Hydroforce',                     camelotPosition: '6A',  tune: 'Gm'}),    
    (d40_5:   Track {author: 'Audio',           name: 'Fallout',                        camelotPosition: '6A',  tune: 'Gm'}),    
    (d40_6:   Track {author: 'Noisia',          name: 'Deception',                      camelotPosition: '7A',  tune: 'Dm'}),    
    (d40_7:   Track {author: 'Culture Shock',   name: 'Gears',                          camelotPosition: '2B',  tune: 'F#'}),    
    (d40_8:   Track {author: 'Lomax',           name: 'Jungle FX',                      camelotPosition: '8A',  tune: 'Am'}),    
    (d40_9:   Track {author: 'Utopia',          name: 'Diamond shine',                  camelotPosition: '7A',  tune: 'Dm'}),    

    (d49_3:   Track {author: 'Naiive',          name: 'Scars',                          camelotPosition: '2A',  tune: 'Ebm'}),    
    
    (d50_5:   Track {author: 'Pendulum',        name: 'Plastic world',                  camelotPosition: '1A',  tune: 'G#m'}),    
    (d50_8:   Track {author: 'Whethan',         name: 'Be like you (Hajimari rmx)',     camelotPosition: '2A',  tune: 'Ebm'}),


    (d51_1:   Track {author: 'Keeno',           name: 'Fading Fast',                    camelotPosition: '2A',  tune: 'Ebm'}),
    (d51_6:   Track {author: 'Pendulum',        name: 'Girl in the fire',               camelotPosition: '3A',  tune: 'A#m'}),
    (d51_10:  Track {author: 'Dualistic',       name: 'Station six',                    camelotPosition: '2A',  tune: 'Ebm'}),    
---
Break

[source,cypher]
----
MATCH (d51_10)-[:MIX_WITH]->(d51_6) RETURN d51_10, d51_6
----

