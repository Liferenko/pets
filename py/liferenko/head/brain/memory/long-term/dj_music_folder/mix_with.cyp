= Mix_with table 
== Child for camelot.cyp
:author: Pavel Liferenko
:twitter: https://twitter.com/mr_Liferenko
:gitlab: https://gitlab,com/Liferenko



[source,cypher]
---
CREATE
  (d50_5)-[:MIX_WITH]->(d51_1),
  (d50_5)-[:MIX_WITH]->(d50_8),
  (d50_5)-[:MIX_WITH]->(d51_10),  

  (d50_8)-[:MIX_WITH]->(d50_5),
  (d50_8)-[:MIX_WITH]->(d51_1),
  (d50_8)-[:MIX_WITH]->(d51_6),  
  (d50_8)-[:MIX_WITH]->(d51_10),

  (d51_1)-[:MIX_WITH]->(d51_10),
  (d51_1)-[:MIX_WITH]->(d50_5),
  (d51_1)-[:MIX_WITH]->(d50_8),
  (d51_1)-[:MIX_WITH]->(d51_6),

  (d51_6)-[:MIX_WITH]->(d50_8),
  (d51_6)-[:MIX_WITH]->(d51_1),
  (d51_6)-[:MIX_WITH]->(d51_10),    

  (d51_10)-[:MIX_WITH]->(d50_5),
  (d51_10)-[:MIX_WITH]->(d50_8),
  (d51_10)-[:MIX_WITH]->(d51_1),    
  (d51_10)-[:MIX_WITH]->(d51_6)

---
