'''
This is the main file of the blackbox audiorecorder macOS app.
'''
import os
import time
import threading
import tkinter as tk
import sounddevice as sd
import soundfile as sf

class BlackboxApp:
    def __init__(self):
        self.recording = False
        self.folder_path = "BlackboxRecords"
        self.file_duration = 180  # 3 minutes in seconds
        self.create_folder()
        self.root = tk.Tk()
        self.root.title("Blackbox Audiorecorder")
        self.root.geometry("300x150")
        self.start_button = tk.Button(self.root, text="Start blackbox", command=self.start_recording)
        self.start_button.pack()
        self.stop_button = tk.Button(self.root, text="Stop blackbox", command=self.stop_recording)
        self.stop_button.pack()
        self.root.protocol("WM_DELETE_WINDOW", self.on_window_close)
        self.root.mainloop()
    def create_folder(self):
        if not os.path.exists(self.folder_path):
            os.makedirs(self.folder_path)
    def start_recording(self):
        if not self.recording:
            self.recording = True
            self.start_button.config(state=tk.DISABLED)
            threading.Thread(target=self.record_audio).start()
            print("Started recording")

    def record_audio(self):
        while self.recording:
            timestamp = time.strftime("%Y%m%d-%H%M%S")
            filename = os.path.join(self.folder_path, f"{timestamp}.wav")
            samplerate = 44100
            data = sd.rec(int(self.file_duration * samplerate), channels=1)
            print("Recording file: ", filename)
            sd.wait()
            sf.write(filename, data, samplerate=samplerate)

    def stop_recording(self):
        self.recording = False
        self.start_button.config(state=tk.NORMAL)
        print("Stopped recording")

    def on_window_close(self):
        if self.recording:
            self.stop_recording()
        self.root.destroy()
if __name__ == "__main__":
    app = BlackboxApp()
