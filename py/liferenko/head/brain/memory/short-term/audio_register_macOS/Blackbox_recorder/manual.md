# Blackbox Audiorecorder macOS App User Manual

## Introduction

The Blackbox Audiorecorder macOS App is a Python-based application that allows users to record 3-minute audio files from the default microphone input on their macOS device. The app is designed to function similar to a dashcam recorder, constantly recording audio files with timestamps and storing them in a folder named "BlackboxRecords".

## Installation

To use the Blackbox Audiorecorder macOS App, follow these steps:

1. Install Python 3 on your macOS device if it is not already installed.
2. Open a terminal window and navigate to the directory where you want to install the app.
3. Clone the repository by running the following command:

   ```
   git clone <repository_url>
   ```

4. Navigate to the cloned repository directory:

   ```
   cd <repository_directory>
   ```

5. Install the required dependencies by running the following command:

   ```
   pip install -r requirements.txt
   ```

## Usage

To use the Blackbox Audiorecorder macOS App, follow these steps:

1. Open a terminal window and navigate to the directory where the app is installed.
2. Run the following command to start the app:

   ```
   python main.py
   ```

3. The app window will open, displaying two buttons: "Start blackbox" and "Stop blackbox".
4. Click the "Start blackbox" button to start recording audio. The button will be disabled while recording is in progress.
5. The app will constantly record 3-minute audio files from the default microphone input and save them in the "BlackboxRecords" folder with timestamps as the file names.
6. To stop recording, click the "Stop blackbox" button. The "Start blackbox" button will be enabled again.
7. To exit the app, close the app window or press the close button.

## Troubleshooting

If you encounter any issues while using the Blackbox Audiorecorder macOS App, try the following troubleshooting steps:

1. Make sure the default microphone input on your macOS device is properly configured and working.
2. Check if the "BlackboxRecords" folder is created in the same directory as the app. If not, create the folder manually.
3. Ensure that you have the necessary permissions to access the default microphone input and save files in the "BlackboxRecords" folder.
4. If the app crashes or freezes, try restarting the app or your macOS device.

## Conclusion

The Blackbox Audiorecorder macOS App provides a simple and convenient way to record 3-minute audio files from the default microphone input on your macOS device. By following the installation and usage instructions in this user manual, you can easily start using the app and store your audio recordings in the designated folder. Enjoy recording your audio with the Blackbox Audiorecorder macOS App!