# What is perfect monitor ever? That monitor that doesn't exist

## Idea
Monitorless and noGUI operating system written for software engineers.

## Features
- code wherever you want
- minus one dirt-collector on your desk
- UltraHD 8K 144fps infinity-inch diagonal... already inside your mind.


## TODO:
- cursor moves "in a room": you're hearing it and where it is right now. Sounds like ein Echolot or a sonar.
- screenreader is your focused point of view
- vim-inspired hjkl to move cursor. HJKL to move faster through the desktop/window/application
- GPUless computer or even phone. You can use whatever you want
- your favorite background music is still with you
- ssh connection to cloud solutions to use more RAM or CPU for your tasks


## FAQ
- How can I use it? 
You are goddamn software engineer: you already know (and remember) all movements/commands you will use in 90% of time. Let your hands play first fiddle
