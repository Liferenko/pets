# sub

import socket
import os

sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

sock.bind("/tmp/file_name.sock")
sock.listen(1)

# Accept a conn
conn, client_address = sock.accept()

data = conn.recv(1024).decode()
print("Recv data:", data)

conn.close()
sock.close()
