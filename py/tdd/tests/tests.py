import unittest
import sys, os

sys.path.append(os.path.dirname(os.getcwd()))
from main import *




class TestNitroSalt(unittest.TestCase):

    """Docstring for TestNitroSalt. """

    def test_nitro_salt_returns_mass(self):
        """TODO: to be defined. """
        self.assertEqual(nitro_salt(1000), 10)    
        self.assertEqual(nitro_salt(1500), 15)    
        self.assertEqual(nitro_salt(800), 8)    
    
    def test_nitro_salt_returns_int(self):
        self.assertIsInstance(nitro_salt(1000), int)


if __name__ == '__main__':
    unittest.main()
