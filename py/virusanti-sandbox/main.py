"""
It uses public VirusShare(https://www.virusshare.com/)
to work with virus/malware opensource databases
"""

from fileinput import filename
import requests
import hashlib
import sys
import os

current_dir = os.getcwd()

# "poisoned" files to test virus scanning
virustotal_api = "https://www.virustotal.com/api/v3/files/report"


# default virus found to false
virus_found = False

class FILE_SCAN():
    def inspect_suspected_file(filepath, readable_hash):
        with open(filepath, 'r') as pack:
            for line in pack:
                if readable_hash in line:
                    virus_found = True
                    print(f"File {filepath} is poisoned")
                    return virus_found
                if not readable_hash in line:
                    pass
                else:
                    pack.close()

    def scan():
        suspect_file_path = f'{current_dir}/suspect_file.txt'
        # get Virus hashes from DB
        list_of_virus_hash_files = [
                f'{current_dir}/poisoned_files/SHA256-Hashes_pack1.txt',
                f'{current_dir}/poisoned_files/SHA256-Hashes_pack2.txt',
                f'{current_dir}/poisoned_files/SHA256-Hashes_pack3.txt',
                f'{current_dir}/poisoned_files/md5-Hashes_pack1.txt',
                f'{current_dir}/poisoned_files/md5-Hashes_pack2.txt',
                f'{current_dir}/poisoned_files/md5-Hashes_pack3.txt',
        ]

        with open(suspect_file_path,"rb") as target_file:
            bytes = target_file.read()
            readable_hash = hashlib.md5(bytes).hexdigest()
            # For a test you can uncomment the line below and run script `python3 main.py`
            #readable_hash = "8c4867a434e0b2"

        # display hash
        print("----------")
        print("Suspect file Hash:  " + readable_hash)
        print("----------")
        # close file
        target_file.close()

        # find suspect_file_hash in virus_total_DB hash files
        for file in list_of_virus_hash_files:
            result = FILE_SCAN.inspect_suspected_file(file, readable_hash)
            return result


if __name__ == "__main__":
    """
    TODO:
    - read file from CLI (filepath)
    """
    result = FILE_SCAN.scan()
    print("----")
    print(f"Scan result. Virus found: {result}")
    print("----")

