#[test]
fn test_main() {
    let expected = "How did I resolve it?\n";

    let mut output = Vec::new();
    hdiri::main(&mut output);

    assert_eq!(String::from_utf8_lossy(&output), expected);
}

