use mini_redis::{client, Result};

#[tokio::main]
async fn main() -> Result<()> {
    let mut addr = String::new();
    addr.push_str("127.0.0.1");
    addr.push_str(":6379");

    let mut client = client::connect(addr).await?;

    client.set("supppp", "big boy".into()).await?;

    // Get a key
    let result = client.get("supppp").await?;

    println!("server response: >> {:?}", result);

    Ok(())
}
