use mini_redis::{Connection, Frame};
use tokio::net::{TcpListener, TcpStream};

#[tokio::main]
async fn main() {
    let mut addr = String::new();
    addr.push_str("127.0.0.1");
    addr.push_str(":6379");

    // Bind the listener to addr
    let listener = TcpListener::bind(addr).await.unwrap();

    loop {
        let (socket, _) = listener.accept().await.unwrap();

        // Seems like it is Elixir's GenServer.start_link/1 or Task.start_async
        tokio::spawn(async move {
            process(socket).await;
        });
    }
}

// TODO: continue here - https://tokio.rs/tokio/tutorial/shared-state

async fn process(socket: TcpStream) {
    use mini_redis::Command::{self, Get, Set};
    use std::collections::HashMap;

    let mut db = HashMap::new(); // Store data here
    let mut conn = Connection::new(socket);

    while let Some(frame) = conn.read_frame().await.unwrap() {
        let response = match Command::from_frame(frame).unwrap() {
            Set(cmd) => {
                // value is stored as Vec<u8>
                db.insert(cmd.key().to_string(), cmd.value().to_vec());
                println!(">> !!! <<");
                Frame::Simple("OK".to_string())
            }

            Get(cmd) => {
                if let Some(value) = db.get(cmd.key()) {
                    Frame::Bulk(value.clone().into())
                } else {
                    Frame::Null
                }
            }

            cmd => {
                panic!("unimplemented {:?}", cmd)
            }
        };

        conn.write_frame(&response).await.unwrap();
    }
}
