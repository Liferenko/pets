let SessionLoad = 1
let s:so_save = &g:so | let s:siso_save = &g:siso | setg so=0 siso=0 | setl so=-1 siso=-1
let v:this_session=expand("<sfile>:p")
silent only
silent tabonly
cd ~/pets/rust/my-redis
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
let s:shortmess_save = &shortmess
if &shortmess =~ 'A'
  set shortmess=aoOA
else
  set shortmess=aoO
endif
badd +26 src/main.rs
badd +34 term://~/pets/rust/my-redis//65717:w3m\ https://tokio.rs/tokio/tutorial/spawning
badd +5 examples/hello-redis.rs
badd +1814 ~/plugged/slimv.vim/slime/xref.lisp
badd +1 term://~/pets/rust/my-redis//71845:cargo\ run\ --example\ hello-redis
badd +0 term://~/pets/rust/my-redis//72089:cargo\ run\ --example\ hello-redis
badd +1162 ~/.vim/autoload/plug.vim
argglobal
%argdel
$argadd ~/pets/rust/my-redis
set stal=2
tabnew +setlocal\ bufhidden=wipe
tabrewind
edit src/main.rs
let s:save_splitbelow = &splitbelow
let s:save_splitright = &splitright
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
let &splitbelow = s:save_splitbelow
let &splitright = s:save_splitright
wincmd t
let s:save_winminheight = &winminheight
let s:save_winminwidth = &winminwidth
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
exe 'vert 1resize ' . ((&columns * 91 + 91) / 182)
exe 'vert 2resize ' . ((&columns * 90 + 91) / 182)
argglobal
balt ~/.vim/autoload/plug.vim
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let &fdl = &fdl
let s:l = 26 - ((24 * winheight(0) + 17) / 35)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 26
normal! 018|
lcd ~/pets/rust/my-redis
wincmd w
argglobal
if bufexists(fnamemodify("term://~/pets/rust/my-redis//65717:w3m\ https://tokio.rs/tokio/tutorial/spawning", ":p")) | buffer term://~/pets/rust/my-redis//65717:w3m\ https://tokio.rs/tokio/tutorial/spawning | else | edit term://~/pets/rust/my-redis//65717:w3m\ https://tokio.rs/tokio/tutorial/spawning | endif
if &buftype ==# 'terminal'
  silent file term://~/pets/rust/my-redis//65717:w3m\ https://tokio.rs/tokio/tutorial/spawning
endif
balt ~/pets/rust/my-redis/src/main.rs
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
let s:l = 34 - ((33 * winheight(0) + 17) / 35)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 34
normal! 0
lcd ~/pets/rust/my-redis
wincmd w
exe 'vert 1resize ' . ((&columns * 91 + 91) / 182)
exe 'vert 2resize ' . ((&columns * 90 + 91) / 182)
tabnext
argglobal
if bufexists(fnamemodify("term://~/pets/rust/my-redis//72089:cargo\ run\ --example\ hello-redis", ":p")) | buffer term://~/pets/rust/my-redis//72089:cargo\ run\ --example\ hello-redis | else | edit term://~/pets/rust/my-redis//72089:cargo\ run\ --example\ hello-redis | endif
if &buftype ==# 'terminal'
  silent file term://~/pets/rust/my-redis//72089:cargo\ run\ --example\ hello-redis
endif
balt term://~/pets/rust/my-redis//71845:cargo\ run\ --example\ hello-redis
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
let s:l = 1 - ((0 * winheight(0) + 17) / 35)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 1
normal! 0
lcd ~/pets/rust/my-redis
tabnext 1
set stal=1
if exists('s:wipebuf') && len(win_findbuf(s:wipebuf)) == 0 && getbufvar(s:wipebuf, '&buftype') isnot# 'terminal'
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20
let &shortmess = s:shortmess_save
let s:sx = expand("<sfile>:p:r")."x.vim"
if filereadable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &g:so = s:so_save | let &g:siso = s:siso_save
set hlsearch
nohlsearch
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
