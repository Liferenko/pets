extern crate webbrowser;

use std::env;
use webbrowser::open;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("Usage: ? <search query>");
        println!("- ?r <search query> in Rust");
        println!("- ?e <search query> in Elixir");
        return;
    }

    // Open the search query in the default browser
    let query =
        &args[1..]
        .join(" ");
                 
    //let mut search_url: String = "".to_string();
    
    println!("{:?}", &args);

    let search_url =
        match args[0].as_str() {
        "?e" => 
            format!("https://www.google.com/search?q={}&{}", query, "in Elixir"),
        "?r" => 
            format!("https://www.google.com/search?q={}&{}", query, "in Rust"),
        _ => 
            format!("https://www.google.com/search?q={}", query),
        };
    // let search_url = format!("https://www.google.com/search?q={}", query);

    match open(&search_url) {
        Ok(_) => println!("Opening search query in the default browser..."),
        Err(e) => println!("Failed to open the browser: {}", e),
    }
}
