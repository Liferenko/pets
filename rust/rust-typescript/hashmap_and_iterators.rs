use std::collections::HashMap;

fn main() {
    let hmap = HashMap::from([
            ("name", "Karlo"),
            ("lname", "Pasolini"),
            ("city", "Tbilisi"),
            ("country", "Saqartvelo"),
    ]);

    for (k, v) in &hmap {
        println!("{}: {}", k, v)
    };
}
