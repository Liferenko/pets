use std::fs::read_to_string;
use core::fmt::Debug;

//////////////
// Structs and friends 
//////////////

enum Color {
    Red,
    Blue,
    Green,
    Yellow,
}

impl Color {
    fn is_green(&self) -> bool {
        if let Color::Green = self {
            return true;
        } return false;
    }

    fn is_green_parts(&self) -> bool {
        match self {
            Color::Red => return false,
            Color::Blue => return true,
            Color::Green => return true,
            Color::Yellow => return true,
        }
    }
}

#[derive(Debug)]
struct Custom {
    age: usize,
    name: String,
}

// impl Debug for Custom {
//     fn print_custom(&self) {
//         return (self.0, self.1);
//     }
// }

#[derive(Debug)]
enum Item {
    Number(usize),
    Word(String),
    MyCustom(Custom),
}
//////////////
// Functions 
//////////////
fn append(items: &mut Vec<Item>) {
    items.push(Item::Word("Hello, dude".into()));
    // items
    // .iter()
    // .for_each(|item| println!("{:?}", item));
}

fn read_colors(color: Color) {
    // add enum
    match color {
        Color::Blue => println!("blue, is_green_parts? {}", Color::is_green_parts(&Color::Blue)),
        Color::Green => println!("green, is_green_parts? {}", Color::is_green_parts(&Color::Green)),
        Color::Red => println!("red, is_green_parts? {}", Color::is_green_parts(&Color::Red)),
        _ => println!("any other color"),
    }
}

fn read_file() {
    let file = read_to_string("lines.txt").unwrap();

    file
        .lines()
        .enumerate()
        .skip(2)
        .take(4)
        //.filter(|line| line.0 < 4)
        .for_each(|line| println!("{:?} ", line.1));
    // .collect();
    todo!();
}

fn main() {
    let mut list_str: Vec<Item>
        = vec![
            Item::Word("one".to_string()),
            Item::Word("two".to_string()),
            Item::Word("four".to_string())
        ];

    let mut list_num: Vec<Item> = vec![
        Item::Number(1),
        Item::Number(2),
        Item::Number(3),
        Item::Number(6),
    ];

    append(&mut list_str);
    println!("{:?}", list_str);

    append(&mut list_num);
    println!("{:?}", list_num);

}
