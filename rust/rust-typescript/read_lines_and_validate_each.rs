use std::env;

// // enum RawData {
// //     String,
// //     Number(i32),
// //     Vec(i32)
// // }
// // 
// // impl RawData for String {
// // 
// // }

// trait RawData {
//    fn is_number(&self) -> bool;
// }
// 
// impl RawData for i32 {
//     fn is_number(&self) -> bool {
//         return *self.parse::<i32>();
//     }
// }
// 
// impl RawData for String {
//     fn is_number(&self) -> bool {
//         return Err = &self.parse::<i32>();
//     }
// }
// 

fn print_validated_msg(item: &str) {
    if let Ok(value) = item.parse::<usize>() {
        println!("{}", value);
    } else {
        println!("Line is not a number");
    }
}

fn read_from_file_by_filepath(filepath: String) {
    let file = std::fs::read_to_string(filepath)
        .expect("unable to read the file {}");
    
    file
       .lines()
       .filter_map(|s| s.parse::<usize>().ok())
       //.for_each(|line| print_validated_msg(line));
       .for_each(|line| println!("{}", line));
}

fn main() {
    let path: String =
        env::args()
        .last()
        .expect("should be at least one arg");

    read_from_file_by_filepath(path);
}
