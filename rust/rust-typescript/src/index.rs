fn main() {
    let list_for_iterator: Vec<i32> = vec![1, 2, 3];
    
    let mut result =
        list_for_iterator
            .iter()
            .map(|i| i + 1);
            //.collect(); // take the iterator and put it somewhere

    let mut new_vector = vec![];

    while let Some(x) = result.next() {
        new_vector.push(x);
    }

    println!("result {:?}", result);
}
