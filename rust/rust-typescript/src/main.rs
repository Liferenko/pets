use anyhow::Result;
use std::str::FromStr;
use crate::shapes::{
    area::Area,
    circle::Circle,
    rectangle::Rectangle,
    collisions::Contains,
    collisions::Points,
    collisions::Collidable,
};
mod shapes;

#[derive(Debug)]
enum Shape {
    Circle(Circle),
    Rectangle(Rectangle),
}

impl FromStr<> for Shape {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (shape, data) =
            s
            .split_once(" ")
            .ok_or(anyhow::anyhow!("Invalid shape"))?;

        //println!("shape: {}, data: {}", shape, data);

        match shape {
            "rectangle" => return Ok(Shape::Rectangle(data.parse()?)),
            "circle" => return Ok(Shape::Circle(data.parse()?)),
            _ => return Err(anyhow::anyhow!("Invalid shape")),
        }

    }
}

impl Points for Shape {
    fn points(&self) -> shapes::collisions::PointIter {
        match self {
            Shape::Circle(c) => c.points(),
            Shape::Rectangle(r) => r.points(),
        }
    }
}

impl Points for &Shape {
    fn points(&self) -> shapes::collisions::PointIter {
        match self {
            Shape::Circle(c) => c.points(),
            Shape::Rectangle(r) => r.points(),
        }
    }
}

impl Contains for Shape {
    fn contains_point(&self, point: (f64, f64)) -> bool {
        match self {
            Shape::Circle(c) =>
                return c.contains_point(point),
                
            Shape::Rectangle(r) =>
                return r.contains_point(point),
        }
    }
}

// //////////////
// Functions
// //////////////

fn main() -> Result<()> {
    //read the file
    let file = std::fs::read_to_string("shapes_data.txt")?;

    let shapes =
        file
            .lines()
            .filter_map(|line| line.parse::<Shape>().ok())
            .collect::<Vec<Shape>>();

    println!("{:?}", shapes);

    let collisions:Vec<(&Shape, &Shape)> =
        shapes
        .iter()
        .skip(1)
        .zip(shapes.iter().take(shapes.len() - 1))
        .filter(|(a, b)| a.collide(b))
        .collect();
        
        for (a, b) in collisions {
            println!("{:?} collides with {:?}", a, b)
        }

    return Ok(());
}
