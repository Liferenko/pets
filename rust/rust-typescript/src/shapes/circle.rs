use crate::Area;
use std::fmt::Display;
use std::str::FromStr;
use crate::shapes::collisions::PointIter;
use crate::shapes::collisions::Points;
use crate::shapes::collisions::Contains;

#[derive(Debug)]
pub struct Circle {
    pub x: f64,
    pub y: f64,
    pub radius: f64,

}

impl Contains for Circle {
    fn contains_point(&self, (x, y): (f64, f64)) -> bool {
        let dx = self.x - x;
        let dy = self.y - y;
        
        return dx * dx + dy * dy <= self.radius * self.radius;
    }
}

impl Points for Circle {
    fn points(&self) -> PointIter {
        return vec![(self.x, self.y)].into()
    }

}


impl Default for Circle {
    fn default() -> Self {
        return Circle {
            x: 1.0,
            y: 1.0,
            radius: 8.0
        };
    }
}

impl Display for Circle {
   fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
       write!(f, "Circle(x: {}, y: {}), R={}", self.x, self.y, self.radius)
   }    
}

impl Area for Circle {
    fn area(&self) -> f64 {
        let result = self.radius * self.radius * std::f64::consts::PI;

        return result;
    }
}

impl FromStr for Circle {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts = s.split(" ").collect::<Vec<_>>();

        if parts.len() != 4 {
            return Err(anyhow::anyhow!("Bad circle"));
        }

        return Ok(Circle {
            x: parts[0].parse()?,
            y: parts[1].parse()?,
            radius: parts[2].parse()?,
        })
    }
}
