pub trait Collidable<T> {
    fn collide(&self, _: &T) -> bool;
    fn collides(&self, list: &[T]) -> bool{
        for point in list {
            if self.collide(&point) {
                return true;
            }
        }
        return false;
    }
}

pub trait Points {
    fn points(&self) -> PointIter;
}

pub trait Contains {
    fn contains_point(&self, point: (f64, f64)) -> bool;
}

pub struct PointIter {
    points: Vec<(f64, f64)>,
    idx: usize,
}

impl Iterator for PointIter {
    type Item = (f64, f64);

    fn next(&mut self) -> Option<Self::Item> {
        if self.idx >= self.points.len() {
            return None;
        }

        let point = self.points[self.idx];
        self.idx += 1;

        return Some(point);
    }
}

impl From<Vec<(f64, f64)>> for PointIter {
    fn from(points: Vec<(f64, f64)>) -> Self {
        return PointIter {
            points,
            idx: 0,
        };
    }
}

impl<T, U> Collidable<T> for U
where T: Points,
      U: Contains {
   fn collide(&self, other: &T) -> bool {
       for point in other.points() {
           if self.contains_point(point) {
               return true;
           }
       }
       return false;

   }
}
