fn orphan_fn() -> Vec<i32> {
    if 4 == 1 {
        return vec!(1);
    };

    todo!();
}

fn match_me(x: i32, y: &Option<String>) -> (i32, &Option<String>) {
    return (x, y);
}

fn main() {
    let try_this_string = Some("Flots".to_string());
    let mk_begin = match_me(10, &try_this_string);
    let (my_int, my_str) = mk_begin;

    println!("| my_int {}", my_int);
    println!("| my_str {:?}", my_str);

    println!("| plain str {:?}", &try_this_string);
}

