fn not_even_main(lol: i32) -> i32 {
    println!("\"im not in use\"- {} said", lol);

    let x = vec![1,2, 3, 4, 5, 10]
        .iter()
        .map(|map_item| map_item + 1)
        .fold(0, |fold_item_x, fold_item_y| fold_item_x + fold_item_y);

    return x;
}


fn main() {
    let x = 5 + /* 90 + */ 5;
    println!("Is `x` 10 or 100? x = {}\n", x);
    println!("Hello, world!");
    
    let result = not_even_main(x);
    println!("{:?}", result);
}
