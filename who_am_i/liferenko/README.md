Pet projects:
- Tiny cliniq (BE: Clojure, FE: ClojureScript)
- Blackbox (Python 3.6, desktop app)
- Poehali.info v2 (BE: Go + Gin; FE: ReactJS)
- BodyLogs

Hackathons:
- [NASA SpaceApp 2018, Kyiv, Ukraine](https://2018.spaceappschallenge.org/challenges/what-world-needs-now/land-where-displaced-people-settle/teams/greenvich/project)
- [NASA SpaceApp 2018, Abastumani, Sakartvelo](https://2019.spaceappschallenge.org/challenges/living-our-world/show-me-data/teams/earthlings/stream)
